from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(250), nullable=False)


class UserSurvey1(Base):
    __tablename__ = 'userSurvey1'
    id = Column(Integer, primary_key=True)
    username = Column(String(250), nullable=False)


class UserSurvey2(Base):
    __tablename__ = 'userSurvey2'
    id = Column(Integer, primary_key=True)
    username = Column(String(250), nullable=False)


class Score(Base):
    __tablename__ = 'score'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    level_number = Column(Integer, nullable=False)
    right_allowed = Column(Integer, nullable=False)
    right_blocked = Column(Integer, nullable=False)
    wrong_allowed = Column(Integer, nullable=False)
    wrong_blocked = Column(Integer, nullable=False)
    score = Column(Integer, nullable=False)
    max_score = Column(Integer, nullable=False)
    user = relationship(User)


class LogEntry(Base):
    __tablename__ = 'logentries'
    id = Column(Integer, primary_key=True)
    score_id = Column(Integer, ForeignKey('score.id'))
    action = Column(String(20), nullable=False)
    ip_address = Column(String(50), nullable=False)
    src_port = Column(Integer, nullable=False)
    dest_port = Column(Integer, nullable=False)
    ingoing = Column(Boolean, nullable=False)
    protocol = Column(String(50), nullable=False)
    application = Column(String(50), nullable=False)
    app_protocol = Column(String(50), nullable=True)
    should_be = Column(String(20), nullable=False)
    score = relationship(Score)


def create_database():
    engine = create_engine('sqlite:///database/DropItDB.db')
    Base.metadata.create_all(engine)
