from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import db


def insert_player_complete_data(username=None, levels=None):
    engine = create_engine('sqlite:///database/DropItDB.db')
    db.Base.metadata.bind = engine

    db_session = sessionmaker(bind=engine)
    session = db_session()

    if session.query(db.User).filter(db.User.username == username).first() is not None:
        print "Player " + username + " tried to upload data more than once"
        return "{\"status\":\"error\", \"message\":\"You have already uploaded your data! " \
               "Link to the second survey: https://goo.gl/FO94LC\"}"
    else:
        new_user = db.User(username=username)
        session.add(new_user)

    for level_data in levels:
        stats = level_data["stats"]
        new_score = db.Score(level_number=level_data["levelNumber"], right_allowed=stats["ra"],
                             right_blocked=stats["rb"], wrong_allowed=stats["wa"], wrong_blocked=stats["wb"],
                             score=stats["score"], max_score=stats["maxScore"], user=new_user)
        session.add(new_score)

        for log_entry in level_data["logEntries"]:
            conn = log_entry["conn"]
            new_log = db.LogEntry(action=log_entry["action"], ip_address=conn["ipAddress"], src_port=conn["srcPort"],
                                  dest_port=conn["destPort"], ingoing=conn["ingoing"], protocol=conn["protocol"],
                                  application=conn["application"], app_protocol=conn["appProtocol"],
                                  should_be=conn["shouldBe"], score=new_score, )
            session.add(new_log)
    session.commit()

    return "{\"status\":\"ok\", " \
           "\"message\":\"Data uploaded successfully! Link to the second survey: https://goo.gl/FO94LC\"}"
