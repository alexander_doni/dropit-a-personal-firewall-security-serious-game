Good afternoon,
as you know, we, at the "MoveTheNet Inc.", produce every kind of network equipment. Routers, firewalls, and VPN gateways are just some of them. Our targets range from house customers to big companies and governments as well. I am proud to say that more than 80% of the network devices around the world have been produced by us!_
At the moment we are opening our new headquarters and we lack staff. You have been temporarily employed as a computer security agent given your outstanding career. You will be examined carefully during your job. Do not make us regret our choice or you will have something to regret as well..._
Our engineers are still setting the network up and you have to provide them with assistance. We are still not ready to communicate with the Internet. Your objective is to drop all the connections originating from the external networks and allow only the ones originated from the internal network._
Our firewall is not totally ready as well so you will not be able to use its "Consider as" functionality. Not a big problem since it cannot even show the application name related to a request, which is a main benefit of a personal firewall in respect to other types of firewalls. 
Our internal network subnet range is 192.168.1.1 - 192.168.1.255, everything else has to be considered external and be dropped as a result._
#image$400$85$tips
#text$90$195$An IP address can be seen as an ID of a computer on a network. It is made up by four numbers separated by a dot and each number ranges from 0 to 255. In most cases the private network address starts with 192.168 while the third and the fourth ones may vary. However, note that, while 192.168.5.67 and 192.168.5.215 belong to the same private network, 192.168.5.67 and 192.168.4.50 do not. As you can see, 192.168.1.1 - 192.168.1.255 is not the only possible subnet range for a private network.$180_
#image$400$85$tips
#image$400$245$notice
#image$515$175$alertWhite
#image$380$1755$fwAlertText
#text$275$230$Application: fake.exe
#text$275$260$IP Address: 192.168.1.34
#text$275$290$Port: 135 TCP (ms-rpc)
#image$515$250$inArrow
#text$90$410$This is an example of an IP address of a computer inside your internal network. An example of internal network is your home or office network, while the external network is the one beyond your router.$180
#image$435$258$redEllipse_
#image$400$85$tips
#image$400$245$notice
#image$515$175$alertWhite
#image$380$1755$fwAlertText
#text$275$230$Application: fake.exe
#text$275$260$IP Address: 127.0.0.1
#text$275$290$Port: 135 TCP (ms-rpc)
#image$515$250$outArrow
#text$90$410$This is the loopback address and it means your computer itself. It can happen for an application to generate this kind of notices. It can be safely allowed though many personal firewalls normally do this automatically.$180
#image$425$258$redEllipse
