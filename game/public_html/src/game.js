/*global Phaser*/
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.game = new Phaser.Game(800, 600, Phaser.AUTO, 'game');

window.game.globals = {
    //Add variables here that you want to access globally
    tutorialRead: false,
    playerName: "Player123",
    savedata: [],
    soundEffectsOn: 0.7,
    //0: Easy, 1: Medium, 2: Hard
    difficulty: 0,
    music: require('./modules/music')
};

window.game.state.add('play', require('./states/play.js'));
window.game.state.add('playEnd', require('./states/playEnd.js'));
window.game.state.add('howto', require('./states/howto.js'));
window.game.state.add('load', require('./states/load.js'));
window.game.state.add('error', require('./states/error.js'));
window.game.state.add('menu', require('./states/menu.js'));
window.game.state.add('boot', require('./states/boot.js'));
window.game.state.add('selection', require('./states/selection.js'));
window.game.state.add('levelEnd', require('./states/levelEnd.js'));
window.game.state.start('boot');