/* global Phaser */
var AjaxFileReader = require('../modules/classes/AjaxFileReader')();
var loadSave = require('../modules/load_save_functions');

/**
 * State that loads all the game assets: images, maps, sprites, sounds, and the various text and JSON files needed.
 * @type {{soundArray: Array, preload: module.exports.preload, create: module.exports.create, nameFunc: module.exports.nameFunc, loadingLabel: module.exports.loadingLabel, loadAssetsMenu: module.exports.loadAssetsMenu, loadAssetsPda: module.exports.loadAssetsPda, loadAssetsTutorial: module.exports.loadAssetsTutorial, loadAssetsNotice: module.exports.loadAssetsNotice, loadLevelSelection: module.exports.loadLevelSelection, loadNotificationAndDialogue: module.exports.loadNotificationAndDialogue, loadMapAssets: module.exports.loadMapAssets, loadCharacters: module.exports.loadCharacters, loadEffects: module.exports.loadEffects, loadLog: module.exports.loadLog, loadSounds: module.exports.loadSounds, loadMusic: module.exports.loadMusic, loadSaves: module.exports.loadSaves, loadLevels: module.exports.loadLevels, loadLevelsTutorials: module.exports.loadLevelsTutorials, loadConsequences: module.exports.loadConsequences, loadDialogues: module.exports.loadDialogues, loadOutros: module.exports.loadOutros, loadHowto: module.exports.loadHowto, loadCredits: module.exports.loadCredits, loadConsT: module.exports.loadConsT, launch: module.exports.launch, errorLoadingFunc: module.exports.errorLoadingFunc}}
 * @module
 * @name State: Load
 */
module.exports = {
    soundArray: [],
    /**
     * Main function that calls each loading subfunction.
     */
    preload: function () {
        //Loading assets for the game
        this.loadingLabel();
        this.loadAssetsMenu();
        this.loadAssetsPda();
        this.loadAssetsTutorial();
        this.loadAssetsNotice();
        this.loadLevelSelection();
        this.loadNotificationAndDialogue();
        this.loadMapAssets();
        this.loadCharacters();
        this.loadEffects();
        this.loadLog();
        this.loadSounds();
        this.loadMusic();
        this.loadSaves();
    },

    /**
     * Loads level data after all the assets have been loaded.
     * The levels loading function will call the consequence loading function after completion.
     */
    create: function () {
        this.nameSignal = new Phaser.Signal();
        this.nameSignal.add(this.nameFunc, this);
        loadSave.promptForName(this.nameSignal);
    },

    nameFunc: function (outcome) {
        if (outcome) {
            this.loadLevels();
        }
        else {
            this.errorLoadingFunc("Web Storage API is not supported!\n" +
                "You should use a newer browser!!!");
        }
    },

    /**
     * Displays loading images and manages the loading bar updates.
     */
    loadingLabel: function () {
        //Here we add a label to let the user know we are loading everything
        //This is the "Loading" phrase in pixel art
        //You can just as easily change it for your own art :)
        this.loading = window.game.add.sprite(window.game.world.centerX, window.game.world.centerY - 20, 'loading');
        this.loading.anchor.setTo(0.5, 0.5);
        //This is the bright blue bar that is hidden by the dark bar
        this.barBg = window.game.add.sprite(window.game.world.centerX, window.game.world.centerY + 40, 'load_progress_bar');
        this.barBg.anchor.setTo(0.5, 0.5);
        //This bar will get cropped by the setPreloadSprite function as the game loads!
        this.bar = window.game.add.sprite(window.game.world.centerX - 220, window.game.world.centerY + 40, 'load_progress_bar_dark');
        this.bar.anchor.setTo(0, 0.5);
        window.game.load.setPreloadSprite(this.bar);
    },

    /**
     * Loads menu assets.
     */
    loadAssetsMenu: function() {
        //Loading assets for the menu
        window.game.load.image('menu_title', 'src/images/menu_game_title.png');
        window.game.load.spritesheet('menu_button1', 'src/images/play_button.png', 208, 58);
        window.game.load.spritesheet('menu_button2', 'src/images/howto_button.png', 208, 58);
        window.game.load.spritesheet('menu_button3', 'src/images/credits_button.png', 208, 58);
        window.game.load.image('saveButton', 'src/images/save_button.png');
        window.game.load.image('cursor1', 'src/images/cursor1.png');
        window.game.load.image('cursor2', 'src/images/cursor2.png');
        window.game.load.spritesheet('quitButton', 'src/images/quit_button.png', 150, 75);
        window.game.load.image('speakerButton', 'src/images/speaker_button.png');
        window.game.load.image('musicButton', 'src/images/music_button.png');
        window.game.load.image('littleRedCross', 'src/images/little_red_cross.png');
        window.game.load.image('difficultyButton', 'src/images/difficulty_button.png');
        window.game.load.image('smallFire', 'src/images/small_fire.png');
        window.game.load.image('bigFire', 'src/images/big_fire.png');
        window.game.load.spritesheet('reloadButton', 'src/images/reload_button.png', 208, 58);
        window.game.load.image('sendButton', 'src/images/send_button.png');
    },

    /**
     * Loads assets needed to draw the Pda.
     */
    loadAssetsPda: function() {
        //PDA
        window.game.load.image('pda', 'src/images/pda.png');
        window.game.load.spritesheet('homeb', 'src/images/home_button.png', 70, 70);
        window.game.load.spritesheet('larrowb', 'src/images/left_arrow.png', 60, 60);
        window.game.load.spritesheet('rarrowb', 'src/images/right_arrow.png', 60, 60);
        window.game.load.image('hintBox', 'src/images/hint_box.png');
    },

    /**
     * Loads the assets used for the game 'How To' and the levels introductions
     */
    loadAssetsTutorial: function() {
        window.game.load.image('buttonA', 'src/images/button_A.png');
        window.game.load.image('buttonC', 'src/images/button_C.png');
        window.game.load.image('buttonD', 'src/images/button_D.png');
        window.game.load.image('buttonP', 'src/images/button_P.png');
        window.game.load.image('buttonH', 'src/images/button_H.png');
        window.game.load.image('spaceBar', 'src/images/spacebar.png');
        window.game.load.image('buttonNumbers1', 'src/images/button_numbers1.png');
        window.game.load.image('buttonNumbers2', 'src/images/button_numbers2.png');
        window.game.load.image('buttonCursors', 'src/images/cursor_keys.png');
        window.game.load.image('fwAlertText', 'src/images/fw_alert_text.png');
        window.game.load.image('redEllipse', 'src/images/redEllipse.png');
        window.game.load.image('logExample', 'src/images/log_example.png');
        window.game.load.image('tips', 'src/images/tips.png');
        window.game.load.image('considerAs', 'src/images/consider_as.png');
    },

    /**
     * Loads assets needed for the firewall notices.
     */
    loadAssetsNotice: function() {
        //Notice
        window.game.load.image('notice', 'src/images/notice.png');
        window.game.load.image('inArrow', 'src/images/ingoing_arrow.png');
        window.game.load.image('outArrow', 'src/images/outgoing_arrow.png');
        window.game.load.image('alertWhite', 'src/images/alert_icon_white.png');
        window.game.load.image('alertOrange', 'src/images/alert_icon_orange.png');

        //Notice queue bar
        window.game.load.spritesheet('redMark', 'src/images/red_mark.png', 20, 20);
        window.game.load.spritesheet('yellowMark', 'src/images/yellow_mark.png', 20, 20);
        window.game.load.spritesheet('blueMark', 'src/images/blue_mark.png', 20, 20);
    },

    /**
     * Loads level selection screen assets.
     */
    loadLevelSelection: function() {
        window.game.load.image('levelButton', 'src/images/level_button.png');
        window.game.load.image('shield', 'src/images/shield.png');
        window.game.load.image('shieldNotEarned', 'src/images/shield_notearned.png');
        window.game.load.image('redX', 'src/images/red_x.png');
    },

    /**
     * Loads assets used for the dialogue box.
     */
    loadNotificationAndDialogue: function() {
        window.game.load.image('alertBox', 'src/images/alert_box.png');
        window.game.load.image('notification', 'src/images/notification.png');
        window.game.load.image('dialogue_box', 'src/images/dialogue_box.png');
    },

    /**
     * Loads maps assets.
     */
    loadMapAssets: function() {
        window.game.load.spritesheet('agent', 'src/assets/images/agent.png', 32, 48);
        window.game.load.tilemap('level', 'src/assets/tilemaps/level.json', null, Phaser.Tilemap.TILED_JSON);
        window.game.load.image('floor_tile', 'src/assets/images/floor_tile.png');
        window.game.load.image('wall_tile', 'src/assets/images/wall.png');
        window.game.load.image('wall_pieces', 'src/assets/images/wallp.png');
        window.game.load.spritesheet('gate', 'src/assets/images/gate.png', 60, 64);
        window.game.load.image('mainframeup', 'src/assets/images/mainframeup.png');
        window.game.load.image('mainframedown', 'src/assets/images/mainframedown.png');
        window.game.load.image('mainframeupN', 'src/assets/images/mainframeupN.png');
        window.game.load.image('mainframedownN', 'src/assets/images/mainframedownN.png');
        window.game.load.image('mainframeupD', 'src/assets/images/mainframeupD.png');
        window.game.load.image('mainframedownD', 'src/assets/images/mainframedownD.png');
        window.game.load.image('doorJamb', 'src/assets/images/door_jamb.png');
        window.game.load.image('black', 'src/assets/images/black.png');
        window.game.load.image('invTile', 'src/assets/images/collision_tile.png');
        window.game.load.image('yellowLight', 'src/assets/images/yellow_light.png');
        window.game.load.spritesheet('bomb', 'src/assets/images/bomb.png', 50, 25);
    },

    /**
     * Loads characters assets.
     */
    loadCharacters: function() {
        window.game.load.spritesheet('concierge', 'src/assets/images/concierge.png', 32, 32);
        window.game.load.spritesheet('conciergeFace', 'src/assets/images/concierge_face.png', 96, 96);
        window.game.load.spritesheet('bossFace', 'src/assets/images/boss_face.png', 96, 96);
        window.game.load.spritesheet('visitor1', 'src/assets/images/visitor1.png', 32, 54);
        window.game.load.spritesheet('visitor2', 'src/assets/images/visitor2.png', 32, 49);
        window.game.load.spritesheet('visitor3', 'src/assets/images/visitor3.png', 32, 48);
        window.game.load.spritesheet('visitor4', 'src/assets/images/visitor4.png', 32, 48);
        window.game.load.spritesheet('visitor5', 'src/assets/images/visitor5.png', 32, 48);
        window.game.load.spritesheet('visitor6', 'src/assets/images/visitor6.png', 32, 48);
        window.game.load.spritesheet('pest1', 'src/assets/images/pest1.png', 32, 48);
        window.game.load.spritesheet('pest2', 'src/assets/images/pest2.png', 32, 48);
        window.game.load.spritesheet('pest3', 'src/assets/images/pest3.png', 32, 54);
        window.game.load.spritesheet('pest4', 'src/assets/images/pest4.png', 32, 48);
        window.game.load.spritesheet('enemy1', 'src/assets/images/enemy1.png', 32, 48);
        window.game.load.spritesheet('enemy2', 'src/assets/images/enemy2.png', 32, 48);
        window.game.load.spritesheet('enemy3', 'src/assets/images/enemy3.png', 32, 48);
        window.game.load.spritesheet('enemy4', 'src/assets/images/enemy4.png', 32, 48);
    },

    /**
     * Loads game effects and particles, such as smoke, fire, etc.
     */
    loadEffects: function() {
        window.game.load.image('smoke', 'src/assets/images/smoke.png');
        window.game.load.image('smoke2', 'src/assets/images/smoke2.png');
        window.game.load.spritesheet('hit1', 'src/assets/images/hit1.png', 55, 56);
        window.game.load.spritesheet('smallExplosion', 'src/assets/images/small_explosion.png', 22, 25);
        //window.game.load.spritesheet('disappearEffect', 'src/assets/images/disappear.png', 80, 105);
        window.game.load.spritesheet('disappearEffect', 'src/assets/images/smokeBomb.png', 130, 85);
        window.game.load.spritesheet('stunEffect', 'src/assets/images/stun.png', 27, 25);
        window.game.load.spritesheet('explosion1', 'src/assets/images/explosion1.png', 192, 160);
        window.game.load.spritesheet('explosion2', 'src/assets/images/explosion2.png', 75, 80);
        window.game.load.spritesheet('explosion3', 'src/assets/images/explosion3.png', 60, 60);
        window.game.load.spritesheet('explosion4', 'src/assets/images/explosion4.png', 130, 130);
        window.game.load.image('hole', 'src/assets/images/hole.png');
        window.game.load.spritesheet('electricity', 'src/assets/images/electricity.png', 38, 65);
        window.game.load.image('spark', 'src/assets/images/spark.png');
        window.game.load.image('white', 'src/assets/images/white.png');
        window.game.load.image('black', 'src/assets/images/black.png');
    },

    /**
     * Loads assets needed for the level log.
     */
    loadLog: function() {
        window.game.load.image('logButton', 'src/images/log_button.png');
        window.game.load.image('errorIcon', 'src/images/error_icon.png');
        window.game.load.image('warningIcon', 'src/images/warning_icon.png');
        window.game.load.image('correctIcon', 'src/images/correct_icon.png');
    },

    /**
     * Loads game sound effects
     */
    loadSounds: function () {
        window.game.load.audio('typing', ['src/sounds/typing.ogg', 'src/sounds/typing.mp3']);
        window.game.load.audio('buttonClick', ['src/sounds/button_click.ogg', 'src/sounds/button_click.mp3']);
        window.game.load.audio('buttonClick2', ['src/sounds/button_click2.ogg', 'src/sounds/button_click2.mp3']);
        window.game.load.audio('wrong', ['src/sounds/wrong.ogg', 'src/sounds/wrong.mp3']);
        window.game.load.audio('incomingCall', ['src/sounds/incoming_call.ogg', 'src/sounds/incoming_call.mp3']);
        window.game.load.audio('gateOpening', ['src/sounds/gate_opening.ogg', 'src/sounds/gate_opening.mp3']);
        window.game.load.audio('gateClosing', ['src/sounds/gate_closing.ogg', 'src/sounds/gate_closing.mp3']);
        window.game.load.audio('poof', ['src/sounds/poof.ogg', 'src/sounds/poof.mp3']);
        window.game.load.audio('mainframeHit', ['src/sounds/hit.ogg', 'src/sounds/hit.mp3']);
        window.game.load.audio('smallExplosion', ['src/sounds/small_explosion.ogg', 'src/sounds/small_explosion.mp3']);
        window.game.load.audio('bigExplosion', ['src/sounds/big_explosion.ogg', 'src/sounds/big_explosion.mp3']);
        window.game.load.audio('finalExplosion', ['src/sounds/final_explosion.ogg', 'src/sounds/final_explosion.mp3']);
        window.game.load.audio('punch', ['src/sounds/punch.ogg', 'src/sounds/punch.mp3']);
    },

    /**
     * Loads game sound tracks
     */
    loadMusic: function () {
        window.game.load.audio('track0', ['src/sounds/music/all_of_us.ogg', 'src/sounds/music/all_of_us.mp3']);
        window.game.load.audio('track1', ['src/sounds/music/night_dizzy_spells.ogg', 'src/sounds/music/night_dizzy_spells.mp3']);
        window.game.load.audio('track2', ['src/sounds/music/chibi_ninja.ogg', 'src/sounds/music/chibi_ninja.mp3']);
        window.game.load.audio('track3', ['src/sounds/music/come_find_me.ogg', 'src/sounds/music/come_find_me.mp3']);
        window.game.load.audio('track4', ['src/sounds/music/hhavok_main.ogg', 'src/sounds/music/hhavok_main.mp3']);
    },

    /**
     * Tries to load previous saved data. Does nothing if non existent
     */
    loadSaves: function() {
        loadSave.loadSaveData();
    },

    /**
     * Loads levels data and switches to the menu state when done.
     */
    loadLevels: function() {
        var levelNumber = 1;
        window.game.globals.levelsData = [];
        this.callbackLevels = function ()
        {
            if(this.levelReader.rawFile.readyState === 4)
            {
                if (this.levelReader.rawFile.status === 200 || this.levelReader.rawFile.status === 304) {
                    window.game.globals.levelsData[levelNumber - 1] = JSON.parse(this.levelReader.rawFile.responseText);
                    this.levelReader = new AjaxFileReader();
                    levelNumber += 1;
                    this.levelReader.readTextFile("src/contents/l" + levelNumber + "_level.json", this.callbackLevels.bind(this),
                        null);
                }
                else {
                    this.totalLevels = levelNumber - 1;
                    this.loadLevelsTutorials();
                }
            }
        };

        this.levelReader = new AjaxFileReader();
        this.levelReader.readTextFile("src/contents/l" + levelNumber + "_level.json", this.callbackLevels.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " main file!"));

    },

    /**
     * Loads the pre level tutorial contents
     */
    loadLevelsTutorials: function() {
        var levelNumber = 1;
        window.game.globals.levelTutorial = [];
        this.callbackLevelsTutorials = function ()
        {
            if(this.tutorialsReader.rawFile.readyState === 4)
            {
                if (this.tutorialsReader.rawFile.status === 200 || this.tutorialsReader.rawFile.status === 304) {
                    window.game.globals.levelTutorial[levelNumber - 1] = this.tutorialsReader.rawFile.responseText;
                    this.tutorialsReader = new AjaxFileReader();
                    if (levelNumber !== this.totalLevels) {
                        levelNumber += 1;
                        this.tutorialsReader.readTextFile("src/contents/l" + levelNumber + "_intro.txt", this.callbackLevelsTutorials.bind(this),
                            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " intro file!"));
                    }
                    else {
                        this.loadConsequences();
                    }
                }
                else {
                    this.errorLoadingFunc("Error while loading level " + levelNumber + " intro file!");
                }
            }
        };

        this.tutorialsReader = new AjaxFileReader();
        this.tutorialsReader.readTextFile("src/contents/l" + levelNumber + "_intro.txt", this.callbackLevelsTutorials.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " intro file!"));
    },

    /**
     * Loads messages displayed after a reply to a notice.
     */
    loadConsequences: function() {
        var levelNumber = 1;
        window.game.globals.cons = [];
        this.callbackCons = function ()
        {
            if(this.consReader.rawFile.readyState === 4)
            {
                if (this.consReader.rawFile.status === 200 || this.consReader.rawFile.status === 304) {
                    window.game.globals.cons[levelNumber - 1] = JSON.parse(this.consReader.rawFile.responseText);
                    this.consReader = new AjaxFileReader();
                    if (levelNumber !== this.totalLevels) {
                        levelNumber += 1;
                        this.consReader.readTextFile("src/contents/l" + levelNumber + "_cons.json", this.callbackCons.bind(this),
                            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " cons file!"));
                    }
                    else {
                        this.loadDialogues();
                    }
                }
                else {
                    this.errorLoadingFunc("Error while loading level " + levelNumber + " cons file!");
                }
            }
        };

        this.consReader = new AjaxFileReader();
        this.consReader.readTextFile("src/contents/l" + levelNumber + "_cons.json", this.callbackCons.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " cons file!"));
    },

    /**
     * Loads the concierge story dialogue for each level
     */
    loadDialogues: function() {
        var levelNumber = 1;
        window.game.globals.storyDialogue = [];
        this.callbackDialogue = function ()
        {
            if(this.dialogueReader.rawFile.readyState === 4)
            {
                if (this.dialogueReader.rawFile.status === 200 || this.dialogueReader.rawFile.status === 304) {
                    window.game.globals.storyDialogue[levelNumber - 1] = this.dialogueReader.rawFile.responseText;
                    this.dialogueReader = new AjaxFileReader();
                    if (levelNumber !== this.totalLevels) {
                        levelNumber += 1;
                        this.dialogueReader.readTextFile("src/contents/l" + levelNumber + "_dialogue.txt", this.callbackDialogue.bind(this),
                            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " dialogue file!"));
                    }
                    else {
                        this.loadOutros();
                    }
                }
                else {
                    this.errorLoadingFunc("Error while loading level " + levelNumber + " dialogue file!");
                }
            }
        };

        this.dialogueReader = new AjaxFileReader();
        this.dialogueReader.readTextFile("src/contents/l" + levelNumber + "_dialogue.txt", this.callbackDialogue.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " dialogue file!"));
    },

    /**
     * Loads the concierge story dialogue for each level
     */
    loadOutros: function() {
        var levelNumber = 1;
        window.game.globals.outros = [];
        this.callbackOutros = function ()
        {
            if(this.outrosReader.rawFile.readyState === 4)
            {
                if (this.outrosReader.rawFile.status === 200 || this.outrosReader.rawFile.status === 304) {
                    window.game.globals.outros[levelNumber - 1] = this.outrosReader.rawFile.responseText;
                    this.outrosReader = new AjaxFileReader();
                    if (levelNumber !== this.totalLevels) {
                        levelNumber += 1;
                        this.outrosReader.readTextFile("src/contents/l" + levelNumber + "_outro.txt", this.callbackOutros.bind(this),
                            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " outro file!"));
                    }
                    else {
                        this.loadHowto();
                    }
                }
                else {
                    this.errorLoadingFunc("Error while loading level " + levelNumber + " outro file!");
                }
            }
        };

        this.outrosReader = new AjaxFileReader();
        this.outrosReader.readTextFile("src/contents/l" + levelNumber + "_outro.txt", this.callbackOutros.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading level " + levelNumber + " outro file!"));
    },

    /**
     * Loads tutorial contents.
     */
    loadHowto: function() {
        this.callbackHowto = function ()
        {
            if(this.howtoReader.rawFile.readyState === 4)
            {
                if (this.howtoReader.rawFile.status === 200 || this.howtoReader.rawFile.status === 304) {
                    window.game.globals.howtoContent = this.howtoReader.rawFile.responseText;
                    this.loadCredits();
                }
                else {
                    this.errorLoadingFunc("Error while loading howto file!");
                }
            }
        };
        this.howtoReader = new AjaxFileReader();
        this.howtoReader.readTextFile("src/contents/howto.txt", this.callbackHowto.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading howto file!"));
    },

    /**
     * Loads credits contents.
     */
    loadCredits: function() {
        this.callbackCredits = function ()
        {
            if(this.creditsReader.rawFile.readyState === 4)
            {
                if (this.creditsReader.rawFile.status === 200 || this.creditsReader.rawFile.status === 304) {
                    window.game.globals.creditsContent = this.creditsReader.rawFile.responseText;
                    this.loadConsT();
                }
                else {
                    this.errorLoadingFunc("Error while loading credits file!");
                }
            }
        };
        this.creditsReader = new AjaxFileReader();
        this.creditsReader.readTextFile("src/contents/credits.txt", this.callbackCredits.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading credits file!"));
    },

    /**
     * Loads the "Consider as" messages.
     */
    loadConsT: function() {
        this.callbackConsT = function ()
        {
            if(this.consThowtoReader.rawFile.readyState === 4)
            {
                if (this.consThowtoReader.rawFile.status === 200 || this.consThowtoReader.rawFile.status === 304) {
                    window.game.globals.consTContent = JSON.parse(this.consThowtoReader.rawFile.responseText);
                    window.game.globals.music.init(this.launch);
                }
                else {
                    this.errorLoadingFunc("Error while loading consT file!");
                }
            }
        };
        this.consThowtoReader = new AjaxFileReader();
        this.consThowtoReader.readTextFile("src/contents/consT.json", this.callbackConsT.bind(this),
            this.errorLoadingFunc.bind(this, "Error while loading consT file!"));
    },

    /**
     * Starts the music and switches to the main menu state.
     */
    launch: function () {
        window.game.globals.music.play();
        window.game.state.start('menu');
    },

    /**
     * Launches the error state if a fatal error has occurred during game loading.
     * @param {string} message - String containing the error message
     */
    errorLoadingFunc: function (message) {
        if (!this.errorMessage) {
            this.errorMessage = message;
            window.game.state.start('error', true, false, this.errorMessage);
        }
    }
};
