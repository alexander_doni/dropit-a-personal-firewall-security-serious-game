/* global Phaser */
var NoticeManager = require('../modules/classes/NoticeManager')();
var tileFunctions = require('../modules/tile_functions');
var Dialogue = require('../modules/classes/Dialogue')();
var Visitor = require('../modules/classes/Visitor')();
var mainframeEffects = require('../modules/mainframes_effects');
var movement = require('../modules/visitor_movement');

/**
 * This is the state that handles the last level. It takes care of coordinating all the sub-modules
 * needed in order to ensure a correct gameplay.
 * @type {{blinkingTextTimer: number, noticeManager: Array, responseSignal: Array, signalOver: Array, mainframeTexture: boolean, visitors: Array, pauseButtonStatus: boolean, init: module.exports.init, create: module.exports.create, storyFunc: module.exports.storyFunc, update: module.exports.update, showNotice: module.exports.showNotice, createMap: module.exports.createMap, createPlayer: module.exports.createPlayer, animateMainframes: module.exports.animateMainframes, destroyMainframe: module.exports.destroyMainframe, getRandomIntInclusive: module.exports.getRandomIntInclusive, animResponse: module.exports.animResponse, yellowFunc: module.exports.yellowFunc, openGate: module.exports.openGate, closeGate: module.exports.closeGate, pauseFunc: module.exports.pauseFunc, pauseClick: module.exports.pauseClick, unpauseFunc: module.exports.unpauseFunc, pauseUpdate: module.exports.pauseUpdate, incFunc: module.exports.incFunc, endLevel: module.exports.endLevel, shutdown: module.exports.shutdown}}
 * @module
 * @name State: Play End
 */
module.exports = {
    /**
     * The array containing the NoticeManager objects.
     * @member {Array} noticeManager
     */
    noticeManager: [],

    /**
     * The array of signals the, when dispatched, indicate that the player replied to a notice.
     * @member {Array} responseSignal
     */
    responseSignal: [],

    /**
     * The array of signals that, when dispatched, indicate that a gate is available again, that is
     * a notice can be replied again on that gate.
     * @member {Array} signalOver
     */
    signalOver: [],

    /**
     * The array containing the Visitor objects on screen.
     * @member {Array} visitors
     */
    visitors: [],

    /**
     * A flag indicating if the mouse is over the unpause button or not in order to activate the button
     * over effect.
     * @member {boolean} pauseButtonStatus
     */
    pauseButtonStatus: false,

    /**
     * Flag indicating if the play can exit the stage. If this is true, the player collision with the central
     * upper gate will make the player leave the game stage.
     * @member {boolean} playerCanExit
     */
    playerCanExit: false,

    /**
     * Array of the used keyboard keys.
     * @member {Array} keysToCapture
     */
    keysToCapture: [Phaser.Keyboard.A, Phaser.Keyboard.D, Phaser.Keyboard.C, Phaser.Keyboard.LEFT,
        Phaser.Keyboard.RIGHT, Phaser.Keyboard.UP, Phaser.Keyboard.DOWN, Phaser.Keyboard.SPACEBAR],

    /**
     * It initializes the state knowing the level number. During the initialization, the NoticeManger objects
     * are created. Moreover, the data in the level definition file previously loaded are saved into a state
     * attribute.
     * @param {int} levelNumber - The level number identifying the level currently being played
     */
    init: function (levelNumber) {
        //Creates signal dispatched when end level conditions have to be checked
        this.signalLevelOver = new Phaser.Signal();
        this.signalLevelOver.add(this.endLevel, this);

        //Creates signal dispatched when a mainframe has been destroyed
        this.signalMainframeDestroyed = new Phaser.Signal();
        this.signalMainframeDestroyed.add(this.destroyMainframe, this);

        //Creates signal dispatched when a story dialogue has been completely read
        this.signalStoryDialogueOver = new Phaser.Signal();

        //Creates signal dispatched when the yellow lights above the upper gates have to be toggled
        this.signalYellow = new Phaser.Signal();
        this.signalYellow.add(this.yellowFunc, this);

        //Reads level defintion data
        this.levelData = window.game.globals.levelsData[levelNumber].connections;
        this.helpMessage = window.game.globals.levelsData[levelNumber].helpMessage;
        this.levelNumber = levelNumber;
        this.noticesReplied = 0;
        this.totalNotices = this.levelData.length;
        this.yellowLights = [{}, {}, {}];

        //Initializes level statistics object
        this.stats = {score: 0, rb: 0, ra: 0, wb: 0, wa: 0};
        for (var i = 0; i < 6; i++) {
            //Creates signal dispatched when a player replied to a firewall notice
            this.responseSignal[i] = new Phaser.Signal();

            //Creates signal dispatched when a gate is available again
            this.signalOver[i] = new Phaser.Signal();

            this.responseSignal[i].add(this.animResponse, this);

            //Creates NoticeManager objects
            this.noticeManager[i] = new NoticeManager(this.responseSignal[i], i, this.incFunc, this,
                this.levelNumber,this.signalYellow, this.signalOver[i], this.signalLevelOver);
        }
    },

    /**
     * Creates the game stage and starts the story dialogue.
     */
    create: function(){

        this.skyeyeActivation = 0;
        this.createMap();
        this.createPlayer();

        this.gateOpeningSound = window.game.add.audio("gateOpening");
        this.gateClosingSound = window.game.add.audio("gateClosing");

        //Group needed to correctly layer the sprites created after the dialogue
        this.lowerZGroup = window.game.add.group();

        this.signalStoryDialogueOver.add(this.storyFunc, this);

        //Creates the objects related to both the concierge dialogue and the boss one
        this.conciergeDialogue = new Dialogue(null, this.player, this.levelNumber, this.signalStoryDialogueOver, 'conciergeFace');
        this.bossDialogue = new Dialogue(this.helpMessage, this.player, this.levelNumber, this.signalStoryDialogueOver, 'bossFace');

        this.pauseKey = window.game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.pauseKey.onDown.addOnce(this.pauseFunc, this);

        window.game.input.keyboard.addKeyCapture(this.keysToCapture);
        this.conciergeDialogue.startLevelDialogue(true);
    },

    /**
     * Executed at each frame. It takes care of checking collisions, handling player's movement, and
     * checking for notices pop conditions.
     */
    update: function() {
        this.player.body.velocity.y = 0;
        this.player.body.velocity.x = 0;

        //Collisions with walls
        window.game.physics.arcade.collide(this.player, this.blockedLayer);
        //Collisions with mainframes
        window.game.physics.arcade.collide(this.player, this.mainframesup);
        //Collisions with door jambs, that is lower gates
        window.game.physics.arcade.collide(this.player, this.doorJambs);
        //Collisions with the bomb, if it has been placed
        if (this.bomb) {
            window.game.physics.arcade.collide(this.player, this.bomb);
        }

        //Checks if a player is in front of one of the upper gates to make the related notice appear
        this.collision = window.game.physics.arcade.overlap(this.player, this.gates, this.showNotice, null, this);
        if(this.noticeShown && !this.collision) {
            this.noticeManager[Number(this.noticeShown) - 1].hide();
            this.noticeShown = 0;
        }

        //Checks if a player is in front of one of the lower gates to make the related notice appear
        this.collisionLower = window.game.physics.arcade.overlap(this.player, this.collisionTiles, this.showNotice, null, this);
        if(this.noticeShownLower && !this.collisionLower) {
            this.noticeManager[Number(this.noticeShownLower) - 1].hide();
            this.noticeShownLower = 0;
        }

        //Handles player's movement depending on the keys pressed
        if(this.cursors.up.isDown) {
            //Up
            this.player.body.velocity.y -= 125;
            this.player.animations.play('up');
        }
        else if(this.cursors.down.isDown) {
            //Down
            this.player.body.velocity.y += 125;
            this.player.animations.play('down');
        }
        else if(this.cursors.left.isDown) {
            //Left
            this.player.body.velocity.x -= 125;
            this.player.animations.play('left');
        }
        else if(this.cursors.right.isDown) {
            //Right
            this.player.body.velocity.x += 125;
            this.player.animations.play('right');
        }
        else
        {
            // Stand still
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            this.player.animations.stop();
            this.player.frame = 0;
        }
    },

    /**
     * Shows a firewall notice when the player is in front of  an active gate. Additionally, if the
     * playerCanExit flag is set to true, it launches the player exit function.
     * @param {Phaser.Sprite} player - The player sprite object
     * @param {Phaser.Sprite} gateOrTile - It is the sprite with which the player has overlapped. It is
     * either a gate sprite, for upper gates, or the invisible tile sprite, for lower gates
     */
    showNotice: function (player, gateOrTile) {
        if(this.noticeManager[gateOrTile.number].notice) {
            if (!this.noticeManager[gateOrTile.number].noticeOn) {
                this.noticeManager[gateOrTile.number].show();
                //Used as true/false flag as well as the gate number
                if (gateOrTile.number < 3) {
                    this.noticeShown = Number(gateOrTile.number) + 1;
                }
                else {
                    this.noticeShownLower = Number(gateOrTile.number) + 1;
                }
            }
        }

        //In case the player can exit the main stage and he is in front of the central upper gate
        if (this.playerCanExit && Number(gateOrTile.number) === 1) {
            //Launches the appropriate end function depending n the end type that the player has achieved
            if (this.endType === "goodEnd") {
                this.playerExitGood();
            }
            else {
                this.playerExitBad();
            }
        }
    },

    /**
     * Creates the game stage.
     */
    createMap: function() {
        this.map = window.game.add.tilemap('level', 32, 32);

        //Imports the tileset image in the map object
        //The first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        this.map.addTilesetImage('floor', 'floor_tile');
        this.map.addTilesetImage('wall', 'wall_tile');
        this.map.addTilesetImage('wallp', 'wall_pieces');
        this.map.addTilesetImage('black', 'black');

        //Creates layer
        this.backgroundLayer = this.map.createLayer('floor');
        this.blockedLayer = this.map.createLayer('stuff');

        //Sets collision on blockedLayer
        this.map.setCollisionBetween(1, 7, true, 'stuff');
        this.map.setCollisionBetween(10, 20, true, 'stuff');

        //Resizes the game world to match the layer dimensions
        this.backgroundLayer.resizeWorld();

        //Creates and draws the door jambs (lower gates)
        this.doorJambs = tileFunctions.createObjects('doorJamb', this.map, 0.5);
        //Avoids the player from being able to move them
        this.doorJambs.setAll('body.immovable', true);

        //Creates and draws the upper gates
        this.gates = tileFunctions.createObjects('gate', this.map, 2);
        //Avoids the player from being able to move them
        this.gates.setAll('body.immovable', true);

        //Adds the animation to the upper gates
        this.gates.callAll('animations.add', 'animations', 'open', [0, 1, 2, 3], 2, false);
        this.gates.callAll('animations.add', 'animations', 'close', [3, 2, 1, 0], 2, false);

        //Creates the yellowLight objects
        this.yellowLights.forEach(function (obj, index) {
            this.yellowLights[index] = window.game.add.image(68 + 322*index, 42, 'yellowLight');
            this.yellowLights[index].visible = false;
        }, this);

        //Creates and draws the mainframes
        this.mainframesup = tileFunctions.createObjects('mainframeup', this.map, 2);
        this.mainframesup.setAll('body.immovable', true);
        this.mainframesdown = tileFunctions.createObjects('mainframedown', this.map, 2);
        this.mainframesdown.setAll('body.immovable', true);
        this.mainframesdown.mainframesLeft = 4;

        //Creates some invisible tiles to check for collisions to show notices for the lower gates
        this.collisionTiles = tileFunctions.createObjects('invTile', this.map, 2);
        this.collisionTiles.setAll('body.immovable', true);

        //Activates the timer that manages the mainframes animation to simulate them to be on
        this.mainframesTimer = window.game.time.create(window.game, false);
        this.mainframesTimer.loop(1000, this.animateMainframes, this);
        this.mainframesTimer.start();

        //Keys reminder
        this.styleReminder = { font: "15px Courier New, monospace", fill: "#00AA11", align: "left"};
        this.buttonA = window.game.add.sprite(22, 565, 'buttonA');
        this.buttonA.height = 30;
        this.buttonA.width = 30;
        window.game.add.text(60, 570, 'Allow Request', this.styleReminder);

        this.buttonD = window.game.add.sprite(222, 565, 'buttonD');
        this.buttonD.height = 30;
        this.buttonD.width = 30;
        window.game.add.text(260, 570, 'Drop Request', this.styleReminder);

        this.buttonC = window.game.add.sprite(422, 565, 'buttonC');
        this.buttonC.height = 30;
        this.buttonC.width = 30;
        window.game.add.text(460, 570, 'Consider As', this.styleReminder);

        //SkyEye activation text with percentage
        this.skyeyeActivationText = window.game.add.text(575, 570, 'SkyEye Activation: ' + this.skyeyeActivation + '%',
            this.styleReminder);
    },

    /**
     * Creates and draws the player sprite object.
     */
    createPlayer: function() {
        this.player = window.game.add.sprite(100, 100, 'agent');
        this.player.anchor.set(0, 0);
        window.game.physics.arcade.enable(this.player);

        //Sets the player's movement through the cursor keys
        this.player.animations.add('down', [0, 1, 2, 3], 10, true);
        this.player.animations.add('left', [4, 5, 6, 7], 10, true);
        this.player.animations.add('right', [8, 9, 10, 11], 10, true);
        this.player.animations.add('up', [12, 13, 14, 15], 10, true);
        this.cursors = window.game.input.keyboard.createCursorKeys();
    },

    /**
     * Animates the mainframe by changing some of their parts to make them look active.
     */
    animateMainframes: function() {
        //Takes to random mainframes and changes their appearance
        var randIndex = this.getRandomIntInclusive(0, this.mainframesup.length-1);
        var randIndex1 = (randIndex + 1) % this.mainframesup.length;

        //Does not change them if they are destroyed
        if (this.mainframesup.getChildAt(randIndex).key !== "mainframeupD") {
            //Toggles the texture depending on the current one
            if (this.mainframesup.getChildAt(randIndex).key === "mainframeup") {
                this.mainframesup.getChildAt(randIndex).loadTexture("mainframeupN");
                this.mainframesdown.getChildAt(randIndex).loadTexture("mainframedownN");
            }
            else {
                this.mainframesup.getChildAt(randIndex).loadTexture("mainframeup");
                this.mainframesdown.getChildAt(randIndex).loadTexture("mainframedown");
            }
        }

        //Does not change them if they are destroyed
        if (this.mainframesup.getChildAt(randIndex1).key !== "mainframeupD") {
            //Toggles the texture depending on the current one
            if (this.mainframesup.getChildAt(randIndex1).key === "mainframeup") {
                this.mainframesup.getChildAt(randIndex1).loadTexture("mainframeupN");
                this.mainframesdown.getChildAt(randIndex1).loadTexture("mainframedownN");
            }
            else {
                this.mainframesup.getChildAt(randIndex1).loadTexture("mainframeup");
                this.mainframesdown.getChildAt(randIndex1).loadTexture("mainframedown");
            }
        }
    },

    /**
     * Changes the texture of a mainframe when it is destroyed. Called when the signalMainframeDestroyed
     * is dispatched.
     * @param {Phaser.Sprite} mainframeDown - The lower part of the mainframe that has been destroyed.
     */
    destroyMainframe: function(mainframeDown) {
        //Obtains the index of the lower part to be able to reference the upper part, since they have the same index.
        var index = this.mainframesdown.getChildIndex(mainframeDown);
        mainframeDown.loadTexture("mainframedownD");
        this.mainframesup.getChildAt(index).loadTexture("mainframeupD");
    },

    /**
     * Picks a random integer between min (included) and max (excluded)
     * @param {int} min - Min value that the generated number can have
     * @param {int} max - Max value. The generated number can be at most max-1
     * @returns {int} The generated number.
     */
    getRandomIntInclusive: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    /**
     * Called when the player replies to a notice to handle the effects of the reply.
     * @param {Object} outcome - Contains information about the player's reply
     * @param {int} gateNumber - The number of the gate on which the player replied to
     * @param {string} optionalTalkType - (Optional) Identifies the facial expression to use in the dialogue.
     * Used when the player uses the "Consider as" functionality to take the key string from the consT file data
     */
    animResponse: function(outcome, gateNumber, optionalTalkType) {
        //Starts the concierge explanation dialogue
        this.bossDialogue.popup(outcome.message, (optionalTalkType) ? optionalTalkType : outcome.type);

        //If a correct choice has been made, the SkyEye activation percentage is increased and the related
        //text is updated
        if (outcome.type === "correctDrop" || outcome.type === "correctAllow") {
            this.skyeyeActivation += 10;
            this.skyeyeActivationText.setText("SkyEye Activation: " + this.skyeyeActivation + "%");
        }

        //If an error has been made, lowers the number of alive mainframes
        if (outcome.type === "error") {
            this.mainframesdown.mainframesLeft--;
        }

        //If the request has been allowed, launches the function to open the corresponding gate and launch
        //the appropriate effect
        if (outcome.type !== "correctDrop" && outcome.type !== "wrongDrop") {
            this.openGate(gateNumber, outcome);
        }

        //If SkyEye activation percentage reaches 100% or all the mainframes have been destroyd, the notice managers
        //are disabled to prevent the player from further reply to other firewall notices
        if (this.skyeyeActivation === 100 || this.mainframesdown.mainframesLeft === 0) {
            this.disableNoticeManagers();
        }
    },

    /**
     * Called when the signalYellow is dispatched to toggle the yellow light on the corresponding gate.
     * It is only valid for upper gates.
     * @param {int} gateNumber - The gate number
     * @param {boolean} visible - Indicates if the yellow light has to be switched on (true) or off (false)
     */
    yellowFunc: function (gateNumber, visible) {
        //Only valid for upper gates
        if (gateNumber < 3) {
            this.yellowLights[gateNumber].visible = visible;
        }
    },

    /**
     * Opens a gate and launches the appropriate effect.
     * @param {int} gateNumber - The number of the gate on which the player replied to
     * @param {Object} outcome - Contains information about the player's reply
     */
    openGate: function(gateNumber, outcome) {
        //Checks whether it is an upper gate or a lower one
        if (gateNumber < 3) {
            //Sets a flag that indicates that on this gate an animation is in progress
            //Useful to prevent the player from responding to other notices while this is true
            this.gates.getChildAt(gateNumber).animInProgress = true;

            //Switches off the yellow light, effectively making it red
            this.yellowFunc(gateNumber, false);

            //Creates the opening animation
            this.openingAnim = this.gates.getChildAt(gateNumber).animations.getAnimation('open');
            this.openingAnim.onStart.addOnce(function () {
                this.gateOpeningSound.play("", 0, window.game.globals.soundEffectsOn);
            }, this);

            //Starts the opening animation
            this.openingAnim.play('open', 2);
            this.openingAnim.onComplete.addOnce(function() {
                //Creates a visitor once the gate has been completely opened
                this.visitors[gateNumber] = new Visitor(this.gates.getAt(gateNumber), outcome.type, this.mainframesdown,
                    this.lowerZGroup, this.signalOver[gateNumber], this.signalLevelOver, this.signalMainframeDestroyed, this.player);

                //Calls the close gate function when the visitor has entered
                this.closeGate(gateNumber);
            }, this);
        }
        else {
            this.doorJambs.getChildAt(gateNumber-3).animInProgress = true;

            //Starts the opening sound
            this.gateOpeningSound.play("", 0, window.game.globals.soundEffectsOn).onStop.addOnce(function () {
                //Creates a visitor once the opening sound has finished
                this.visitors[gateNumber] = new Visitor(this.doorJambs.getAt(gateNumber-3), outcome.type, this.mainframesdown,
                    this.lowerZGroup, this.signalOver[gateNumber], this.signalLevelOver, this.signalMainframeDestroyed, this.player);
                //Starts the closing sound when the visitor has entered
                this.gateClosingSound.play("", 0, window.game.globals.soundEffectsOn);
            }, this);
        }
    },

    /**
     * Closes the gate passed as parameter.
     * @param {int} gateNumber - Identifies which gate has to be closed
     */
    closeGate: function(gateNumber) {
        this.closingAnim = this.gates.getChildAt(gateNumber).animations.getAnimation('close');
        this.closingAnim.onStart.addOnce(function () {
            this.gateClosingSound.play("", 0, window.game.globals.soundEffectsOn);
        }, this);
        this.closingAnim = this.gates.getChildAt(gateNumber).play('close', 2);
    },

    /**
     * Pauses the game.
     */
    pauseFunc: function() {
        //Creates the pause box if this is the first time that the game has been paused in the current level
        if (!this.pauseBox) {
            this.pauseBox = window.game.add.sprite(window.game.world.centerX, window.game.world.centerY, 'alertBox');
            this.pauseBox.anchor.set(0.5, 0.5);
            // Adding quit button
            this.toMenuButton = window.game.add.sprite(400, 350, 'quitButton');
            this.toMenuButton.anchor.setTo(0.5, 0.5);
            // Calculating the corners of the quit button
            this.buttX1 = this.toMenuButton.x - this.toMenuButton.width / 2;
            this.buttX2 = this.toMenuButton.x + this.toMenuButton.width / 2;
            this.buttY1 = this.toMenuButton.y - this.toMenuButton.height / 2;
            this.buttY2 = this.toMenuButton.y + this.toMenuButton.height / 2;
            this.toMenuButton.visible = false;
        }

        //Creates and starts the tween of the pause box
        window.game.add.tween(this.pauseBox.scale).from({x: 0, y:0},500,Phaser.Easing.Linear.None.In,true, 0, 0);
        this.pauseEntryTween = window.game.add.tween(this.pauseBox).from({alpha: 0},500,Phaser.Easing.Linear.None,true, 0, 0);
        this.pauseEntryTween.onComplete.addOnce(function() {
            window.game.input.onDown.add(this.pauseClick, this);
            this.toMenuButton.visible = true;


            //Menu label
            this.choiceLabel = window.game.add.text(400, 265, 'Click outside the pause screen to continue', { font: '27px Arial', fill: '#00AA11', align: "center", wordWrap: true, wordWrapWidth: this.pauseBox.x - 45});
            this.choiceLabel.anchor.setTo(0.5, 0.5);

            //Tells the game engine that the game has to enter the paused state
            window.game.paused = true;
        }, this);
    },

    /**
     * Handles the mouse click events when the game is paused.
     * @param {Phaser.Pointer} event - Containins the information about the clicked location
     */
    pauseClick: function (event) {
        // Check if the menu button has been clicked
        if( (event.x > this.buttX1 && event.x < this.buttX2 && event.y > this.buttY1 && event.y < this.buttY2) ) {
            //Coming back to menu screen
            this.toMenuButton.destroy();
            this.choiceLabel.destroy();
            window.game.input.onDown.remove(this.pauseClick, this);
            window.game.paused = false;
            window.game.state.start('menu');
        }
        //Checks if a location outside the pause box has been clicked. If that is the case, unpause the game.
        else if (event.x < this.pauseBox.x - this.pauseBox.width/2 || event.x > this.pauseBox.x + this.pauseBox.width/2 ||
            event.y > this.pauseBox.y + this.pauseBox.height/2 || event.y < this.pauseBox.y - this.pauseBox.height/2) {
            this.unpauseFunc();
        }

        //Otherwise, do nothing
    },

    /**
     * Unpauses the game.
     */
    unpauseFunc: function() {
        //Remove the pause box and the unpause button
        this.toMenuButton.destroy();
        this.choiceLabel.destroy();
        this.pauseBox.destroy();
        delete this.pauseBox;
        window.game.input.onDown.remove(this.pauseClick, this);
        this.pauseKey.onDown.addOnce(this.pauseFunc, this);

        //Tells the game engine that the game has to exit the paused state
        window.game.paused = false;
    },

    /**
     * It is executed at each frame while the game is paused. It is used to animate the unpause button
     * and to handle the pause key press. If the pause key is pressed, the game is unpaused.
     */
    pauseUpdate: function() {
        if (this.toMenuButton) {
            if (window.game.input.mousePointer.x > this.buttX1 && window.game.input.mousePointer.x < this.buttX2 && window.game.input.mousePointer.y > this.buttY1 && window.game.input.mousePointer.y < this.buttY2) {
                this.toMenuButton.frame = 1;
                this.pauseButtonStatus = true;
            }
            else if (this.pauseButtonStatus) {
                this.pauseButtonStatus = false;
                this.toMenuButton.frame = 0;
            }

            if (this.pauseKey.isDown) {
                this.unpauseFunc();
            }
        }
    },

    /**
     * Updates the game score and the level statistics once the player replies to a notice.
     * @param {int} code - It specifies how the player replied to a notice
     */
    incFunc: function (code) {
        this.noticesReplied++;

        switch (code) {
            case 0: //CorrectAllow
                this.stats.ra++;
                this.stats.score += 100;
                break;
            case 1: //CorrectDrop
                this.stats.rb++;
                this.stats.score += 100;
                break;
            case 2: //WrongAllow
                this.stats.wa++;
                this.stats.score -= 100;
                break;
            case 3: //WrongDrop
                this.stats.wb++;
                this.stats.score -= 75;
                break;
            case 4: //AutoDrop
                this.stats.wb++;
                switch (window.game.globals.difficulty) {
                    case 0: //Easy
                        this.stats.score -= 0;
                        break;
                    case 1: //Medium
                        this.stats.score -= 50;
                        break;
                    case 2: //Hard
                        this.stats.score -= 100;
                        break;
                }
                break;
        }
    },

    /**
     * Launched when the level start or the level end story dialogue have been read.
     * @param {boolean} isStartLevel - Flag indicating whether the story dialogue that has been read is the
     * level start or the level end one
     * @param {boolean} isConciergeDialogue - Indicates that the end level dialogue was from the concierge (true)
     * or from the boss (false)
     */
    storyFunc: function(isStartLevel, isConciergeDialogue) {
        //Enters if one of the two level start dialogue have been read
        if (isStartLevel) {
            //Enters if only the concierge dialogue has been read
            if (!this.bossDialogueDone) {
                this.bossDialogueDone = true;
                this.bossDialogue.startLevelDialogue(false);
            }
            //enters if also the boss dialogue has been read
            else {
                this.logEntries = [];

                //Creates one Notice object for each connection present in the level definition file
                for (var i = 0; i < this.levelData.length; i++) {
                    this.noticeManager[this.levelData[i].gate].createNotice(this.levelData[i], this.logEntries, this.lowerZGroup);
                }

                //Computes maxScore achievable in the current level
                this.maxScore = this.levelData.length * 100;

                //Starts the first timer to make the first request appear for all the notice managers
                for (var j = 0; j < 6; j++) {
                    this.noticeManager[j].startTimer();
                }
            }
        }
        //Enters if the level is over and the good ending has been achieved
        else if (isConciergeDialogue) {
            //Opens the central upper gate to let the concierge enter
            var centralGate = this.gates.getChildAt(1);
            this.yellowFunc(1, false);
            this.openingAnim = this.gates.getChildAt(1).animations.getAnimation('open');
            this.openingAnim.onStart.addOnce(function () {
                this.gateOpeningSound.play("", 0, window.game.globals.soundEffectsOn);
            }, this);
            this.openingAnim.play(2, false , false);
            this.openingAnim.onComplete.addOnce(function() {
                //Creates the concierge sprite, activates its physics and adds the walk animations
                this.concierge = window.game.add.sprite(centralGate.position.x + centralGate.width/2,
                    centralGate.position.y + centralGate.height, 'concierge');
                this.concierge.anchor.setTo(0.5, 0.5);
                window.game.physics.enable(this.concierge, Phaser.Physics.ARCADE);
                this.concierge.body.collideWorldBounds = true;
                this.concierge.body.moves = false;
                this.concierge.animations.add('down', [0, 1, 2], 10, true);
                this.concierge.animations.add('left', [3, 4, 5], 10, true);
                this.concierge.animations.add('right', [6, 7, 8], 10, true);
                this.concierge.animations.add('up', [9, 10, 11], 10, true);

                //Moves the concierge to the center of the room
                var tween = movement.moveTo(this.concierge, 1, 7, true);

                tween.onComplete.add(function () {
                    this.concierge.animations.stop();
                    this.concierge.frame = 1;

                    //Starts a concierge dialogue
                    this.tempSignal = new Phaser.Signal();
                    this.dialogueVett = [{message: "Finally. This is the important mainframe room then.", talkType:"correctAllow"},
                        {message: "Anyway, as you can imagine, I am not a concierge nor a cyber-terrorist.", talkType:"correctAllow"},
                        {message: "I work for the IIS, the Independent Intelligence Service.", talkType:"correctAllow"},
                        {message: "We suspected that MoveTheNet was up to something, so I have been sent here.", talkType:"correctAllow"},
                        {message: "However, it is only thanks to you that we discovered their plans.", talkType:"correctAllow"},
                        {message: "And you also gave us the possibility to stop them.", talkType:"correctAllow"},
                        {message: "We would like to have you with us, but we will talk about this later.", talkType:"correctAllow"},
                        {message: "Now it is time to blow this place up to destroy 'SkyEye' servers!", talkType: "correctDrop"},
                        {message: "Run!!!!", talkType: "error"}];
                    this.conciergeDialogue.normalDialogue(this.dialogueVett, this.tempSignal);
                    this.tempSignal.add(function () {
                        //Places the bomb
                        this.bomb = window.game.add.sprite(this.concierge.position.x, this.concierge.position.y + 20, 'bomb');
                        this.bomb.anchor.setTo(0.5, 0.5);
                        window.game.physics.enable(this.bomb, Phaser.Physics.ARCADE);
                        this.bomb.body.immovable = true;

                        //Adds and activates the bomb animation
                        this.bomb.animations.add('triggered', [0, 1], 4, true);
                        this.bomb.animations.play('triggered');

                        //Moves the concierge out from the central upper gate
                        var tweenExit = movement.moveTo(this.concierge, 7, 1, false);
                        tweenExit.onComplete.add(function () {
                            //Hides the concierge sprite
                            this.concierge.kill();

                            //Allows the player to exit from the central upper gate
                            this.playerCanExit = true;
                        }, this);
                    }, this);
                }, this);
            }, this);
        }
        //Enters if the level is over and the bad ending has been achieved
        else {
            //Opens the centra upper gate
            this.yellowFunc(1, false);
            this.openingAnim = this.gates.getChildAt(1).animations.getAnimation('open');
            this.openingAnim.onStart.addOnce(function () {
                this.gateOpeningSound.play("", 0, window.game.globals.soundEffectsOn);
            }, this);
            this.openingAnim.play(2, false , false);
            this.openingAnim.onComplete.addOnce(function() {
                //Allows the player to exit from the central upper gate
                this.playerCanExit = true;
            }, this);
        }
    },


    /**
     * Checks if end level conditions are satisfied
     */
    endLevel: function() {
        //Enters here to check if and which end conditions have been met. If one of them is satisfied, this
        //function is called again to activate the proper effect
        if (!this.realEnd) {
            if(this.mainframesdown.mainframesLeft === 0) {
                this.realEnd = 1;
                this.endLevel(); //GoodEnd
            }
            else if (this.skyeyeActivation === 100) {
                this.realEnd = 2;
                this.endLevel(); //BadEnd
            }
        }
        //Enters here if one of the level end conditions has been satisfied
        else {
            //Check if there is some effect animation still going on. If so, do not end the level.
            //Another signalLevelOver will arrive after the end of the animation.
            for (var i = 0; i < 3; i++) {
                if (this.gates.getChildAt(i).animInProgress) {
                    return;
                }
                if (this.doorJambs.getChildAt(i).animInProgress) {
                    return;
                }
            }

            //Launches the function to activate the good ending
            if (this.realEnd === 1) {
                this.goodEnd();
            }
            //Launches the function to activate the bad ending
            else {
                this.badEnd();
            }
        }
    },

    /**
     * Sets the end type to "goodEnd" and calls the end dialogue function
     */
    goodEnd: function () {
        this.endType = "goodEnd";
        this.conciergeDialogue.endLevelDialogue(true);
    },

    /**
     * Sets the end type to "badEnd" and calls the end dialogue function
     */
    badEnd: function () {
        this.endType = "badEnd";
        this.bossDialogue.endLevelDialogue(false);
    },

    /**
     * Launched in case of bad ending when the player exits from the central upper gate.
     */
    playerExitBad: function () {
        var smallExplosionSound = window.game.add.audio("smallExplosion");
        var bigExplosionSound = window.game.add.audio("bigExplosion");
        var centralGate = this.gates.getChildAt(1);

        //The player sprites disappears when he exits
        this.player.kill();

        //The following code launches the explosion effects to destroy the central upper gate
        //Thee blocks are organized to be launched by timers to coordinate the timing of the effects
        this.waitTimer = window.game.time.create(false);
        this.waitTimer.add(1200, function () {
            mainframeEffects.effectFunc(centralGate.position.x, centralGate.position.y, 'explosion2', smallExplosionSound, this.lowerZGroup);
            mainframeEffects.effectFunc(centralGate.position.x + 35, centralGate.position.y + 35, 'explosion2', smallExplosionSound, this.lowerZGroup);
            mainframeEffects.effectFunc(centralGate.position.x + 25, centralGate.position.y + 10, 'explosion2', smallExplosionSound, this.lowerZGroup);
            this.waitTimer.add(1000, function () {
                mainframeEffects.effectFunc(centralGate.position.x + 10, centralGate.position.y + 40, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(centralGate.position.x + 15, centralGate.position.y + 55, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(centralGate.position.x + 45, centralGate.position.y + 10, 'explosion2', smallExplosionSound, this.lowerZGroup);
                this.waitTimer.add(1500, function () {
                    //Creates the hole sprite to simulate the gate destruction
                    var hole = this.lowerZGroup.create(404, 57, 'hole');
                    hole.anchor.setTo(0.5, 0.5);
                    mainframeEffects.effectFunc(centralGate.position.x + centralGate.width/2, centralGate.position.y + centralGate.height/2,
                        'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.smokeFunc(380, 40, this.lowerZGroup);
                    mainframeEffects.smokeFunc(425, 35, this.lowerZGroup);
                    mainframeEffects.smokeFunc(400, 65, this.lowerZGroup);

                    //Starts a boss dialogue
                    this.tempSignal = new Phaser.Signal();
                    this.dialogueVett = [{message: "I am sorry agent, you trusted the wrong man.", talkType:"correctDrop"},
                        {message: "There is no more obstacle now for my world control plan.", talkType:"correctDrop"},
                        {message: "You had seen too much to live, farewell!", talkType: "correctDrop"}];
                    this.bossDialogue.normalDialogue(this.dialogueVett, this.tempSignal);
                    this.tempSignal.add(function () {
                        //Computes the level score
                        this.stats.score = this.mainframesdown.mainframesLeft*250;

                        //Launches the level end screen
                        window.game.state.start('levelEnd', true, false, this.levelNumber, this.stats, this.logEntries, 1000, false);
                    }, this);
                }, this);
            }, this);
        }, this);

        //Starts the first timer to launch the explosion effects
        this.waitTimer.start();
    },

    /**
     * Launched in case of good ending when the player exits from the central upper gate.
     */
    playerExitGood: function () {
        var smallExplosionSound = window.game.add.audio("smallExplosion");
        var bigExplosionSound = window.game.add.audio("bigExplosion");
        var finalExplosionSound = window.game.add.audio("finalExplosion");
        this.blackEffect = window.game.add.sprite(0, 0, 'black');
        this.blackEffect.visible = false;
        this.whiteEffect = window.game.add.sprite(0, 0, 'white');
        this.whiteEffect.alpha = 0;

        //The player sprites disappears when he exits
        this.player.kill();

        //Starts a boss dialogue
        this.tempSignal.dispose();
        this.tempSignal = new Phaser.Signal();
        this.dialogueVett = [{message: "What are you doing???", talkType:"warning"},
            {message: "You don't understand!!!", talkType: "warning"},
            {message: "Do not destroy what I have created!!!", talkType: "warning"}];
        this.bossDialogue.normalDialogue(this.dialogueVett, this.tempSignal);
        this.tempSignal.add(function () {
            this.bomb.animations.stop();

            //The following code launches the explosion effects to destroy the entire server room
            //The blocks are organized to be launched by timers to coordinate the timing of the effects
            this.waitTimer = window.game.time.create(false);
            this.waitTimer.add(1000, function () {
                this.bomb.kill();
                mainframeEffects.effectFunc(this.bomb.position.x, this.bomb.position.y, 'explosion4', bigExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(50, 30, 'explosion3', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(100, 410, 'explosion3', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(155, 20, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(190, 470, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(245, 390, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(70, 100, 'explosion1', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(300, 20, 'explosion1', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(355, 313, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(403, 122, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(462, 311, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(510, 341, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(565, 553, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(604, 353, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(673, 233, 'explosion2', smallExplosionSound, this.lowerZGroup);
                mainframeEffects.effectFunc(722, 123, 'explosion2', smallExplosionSound, this.lowerZGroup);
                this.waitTimer.add(500, function () {

                    mainframeEffects.effectFunc(136, 30, 'explosion3', smallExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(421, 65, 'explosion3', smallExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(135, 86, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(535, 110, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(232, 133, 'explosion2', smallExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(523, 175, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(295, 203, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(325, 260, 'explosion2', smallExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(120, 310, 'explosion3', smallExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(535, 355, 'explosion3', smallExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(353, 392, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(131, 439, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(635, 470, 'explosion2', smallExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(693, 515, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(706, 550, 'explosion1', bigExplosionSound, this.lowerZGroup);
                    mainframeEffects.effectFunc(253, 426, 'explosion2', smallExplosionSound, this.lowerZGroup);
                    this.waitTimer.add(700, function () {

                        mainframeEffects.effectFunc(732, 532, 'explosion3', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(342, 300, 'explosion3', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(59, 100, 'explosion1', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(32, 370, 'explosion1', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(700, 374, 'explosion1', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(252, 290, 'explosion2', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(533, 300, 'explosion3', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(70, 363, 'explosion1', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(622, 40, 'explosion3', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(231, 300, 'explosion3', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(35, 100, 'explosion1', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(355, 245, 'explosion1', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(740, 374, 'explosion3', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(523, 544, 'explosion3', smallExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(134, 300, 'explosion4', bigExplosionSound, this.lowerZGroup);
                        mainframeEffects.effectFunc(632, 35, 'explosion4', bigExplosionSound, this.lowerZGroup);
                        this.waitTimer.add(900, function () {
                            mainframeEffects.effectFunc(732, 532, 'explosion4', bigExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(342, 300, 'explosion4', bigExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(59, 100, 'explosion1', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(32, 370, 'explosion1', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(700, 374, 'explosion1', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(252, 290, 'explosion3', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(533, 300, 'explosion4', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(730, 363, 'explosion4', bigExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(356, 532, 'explosion3', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(461, 30, 'explosion3', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(259, 510, 'explosion1', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(32, 370, 'explosion4', bigExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(700, 472, 'explosion3', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(624, 290, 'explosion1', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(533, 261, 'explosion3', smallExplosionSound, this.lowerZGroup);
                            mainframeEffects.effectFunc(488, 363, 'explosion4', bigExplosionSound, this.lowerZGroup);

                            var lastExplosion = this.lowerZGroup.create(window.game.world.centerX, window.game.world.centerY, 'explosion4');
                            lastExplosion.anchor.setTo(0.5, 0.5);
                            lastExplosion.scale.setTo(6, 6);
                            var anim = lastExplosion.animations.add('explosion4', null, 30, false);

                            anim.onStart.addOnce(function () {
                                finalExplosionSound.play("", 0, window.game.globals.soundEffectsOn);
                            }, this);

                            anim.play(null, false, true);

                            //Tween to make the screen go white
                            var whiteTween = window.game.add.tween(this.whiteEffect).to({alpha: 1}, 2000, Phaser.Easing.Linear.None, true, 0, 0, false);
                            whiteTween.onComplete.add(function () {
                                //Puts a black sprite under the white screen so that when the white screen
                                //goes away all that the player can see is black to simulate the disappearance of
                                //the server room
                                this.blackEffect.visible = true;
                                var endTween = window.game.add.tween(this.whiteEffect).to({alpha: 0}, 2000, Phaser.Easing.Linear.None, true, 0, 0, false);
                                endTween.onComplete.add(function () {
                                    this.stats.score = 1000 - this.skyeyeActivation*10;
                                    window.game.state.start('levelEnd', true, false, this.levelNumber, this.stats, this.logEntries, 1000, true);
                                }, this);
                            }, this);
                        }, this);
                    }, this);
                }, this);
            }, this);

            this.waitTimer.start();
        }, this);
    },

    /**
     * Prevents the player from further moving.
     */
    disableMovement: function () {
        window.game.input.keyboard.removeKey(Phaser.Keyboard.RIGHT);
        window.game.input.keyboard.removeKey(Phaser.Keyboard.LEFT);
        window.game.input.keyboard.removeKey(Phaser.Keyboard.UP);
        window.game.input.keyboard.removeKey(Phaser.Keyboard.DOWN);
    },

    /**
     * Disables every notice manager and switches the yellow light off for the upper gates.
     */
    disableNoticeManagers: function () {
        for (var i=0; i<6; i++) {
            this.noticeManager[i].deactivate();
            this.yellowFunc(i, false);
        }
    },

    /**
     * Deletes objects when the play state is changed.
     */
    shutdown: function() {
        if (this.bossDialogue) {
            this.bossDialogue.destroyObject();
            delete this.bossDialogue;
        }
        if (this.conciergeDialogue) {
            this.conciergeDialogue.destroyObject();
            delete this.conciergeDialogue;
        }
        delete this.levelData;
        delete this.levelNumber;
        delete this.noticesReplied;
        delete this.totalNotices;
        delete this.stats;
        delete this.bossDialogue;
        delete this.bossDialogueDone;
        delete this.playerCanExit;
        delete this.conciergeDialogue;
        delete this.pauseKey;
        delete this.logEntries;
        delete this.pauseBox;
        delete this.toMenuButton;
        delete this.realEnd;
        delete this.skyeyeActivation;
    }
};
