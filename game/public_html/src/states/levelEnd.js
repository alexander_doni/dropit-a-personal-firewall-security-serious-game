/* global Phaser, alertify  */
var Pda = require('../modules/classes/Pda')();
var dynamic_text = require('../modules/dynamic_text');
var Log = require('../modules/classes/Log')();
var loadSave = require('../modules/load_save_functions');
var CORSSender = require('../modules/learning_data_send');

/**
 * Screen containing the level score and statistics. It also indicates if the player has won or lost the level and
 * it allows to access the log screen.
 * @type {{init: module.exports.init, create: module.exports.create, activateInput: module.exports.activateInput, lArrow: module.exports.lArrow, home: module.exports.home, rArrow: module.exports.rArrow, logFunc: module.exports.logFunc, showHintBox: module.exports.showHintBox, hideHintBox: module.exports.hideHintBox, shutdown: module.exports.shutdown}}
 * @module
 * @name State: Level End
 */
module.exports = {
    /**
     * Receives and saves parameters passed by the 'play' state.
     * @param {int} levelNumber - Number of the failed level number
     * @param {Object} stats - Object containing the level statistics (number of notices correctly/wrongly allowed/denied)
     * @param {Array} logEntries - List of the log entries representing the player replies to all the notices
     * @param {int} maxScore - Maximum score that a player can make in the level. It is used to decide how many shields to earn
     * @param {boolean} playerWon - Flag indicating if the player has won the level or not
     */
    init: function(levelNumber, stats, logEntries, maxScore, playerWon) {
        this.levelNumber = levelNumber;

        if (!window.game.globals.levelsData[levelNumber+1]) { //This is the last level
            this.lastLevel = true;
        }

        this.stats = stats;
        this.stats.maxScore = maxScore;
        this.logEntries = logEntries;
        this.maxScore = maxScore;
        this.playerWon = playerWon;
        if(playerWon) {
            if (this.stats.score / maxScore < 0.5) { //One shield earned
                this.shieldsNumber = 1;
            }
            else if (this.stats.score / maxScore < 1) { //Two shields earned
                this.shieldsNumber = 2;
            }
            else { //Three shields earned
                this.shieldsNumber = 3;
            }
        }
    },

    /**
     * Draws the level end screen .
     */
    create: function(){
        //If the player wins or always in the last level, the data are saved
        if (this.playerWon || this.lastLevel) {
            loadSave.saveSaveData(this.levelNumber, this.stats, this.shieldsNumber, this.logEntries);
            if (!this.lastLevel) {
                alertify.alert("Progress saved!");
            }
        }

        //If this is the end of the last level, learning data are sent to the data gathering service
        if (this.lastLevel) {
            var learningData = {username: window.game.globals.playerName, levelsData: window.game.globals.savedata};
            localStorage.setItem("learningData", JSON.stringify(learningData));
            CORSSender.makeCorsRequest(JSON.stringify(learningData));
        }

        if (this.lastLevel) {
            this.pda = new Pda(this.lArrow, this.home, this.rArrow, "Retry level", "Credits", "Credits", this);
        }
        else {
            this.pda = new Pda(this.lArrow, this.home, this.rArrow, "Retry level", "Back to level selection", (this.playerWon) ? "Next level" : "Retry Level", this);
        }

        this.pda.button1.input.enabled = false;
        this.pda.button2.input.enabled = false;
        this.pda.button3.input.enabled = false;
        this.signal = new Phaser.Signal();
        this.signal.add(this.activateInput, this);

        this.typingSound = window.game.add.audio('typing');
        this.buttonClickSound = window.game.add.audio('buttonClick');

        //Prints the header text
        this.textGroup = window.game.add.group();
        this.style = { font: "20px Courier New, monospace", fill: "#00AA11", align: "center"};
        this.missionText = window.game.add.text(window.game.world.centerX, window.game.world.centerY - 200, "", this.style);
        this.missionText.anchor.setTo(0.5, 0.5);
        this.textGroup.add(this.missionText);
        this.headerText = window.game.add.text(window.game.world.centerX, window.game.world.centerY - 160, "", this.style);
        this.headerText.anchor.setTo(0.5, 0.5);
        this.textGroup.add(this.headerText);

        //Creates the hint box to explain Pda buttons functions
        this.hintBoxGroup = window.game.add.group();
        this.hintBox = this.hintBoxGroup.create(0, 0, 'hintBox');
        this.hintBox.width = this.hintBox.width - 40;
        this.hintBox.anchor.setTo(0.5, 0.5);
        this.hintBoxText = window.game.add.text(0, 0, "", {font: "13px Courier New, monospace", fill: "#00AA11", align: "center", wordWrap: true, wordWrapWidth: this.hintBox.width - 10});
        this.hintBoxText.anchor.setTo(0.5, 0);
        this.hintBoxGroup.add(this.hintBoxText);
        this.hintBoxGroup.visible = false;

        //Creates the log button
        this.logButton = window.game.add.button(40, 435, 'logButton', this.logFunc, this);
        this.logButton.onInputOver.add(this.showHintBox, this, 0, "Show log");
        this.logButton.onInputOut.add(this.hideHintBox, this, 0);
        this.textGroup.add(this.logButton);

        //Creates the level score and statistics text objects
        this.style2 = {font: "17px Courier New, monospace", fill: "#00AA11", align: "left"};
        this.scoreText = window.game.add.text(window.game.world.centerX, window.game.world.centerY - 110, "", this.style);
        this.scoreText.anchor.setTo(0.5, 0.5);
        this.textGroup.add(this.scoreText);
        this.rightBlockedText = window.game.add.text(100, window.game.world.centerY - 50, "", this.style2);
        this.textGroup.add(this.rightBlockedText);
        this.rightAllowedText = window.game.add.text(100, window.game.world.centerY - 10, "", this.style2);
        this.textGroup.add(this.rightAllowedText);
        this.wrongBlockedText = window.game.add.text(100, window.game.world.centerY + 30, "", this.style2);
        this.textGroup.add(this.wrongBlockedText);
        this.wrongAllowedText = window.game.add.text(100, window.game.world.centerY + 70, "", this.style2);
        this.textGroup.add(this.wrongAllowedText);

        this.typingSound.play("", 0, window.game.globals.soundEffectsOn, true);
        dynamic_text.write_one(this.missionText, 20, "Level " + (Number)(this.levelNumber+1), null);
        if (this.lastLevel) {
            dynamic_text.write_one(this.headerText, 20, (this.playerWon) ? "You saved the world!" : "You doomed the world!", null);
        }
        else {
            dynamic_text.write_one(this.headerText, 20, (this.playerWon) ? "Mission Accomplished!" : "You are fired!", null);
        }

        //Prints the level score and statistics
        dynamic_text.write_one(this.scoreText, 20, "Score: " + this.stats.score + "/" + this.maxScore, null);
        dynamic_text.write_one(this.rightBlockedText, 20, "Correctly blocked connections: " + this.stats.rb, null);
        dynamic_text.write_one(this.wrongBlockedText, 20, "Wrongly blocked connections: " + this.stats.wb, null);
        dynamic_text.write_one(this.rightAllowedText, 20, "Correctly allowed connections: " + this.stats.ra, null);
        dynamic_text.write_one(this.wrongAllowedText, 20, "Wrongly allowed connections: " + this.stats.wa, this.signal);
    },

    /**
     * Activates Pda buttons after the statistics have been completely written.
     */
    activateInput: function() {
        this.typingSound.stop();
        this.pda.button1.input.enabled = true;
        this.pda.button2.input.enabled = true;
        this.pda.button3.input.enabled = true;
    },

    /**
     * Retry button. If pressed, it restarts the current level.
     */
    lArrow: function() {
        //Repeat level
        window.game.state.start('howto', true, false, this.levelNumber);
    },

    /**
     * Home button. If pressed, it switches to the level selection state.
     */
    home: function() {
        if (this.lastLevel) {
            //Go to Credits
            window.game.state.start('howto', true, false, -2);
        }
        else {
            //Come back to level selection
            window.game.state.start('selection');
        }
    },

    /**
     * Next level button. If pressed, it starts the next level.
     */
    rArrow: function() {
        if (this.lastLevel) {
            //Go to Credits
            window.game.state.start('howto', true, false, -2);
        }
        else {
            if (this.playerWon) {
                //Next Level
                window.game.state.start('howto', true, false, this.levelNumber + 1);
            }
            else {
                //Repeat level
                window.game.state.start('howto', true, false, this.levelNumber);
            }
        }
    },

    /**
     * Opens the level log page.
     */
    logFunc: function() {
        this.buttonClickSound.play("", 0, window.game.globals.soundEffectsOn);
        //Disabling log button
        this.textGroup.visible = false;
        this.hideHintBox();
        if (!this.log) {
            this.log = new Log(this.logEntries, this.pda, this);
        }
        else {
            this.log.show();
        }
    },

    /**
     * Shows a hint box to better explain the Pda buttons function. It is showed when the mouse hovers a button.
     * @param {Phaser.Sprite} button - The sprite object of the clicked button
     * @param {Phaser.Pointer} item - A pointer object passed by the Phaser.Signal dispatch function
     * @param {string} tag - String to show on the hint box
     */
    showHintBox: function(button, item, tag) {
        this.hintBox.position.x = button.position.x + 30;
        this.hintBox.position.y = button.position.y - 50;
        this.hintBoxText.position.x = this.hintBox.position.x;
        this.hintBoxText.position.y = this.hintBox.position.y - this.hintBox.height/2 + 10;
        this.hintBoxText.setText(tag);
        window.game.world.bringToTop(this.hintBoxGroup);
        this.hintBoxGroup.visible = true;
    },

    /**
     * Hides the hint box when the mouse cursor stops hovering a Pda button.
     */
    hideHintBox: function() {
        this.hintBoxGroup.visible = false;
    },


    /**
     * Deletes used variables to free space up.
     */
    shutdown: function() {
        delete this.signal;
        delete this.style;
        delete this.style2;
        delete this.log;
        delete this.levelNumber;
        delete this.stats;
        delete this.logEntries;
        delete this.shieldsNumber;
        delete this.lastLevel;
    }
};
