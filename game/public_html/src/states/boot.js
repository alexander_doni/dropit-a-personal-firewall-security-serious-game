/* global Phaser */

/**
 * It is the first state launched and it takes care of loading necessary assets and systems before to switch
 * to the proper load state.
 * @type {{init: module.exports.init, preload: module.exports.preload, create: module.exports.create, scaleStage: module.exports.scaleStage}}
 * @module
 * @name State: Boot
 */
module.exports = {
    /**
     * Initialization function with scaling options.
     */
    init: function () {
        /*window.game.scale.pageAlignHorizontally = true;
        window.game.scale.pageAlignVertically = true;*/
        this.scaleStage();
     },

    /**
     * Loads the loading screen assets.
     */
    preload: function () {
        window.game.load.image('loading', 'src/images/loading.png');
        window.game.load.image('load_progress_bar', 'src/images/progress_bar_bg.png');
        window.game.load.image('load_progress_bar_dark', 'src/images/progress_bar_fg.png');
    },

    /**
     * Starts the physics and switches to the 'load' state.
     */
    create: function () {
        window.game.physics.startSystem(Phaser.Physics.ARCADE);
        window.game.state.start('load');
    },

    /**
     * Adapts the game scale to the currently used screen size
     */
    scaleStage: function() {
        if (window.game.device.desktop)
        {
            window.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            window.game.scale.minWidth = 800/2;
            window.game.scale.minHeight = 600/2;
            window.game.scale.maxWidth = 800;
            window.game.scale.maxHeight = 600;
            window.game.scale.pageAlignHorizontally = true;
            window.game.scale.pageAlignVertically = true;
        }
        else
        {
            window.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            window.game.scale.minWidth = 800/2;
            window.game.scale.minHeight = 600/2;
            window.game.scale.maxWidth = 2048; //You can change this to gameWidth*2.5 if needed
            window.game.scale.maxHeight = 1228; //Make sure these values are proportional to the gameWidth and gameHeight
            window.game.scale.pageAlignHorizontally = true;
            window.game.scale.pageAlignVertically = true;
            window.game.scale.forceOrientation(true, false);
            window.game.scale.hasResized.add(window.game.gameResized, this);
            window.game.scale.enterIncorrectOrientation.add(window.game.scale.enterIncorrectOrientation, this);
            window.game.scale.leaveIncorrectOrientation.add(window.game.scale.leaveIncorrectOrientation, this);
            window.game.scale.setScreenSize(true);
        }

        var ow = parseInt(window.game.canvas.style.width,10);
        var oh = parseInt(window.game.canvas.style.height,10);
        var r = Math.max(window.innerWidth/ow,window.innerHeight/oh);
        var nw = ow*r;
        var nh = oh*r;
        window.game.canvas.style.width = nw+"px";
        window.game.canvas.style.height= nh+"px";
        window.game.canvas.style.marginLeft = (window.innerWidth/2 - nw/2)+"px";
        window.game.canvas.style.marginTop = (window.innerHeight/2 - nh/2)+"px";
        document.getElementById("game").style.width = window.innerWidth+"px";
        document.getElementById("game").style.height = window.innerHeight-1+"px";//The css for body includes 1px top margin, I believe this is the cause for this -1
        document.getElementById("game").style.overflow = "hidden";

    }
};
