var Pda = require('../modules/classes/Pda')();
var LevelButton = require('../modules/classes/LevelButton')();
var loadSave = require('../modules/load_save_functions');

/**
 * Level selection screen used to select one among the unlocked levels. This screen also shows how many shields have
 * been earned for each level.
 * @type {{create: Function, levelClicked: Function, lArrow: Function, home: Function, rArrow: Function, changePage: Function}}
 * @module
 * @name State: Level Selection
 */
module.exports = {
    /**
     * Draws the level selection screen.
     */
    create: function() {
        this.pda = new Pda(this.lArrow, this.home, this.rArrow, "Previous page", "Back to menu", "Next page", this);
        this.levelButtons = [];
        this.currentPage = 0;
        this.elemperpage = 15;

        this.numberOfPages = Math.ceil(window.game.globals.levelsData.length / this.elemperpage);
        this.style = { font: "15px Courier New, monospace", fill: "#00AA11", align: "right"};
        this.npageText = window.game.add.text(680, 470, (Number)(this.currentPage+1) + " / " + this.numberOfPages, this.style);
        this.npageText.anchor.set(0, 0);
        this.changePage.call(this, this.currentPage);
    },

    /**
     * Called in the context of the LevelButton object clicked to switch to the corresponding level.
     */
    levelClicked: function(button, item, isPlayable) {
        if (isPlayable) {
            this.clickSound.play("", 0, window.game.globals.soundEffectsOn);
            window.game.state.start('howto', true, false, this.levelNumber);
        }
        else {
            this.wrongClickSound.play("", 0, window.game.globals.soundEffectsOn);
        }
    },

    /**
     * Called when the left arrow button is pressed to go to the previous level selection page.
     */
    lArrow: function() {
        if (this.currentPage > 0) {
            this.currentPage--;
            this.npageText.setText( (Number)(this.currentPage+1) + " / " + this.numberOfPages);
            this.changePage.call(this, this.currentPage);
        }
    },

    /**
     * Called when the home button is pressed to come back to the main menu.
     */
    home: function() {
        window.game.state.start('menu');
    },

    /**
     * Called when the right arrow button is pressed to go to the next level selection page.
     */
    rArrow: function() {
        if (this.currentPage < this.numberOfPages - 1) {
            this.currentPage++;
            this.npageText.setText((Number)(this.currentPage+1) + " / " + this.numberOfPages);
            this.changePage.call(this, this.currentPage);
        }
    },

    /**
     * Called when page has been changed to draw the new page.
     */
    changePage: function() {
        var i;

        for (i=0; i<this.levelButtons.length; i++) {
            this.levelButtons[i].destroyButton();
        }
        delete this.levelButtons;

        this.levelButtons = [];
        var remaining_elem = window.game.globals.levelsData.length - this.currentPage*this.elemperpage;
        for (i=0; i<remaining_elem && i < this.elemperpage; i++) {
            this.levelButtons[i] = new LevelButton(i + this.currentPage*this.elemperpage, this.levelClicked);
        }
    },

    /**
     * Deletes used variables to free space up.
     */
    shutdown: function() {
        delete this.levelButtons;
        delete this.currentPage;
        delete this.elemperpage;
        delete this.numberOfPages;
        delete this.style;
    }
};
