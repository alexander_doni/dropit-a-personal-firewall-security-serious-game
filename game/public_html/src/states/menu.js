var buttons = require('../modules/menu_buttons.js');
var loadSave = require('../modules/load_save_functions');
var alertBox = require('../modules/alert_box');
var lds = require('../modules/learning_data_send');

/**
 * Main menu showed after loading has been completed.
 * @type {{create: module.exports.create, update: module.exports.update, showHintBox: module.exports.showHintBox, hideHintBox: module.exports.hideHintBox, delSaveData: module.exports.delSaveData, toggleSoundEffects: module.exports.toggleSoundEffects, toggleMusic: module.exports.toggleMusic, changeDifficulty: module.exports.changeDifficulty, resendData: module.exports.resendData}}
 * @module
 * @name State: Main Menu
 */
module.exports = {
    /**
     * Initializes and draws the main menu.
     */
    create: function(){
        //Draws DropIt title
        this.gameTitle = window.game.add.image(window.game.world.centerX,
            window.game.world.centerY - 190, 'menu_title');
        this.gameTitle.anchor.setTo(0.5, 0.5);

        //Draw menu buttons
        buttons.draw();

        //Creates hint box group
        this.hintBoxGroup = window.game.add.group();
        this.hintBox = this.hintBoxGroup.create(0, 0, 'hintBox');
        this.hintBox.anchor.setTo(0.5, 0.5);
        this.hintBoxText = window.game.add.text(0, 0, "", {font: "13px Courier New, monospace", fill: "#00AA11", align: "center", wordWrap: true, wordWrapWidth: this.hintBox.width - 15});
        this.hintBoxText.anchor.setTo(0.5, 0);
        this.hintBoxGroup.add(this.hintBoxText);
        this.hintBoxGroup.visible = false;

        //Delete save data button
        this.saveButton = window.game.add.button(60, 540, 'saveButton', this.delSaveData, this);
        this.saveButton.onInputOver.add(this.showHintBox, this, 0, "Delete game data");
        this.saveButton.onInputOut.add(this.hideHintBox, this, 0);

        //Remove sound effects button
        this.soundEffectsButton = window.game.add.button(110, 540, 'speakerButton', this.toggleSoundEffects, this);
        this.soundEffectsButton.onInputOut.add(this.hideHintBox, this, 0);
        this.littleRedCrossForSound = window.game.add.sprite(110, 540, 'littleRedCross');
        if (window.game.globals.soundEffectsOn === 0.7) {
            this.littleRedCrossForSound.visible = false;
            this.soundEffectsButton.onInputOver.add(this.showHintBox, this, 0, "Remove sound effects");
        }
        else {
            this.littleRedCrossForSound.visible = true;
            this.soundEffectsButton.onInputOver.add(this.showHintBox, this, 0, "Enable sound effects");
        }


        //Remove music button
        this.musicButton = window.game.add.button(160, 540, 'musicButton', this.toggleMusic, this);
        this.musicButton.onInputOut.add(this.hideHintBox, this, 0);
        this.littleRedCrossForMusic = window.game.add.sprite(165, 540, 'littleRedCross');
        if (window.game.globals.music.isOn) {
            this.littleRedCrossForMusic.visible = false;
            this.musicButton.onInputOver.add(this.showHintBox, this, 0, "Remove music");
        }
        else {
            this.littleRedCrossForMusic.visible = true;
            this.musicButton.onInputOver.add(this.showHintBox, this, 0, "Enable music");
        }

        //Change difficulty button
        this.difficultyButton = window.game.add.button(window.game.width - 120, 540, 'difficultyButton', this.changeDifficulty, this);
        this.difficultyButton.onInputOut.add(this.hideHintBox, this, 0);
        this.smallFire = window.game.add.sprite(window.game.width - 120, 540, 'smallFire');
        this.bigFire = window.game.add.sprite(window.game.width - 120, 540, 'bigFire');
        switch (window.game.globals.difficulty) {
            case 0: //Easy
                this.difficultyButton.onInputOver.add(this.showHintBox, this, 0, "Easy");
                this.hintBoxText.setText("Easy");
                this.smallFire.visible = false;
                this.bigFire.visible = false;
                break;
            case 1: //Medium
                this.difficultyButton.onInputOver.add(this.showHintBox, this, 0, "Medium");
                this.hintBoxText.setText("Medium");
                this.smallFire.visible = true;
                this.bigFire.visible = false;
                break;
            case 2: //Hard
                this.difficultyButton.onInputOver.add(this.showHintBox, this, 0, "Hard");
                this.hintBoxText.setText("Hard");
                this.bigFire.visible = true;
                this.smallFire.visible = false;
                break;
        }

        //Checks if learningData have or should have been sent so to add the sendLearningData button
        //to allow the resending of the data in case an error has occurred
        if (localStorage.getItem("learningData")) {
            //Resend learning data button
            this.resendDataButton = window.game.add.button(window.game.width - 180, 540, 'sendButton', this.resendData, this);
            this.resendDataButton.onInputOut.add(this.hideHintBox, this, 0);
            this.resendDataButton.onInputOver.add(this.showHintBox, this, 0, "Send learning data");
        }

        this.settingsButtonSound = window.game.add.audio('buttonClick');
        this.settingsButtonWrongSound = window.game.add.audio('wrong');
    },

    /**
     * Shows a hint box to better explain the Pda buttons function. It is showed when the mouse hovers a button.
     * @param {Phaser.Sprite} button - The sprite object of the clicked button
     * @param {Phaser.Pointer} item - A pointer object passed by the Phaser.Signal dispatch function
     * @param {string} tag - String to show on the hint box
     */
    showHintBox: function(button, item, tag) {
        this.hintBox.position.x = button.position.x + 30;
        this.hintBox.position.y = button.position.y - 50;
        this.hintBoxText.position.x = this.hintBox.position.x;
        this.hintBoxText.position.y = this.hintBox.position.y - this.hintBox.height/2 + 10;
        this.hintBoxText.setText(tag);
        window.game.world.bringToTop(this.hintBoxGroup);
        this.hintBoxGroup.visible = true;
    },

    /**
     * Hides the hint box when the mouse cursor stops hovering a Pda button.
     */
    hideHintBox: function() {
        this.hintBoxGroup.visible = false;
    },

    /**
     * Called when the 'deleteSaveData' button is pressed to remove all saved data.
     */
    delSaveData: function () {
        if (loadSave.removeSaveData()) {
            this.settingsButtonSound.play("", 0, window.game.globals.soundEffectsOn);
            alertBox.popup("Saved data successfully deleted!");
        }
        else {
            this.settingsButtonWrongSound.play("", 0, window.game.globals.soundEffectsOn);
        }
    },

    /**
     * Toggles sound effects from the game, but not the music
     */
    toggleSoundEffects: function () {
        this.soundEffectsButton.onInputOver.removeAll();
        if (window.game.globals.soundEffectsOn === 0.7) {
            this.soundEffectsButton.onInputOver.add(this.showHintBox, this, 0, "Enable sound effects");
            this.hintBoxText.setText("Enable sound effects");
            window.game.globals.soundEffectsOn = 0;
        }
        else {
            this.soundEffectsButton.onInputOver.add(this.showHintBox, this, 0, "Remove sound effects");
            this.hintBoxText.setText("Remove sound effects");
            window.game.globals.soundEffectsOn = 0.7;
        }
        this.littleRedCrossForSound.visible = !this.littleRedCrossForSound.visible;
        this.settingsButtonSound.play("", 0, window.game.globals.soundEffectsOn);
    },

    /**
     * Toggles the music from the game, but not the sound effects
     */
    toggleMusic: function () {
        this.settingsButtonSound.play("", 0, window.game.globals.soundEffectsOn);
        this.musicButton.onInputOver.removeAll();
        if (window.game.globals.music.isOn) {
            this.musicButton.onInputOver.add(this.showHintBox, this, 0, "Enable music");
            this.hintBoxText.setText("Enable music");
        }
        else {
            this.musicButton.onInputOver.add(this.showHintBox, this, 0, "Remove music");
            this.hintBoxText.setText("Remove music");
        }
        window.game.globals.music.toggleMusic();
        this.littleRedCrossForMusic.visible = !this.littleRedCrossForMusic.visible;
        window.game.globals.music.isOn = !window.game.globals.music.isOn;
    },

    /**
     * Changes the game difficulty. It impacts the time available to reply to notices and the
     * points removed for auto dropped notices.
     * Easy: High amount of time to reply to a single notice. 0 points lost for automatic drop.
     * Medium: Medium amount of time to reply to a single notice. 50 points lost for automatic drop.
     * Hard: Low amount of time to reply to a single notice. 100 points lost for automatic drop.
     */
    changeDifficulty: function () {
        this.difficultyButton.onInputOver.removeAll();
        this.settingsButtonSound.play("", 0, window.game.globals.soundEffectsOn);
        window.game.globals.difficulty = (window.game.globals.difficulty+1) % 3;

        switch (window.game.globals.difficulty) {
            case 0: //Easy
                this.difficultyButton.onInputOver.add(this.showHintBox, this, 0, "Easy");
                this.hintBoxText.setText("Easy");
                this.smallFire.visible = false;
                this.bigFire.visible = false;
                break;
            case 1: //Medium
                this.difficultyButton.onInputOver.add(this.showHintBox, this, 0, "Medium");
                this.hintBoxText.setText("Medium");
                this.smallFire.visible = true;
                this.bigFire.visible = false;
                break;
            case 2: //Hard
                this.difficultyButton.onInputOver.add(this.showHintBox, this, 0, "Hard");
                this.hintBoxText.setText("Hard");
                this.bigFire.visible = true;
                this.smallFire.visible = false;
                break;
        }
    },

    /**
     * Allows the player to resend learning data in case the initial uploading has failed
     */
    resendData: function () {
        lds.makeCorsRequest(localStorage.getItem("learningData"));
    }
};
