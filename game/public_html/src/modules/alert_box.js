/*global Phaser*/

var dynamic_text = require('./dynamic_text');

/**
 * Alert box used to give brief system messages to the player, such as the successful deletion of saved data.
 * @type {{popup: module.exports.popup, writeMessage: module.exports.writeMessage, updateMessage: module.exports.updateMessage, activateInput: module.exports.activateInput, boxClicked: module.exports.boxClicked, destroyObject: module.exports.destroyObject}}
 * @module
 * @name Alert Box
 */
module.exports = {
    /**
     * Creates and displays an alert box.
     * @param {string} message - The message to write on the alert box
     */
    popup: function(message) {

        this.controlGroup = window.game.add.group();
        //Creates the box sprites
        this.box = this.controlGroup.create(window.game.world.centerX, window.game.world.centerY, 'alertBox');
        this.box.anchor.set(0.5, 0.5);

        //Signal launched when message has been written to allow the user to close the alert box
        //by clicking on it
        this.signal = new Phaser.Signal();
        this.signal.add(this.activateInput, this);

        this.typingSound = window.game.add.audio('typing');

        //Creating text object on which to print the message
        this.style = { font: "20px Courier New, monospace", fill: "#00AA11", align: "center", wordWrap: true, wordWrapWidth: this.box.x - 45};
        this.messageText = window.game.add.text(window.game.world.centerX, window.game.world.centerY, "", this.style);
        this.messageText.anchor.setTo(0.5, 0.5);
        this.controlGroup.add(this.messageText);
        this.controlGroup.setAll('inputEnabled', true);

        //Ensuring image shrinks on the center
        this.controlGroup.pivot.x = window.game.world.centerX;
        this.controlGroup.pivot.y = window.game.world.centerY;
        this.controlGroup.x += this.controlGroup.pivot.x;
        this.controlGroup.y += this.controlGroup.pivot.y;

        //Entering tween of the alert box
        this.startTween = window.game.add.tween(this.controlGroup.scale).from({x: 0, y:0},500,Phaser.Easing.Linear.None,true, 0, 0);
        window.game.add.tween(this.controlGroup).from({alpha: 0},500,Phaser.Easing.Linear.None,true, 0, 0);
        if (message) {
            this.startTween.onComplete.add(this.writeMessage, this, 0, message);
        }
    },

    /**
     * Writes the message on the alert box when no previous was already written on it.
     * @param {Object} item - Not used
     * @param {Object} context - Not used
     * @param {string} message - Message to write on the alert box
     */
    writeMessage: function(item, context, message) {
        this.typingSound.play("", 0, window.game.globals.soundEffectsOn, true);
        dynamic_text.write_one(this.messageText, 40, message, this.signal);
    },

    /**
     * Deletes the message already written on the alert box and writes a new one.
     * @param {string} newMessage - Message to write on thew alert box to substitute the previous message
     */
    updateMessage: function(newMessage) {
        this.messageText.setText("");
        this.controlGroup.callAll('events.onInputDown.remove', 'events.onInputDown', this.boxClicked, this);
        this.typingSound.play("", 0, window.game.globals.soundEffectsOn, true);
        dynamic_text.write_one(this.messageText, 40, newMessage, this.signal);
    },

    /**
     * Called when the message has been written to activate input. This way the player will be able to close the alert
     * box by clicking on it.
     */
    activateInput: function() {
        this.typingSound.stop();
        //Control group could be not defined here since alert box can be closed from the outside due to screen change
        if (this.controlGroup) {
            this.controlGroup.callAll('events.onInputDown.add', 'events.onInputDown', this.boxClicked, this);
        }
    },

    /**
     * Called when the player clicks on the alert box to close it.
     */
    boxClicked: function() {
        window.game.add.tween(this.controlGroup).to({alpha: 0},500,Phaser.Easing.Linear.None, true);
        this.finalTween = window.game.add.tween(this.controlGroup.scale).to({x: 0, y:0},500,Phaser.Easing.Linear.None, true);
        this.finalTween.onComplete.add(this.destroyObject, this);
    },

    /**
     * Called after the closing animation ends to destroy the alert box object.
     */
    destroyObject: function() {
        this.controlGroup.destroy();
        delete this.controlGroup;
    }
};