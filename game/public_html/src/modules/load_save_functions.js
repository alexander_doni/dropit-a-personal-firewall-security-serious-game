/* global alertify */

/**
 * Module that provides data saving functionality. It uses the Web Storage APIs to save data locally on the client.
 * Save data includes completed level along with full statistics and logs.
 * @type {{loadSaveData: module.exports.loadSaveData, saveSaveData: module.exports.saveSaveData, removeSaveData: module.exports.removeSaveData, checkCompatibility: module.exports.checkCompatibility, totalShieldsEarned: module.exports.totalShieldsEarned, promptForName: module.exports.promptForName}}
 * @module
 * @name Load and Save Data
 */
module.exports = {
    /**
     * Loads saved data, if present, and saves them in a global variable.
     */
    loadSaveData: function() {
        if (this.checkCompatibility()) {
            var savedata = JSON.parse(localStorage.getItem('savedata'));
            if (savedata) {
                window.game.globals.savedata = savedata;
            }
        }
    },

    /**
     * Saves data in the local storage if no data for the current level exist or if the current score is higher than
     * the saved one.
     * @param {int} levelNumber - The level that is currently being saved
     * @param {Object} stats - Object containing notices replies statistics and level score
     * @param {int} shieldsNumber - Shields earned in the level
     * @param {Array} logEntries - List of the log entries representing the player replies to all the notices
     */
    saveSaveData: function(levelNumber, stats, shieldsNumber, logEntries) {

        //Create savedata if not existent or overwrite it if saved score is lower than the new one
        if (!window.game.globals.savedata[levelNumber] || window.game.globals.savedata[levelNumber].stats.score < stats.score) {
            window.game.globals.savedata[levelNumber] = {};
			window.game.globals.savedata[levelNumber].levelNumber = levelNumber+1;
            window.game.globals.savedata[levelNumber].shields = shieldsNumber;
            window.game.globals.savedata[levelNumber].stats = stats;
            window.game.globals.savedata[levelNumber].logEntries = $.extend(true, [], logEntries);

            //Removing data unnecessary to be saved to save on Web Storage quota and data to be sent
            //to the data gathering service
            for (var i=0; i<logEntries.length; i++) {
                delete window.game.globals.savedata[levelNumber].logEntries[i].icon;
                delete window.game.globals.savedata[levelNumber].logEntries[i].explanation;
            }

            //Saves the data to the Web Storage if it is supported by the current browser
            if (this.checkCompatibility()) {
                localStorage.setItem('savedata', JSON.stringify(window.game.globals.savedata));
            }
        }
    },

    /**
     * Removes saved data to allow a clean restart.
     * @returns {boolean} True if data have been removed, false otherwise.
     */
    removeSaveData: function() {
        if(window.game.globals.savedata.length !== 0) {
            window.game.globals.savedata = [];
            delete window.game.globals.playerName;
            if (this.checkCompatibility()) {
                localStorage.removeItem('savedata');
                localStorage.removeItem('playerName');
                localStorage.removeItem('learningData');
            }
            return true;
        }
        else {
            return false;
        }
    },

    /**
     * Checks if Web Storage APIs are supported by the player's browser.
     * @returns {boolean} True if Web Storage is supported, false otherwise.
     */
    checkCompatibility: function() {
        var mod = 'checkStorage';
        try {
            localStorage.setItem(mod, mod);
            localStorage.removeItem(mod);
            return true;
        } catch(e) {
            console.log("Web Storage APIs not supported!!!");
            return false;
        }
    },

    /**
     * Reads the total shields number earned thus far by the player.
     * @returns {number} The total number of shields earned.
     */
    totalShieldsEarned: function() {
        var shields = 0;
        window.game.globals.savedata.forEach(function(obj) {
            shields += obj.shields;
        });

        return shields;
    },

    /**
     * Asks the player for his/her username if it has not yet been done. If the name is acquired then the game can
     * proceed to load. Otherwise the game enters an error screen, for example the browser does not support the Web
     * Storage APIs.
     * @param {Phaser.Signal} nameSignal - Signals whether has been possible to acquire the name or not
     */
    promptForName: function (nameSignal) {
        if (!this.checkCompatibility()) {
            nameSignal.dispatch(false);
            return;
        }
        else if (localStorage.getItem("playerName")) { //Name has already been inserted, do not ask again
            window.game.globals.playerName = localStorage.getItem("playerName");
            nameSignal.dispatch(true);
            return;
        }
        // prompt dialog
        var context = this;
        alertify.prompt("Insert your name as NAME.SURNAME.4RANDOMNUMBERS", function (e, str) {
            // str is the input text
            if (e) {
                // user clicked "ok"
                if (/^([a-zA-Z]{2,40}\.[a-zA-Z]{2,40}\.[0-9]{4})$/.test(str)) { //Correct name
                    window.game.globals.playerName = str;
                    localStorage.setItem("playerName", str);
                    alertify.alert("Name saved!");
                    nameSignal.dispatch(true);
                }
                else { //Wrong name
                    alertify.alert("Name not valid! Please input your name as NAME.SURNAME.4RANDOMNUMBERS");
                    context.promptForName(nameSignal);
                }
            } else { //Name not inserted
                alertify.alert("You must insert a name!");
                context.promptForName(nameSignal);
            }
        }, "NAME.SURNAME.4RANDOMNUMBERS");
    }
};
