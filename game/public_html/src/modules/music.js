/**
 * Module that handles the music track list, wrapping engine methods to offer basic music player functionalities.
 * @type {{tracksNumber: number, normalVolume: number, lowVolume: number, isOn: boolean, tracks: Array, init: module.exports.init, nextTrack: module.exports.nextTrack, toggleMusic: module.exports.toggleMusic, setVolume: module.exports.setVolume, play: module.exports.play, pause: module.exports.pause, resume: module.exports.resume, stop: module.exports.stop, getRandomInt: module.exports.getRandomInt}}
 * @module
 * @name Music
 */
module.exports = {
    /**
     * Number of music tracks available.
     * @member {number} tracksNumber
     */
    tracksNumber: 5,
    /**
     * Default volume for the game music.
     * @member {number} normalVolume
     */
    normalVolume: 0.7,
    /**
     * Low volume for the game music to use on tutorial, level intro, and level outro screens.
     * @member {number} normalVolume
     */
    lowVolume: 0.2,
    /**
     * Flag indicating is the music is playing or not.
     * @member {boolean} isOn
     */
    isOn: true,
    /**
     * Music tracks objects array
     * @member {Array} tracks
     */
    tracks: [],

    /**
     * Called by the load state. It initializes the tracks list and decodes the songs, if needed. Then launches the function to continue the
     * game loading.
     * @param {function} launchFunc - Function to execute when the tracks have been decoded or immediately if they
     * do not need to be decoded
     */
    init: function (launchFunc) {
        this.currentVolume = this.normalVolume;
        this.currentTrackNumber = this.getRandomInt(0, this.tracksNumber - 1);
        for (var i=0; i<this.tracksNumber; i++) {
            this.tracks[i] = window.game.add.audio("track" + i, this.currentVolume);
            this.tracks[i].onStop.add(this.nextTrack, this);
        }

        window.game.sound.setDecodedCallback(this.tracks, launchFunc);
    },

    /**
     * Stops the current track and plays the next one in the array.
     */
    nextTrack: function () {
        if (this.tracks[this.currentTrackNumber].isPlaying) {
            this.stop();
        }
        else {
            this.currentTrackNumber = (this.currentTrackNumber+1) % this.tracksNumber;
            this.play();
        }
    },

    /**
     * Pauses the music.
     */
    toggleMusic: function () {
        if (this.isOn) {
            this.pause();
        }
        else {
            this.resume();
        }
    },

    /**
     * Changes music volume.
     * @param {number} volume - New volume value. The number can be from 0 to 1.
     */
    setVolume: function(volume) {
        this.currentVolume = volume;
        this.tracks[this.currentTrackNumber].volume = volume;
    },

    /**
     * Plays the current track.
     */
    play: function () {
        this.tracks[this.currentTrackNumber].play("", 0, this.currentVolume);
    },

    /**
     * Pauses the current track.
     */
    pause: function () {
        this.tracks[this.currentTrackNumber].pause();
    },

    /**
     * Resumes the current track.
     */
    resume: function() {
        this.tracks[this.currentTrackNumber].resume();
    },

    /**
     *
     */
    stop: function () {
        this.tracks[this.currentTrackNumber].stop();
    },

    getRandomInt: function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
};