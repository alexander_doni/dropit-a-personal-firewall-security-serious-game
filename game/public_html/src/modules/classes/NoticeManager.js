/* global Phaser */

var Notice = require('./Notice')();
var Queue = require('./Queue')();
var ConnectionAttempt = require('./ConnectionAttempt')();

module.exports = function() {

    /**
     * @classdesc A Notice Manager manages the entire life cycle of the Notice objects and it launches the appropriate
     * effects depending on the player's reply.
     * @param {Phaser.Signal} responseSignal - Signal dispatched to inform which gate to open and animation to launch
     * @param {int} gateNumber - Id of the gate controlled by this Notice Manager
     * @param {Function} incFunc - Function called to update level statistics
     * @param {Object} extContext - Context of the play state to call incFunc
     * @param {int} levelNumber - Number of the level currently being played
     * @param {Phaser.Signal} signalYellow - Signal dispatched when the yellow light above a gate has to be
     * switched on or off
     * @param {Phaser.Signal} signalOver - Signal dispatched when the consequence animation is over, or immediately
     * if the connection has been dropped
     * @param {Phaser.Signal} signalLevelOver - Signal dispatched if a notice has been dropped to check the level
     * ending conditions
     * @constructor
     */
    function NoticeManager(responseSignal, gateNumber, incFunc, extContext, levelNumber, signalYellow, signalOver, signalLevelOver) {
        this.noticeOn = false;
        this.noticesQueue = new Queue();
        this.activeNoticesQueue = new Queue();
        this.noticeX = 500;
        this.signalYellow = signalYellow;
        this.signalOver = signalOver;
        this.signalLevelOver = signalLevelOver;
        this.responseSignal = responseSignal;
        this.gateNumber = gateNumber;
        this.incFunc = incFunc;
        this.extContext = extContext;
        this.levelNumber = levelNumber;
        this.flag = true;
        this.brokenFunction = window.game.globals.levelsData[levelNumber].brokenFunction;

        //Changes the time available to reply to a notice depending on the chosen difficulty
        switch (window.game.globals.difficulty) {
            case 0: //Easy
                this.thirdTimeForANotice = 30000;
                break;
            case 1: //Medium
                this.thirdTimeForANotice = 20000;
                break;
            case 2: //Hard
                this.thirdTimeForANotice = 15000;
                break;
        }
    }

    /**
     * Function used to create a notice and add it to the notices queue.
     * @param {ConnectionAttempt} connectionAttempt - The connection attempt to generate the notice about
     * @param {Object} logEntries - Object that contains the level log entries
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     */
    NoticeManager.prototype.createNotice = function(connectionAttempt, logEntries, lowerZGroup) {
        this.noticesQueue.enqueue(new Notice(connectionAttempt, this.allowDrop, this.treatAs, this.allowDrop, this, this.brokenFunction, lowerZGroup));
        this.logEntries = logEntries;
    };

    /**
     * Starts the timer to decide when the next notice will arrive. The time value is the number of ms written
     * in the tts variable of the ConnectionAttempt object contained inside the notice.
     */
    NoticeManager.prototype.startTimer = function () {
        if (!this.noticesQueue.isEmpty()) {
            this.nextNoticeTimer = window.game.time.create(window.game, true);
            this.nextNoticeTimer.add(this.noticesQueue.peek().connectionAttempt.tts, this.initNotice, this);
            this.nextNoticeTimer.start();
        }
    };

    /**
     * Inserts the new notice in the active notices queue and adds the corresponding indicator above/below the gate.
     */
    NoticeManager.prototype.initNotice = function() {
        this.noticesQueue.peek();

        //Initializes a notice by removing it from the inactive notices queue and adding it to the active notices queue
        var index = this.activeNoticesQueue.enqueue(this.noticesQueue.dequeue());

        //Shows the notice indicator
        this.activeNoticesQueue.queue[index].showIndicator(index);

        //Starts the timer to initialize the next notice in the inactive notices queue
        this.startTimer();

        //Activates the first notice in the active notices queue. This flag is set when the active notices queue
        //becomes empty and no signal can arrive to call the startNotice function. When it is set, the first notice
        //that will be inserted in the active notices queue is immediately activated.
        if (this.flag) {
            this.flag = false;
            this.startNotice();
        }
    };

    /**
     * Activates the notice in the first position of the active notices queue.
     */
    NoticeManager.prototype.startNotice = function() {
        if (this.activeNoticesQueue.queue.length >= 1) {
            this.notice = this.activeNoticesQueue.peek();

            //Initializes and draws the activated notice
            this.notice.draw();

            //Calls the function to switch the gate light to yellow (if this is one of the upper gates)
            this.signalYellow.dispatch(this.gateNumber, true);

            //Starts the timer that will automatically drop the notice
            this.noticeTimer = window.game.time.create(window.game, false);
            this.noticeTimer.loop(this.thirdTimeForANotice, this.manageNoticeIndicators, this);
            this.noticeTimer.start();
        }
        //Inform that active notices queue is empty again and the signalOver of the previous animation has already been dispatched.
        //This way the next notice that will arrive can immediately start, since no more signal will arrive to activate it.
        else {
            this.flag = true;
            this.signalYellow.dispatch(this.gateNumber, false);
        }
    };

    /**
     * Shows the active notice when the player steps in front of the corresponding gate.
     */
    NoticeManager.prototype.show = function() {
        if(this.notice) {
            this.noticeOn = true;
            this.notice.show();
        }
    };

    /**
     * Hides the active notice when the player steps away from the corresponding gate.
     */
    NoticeManager.prototype.hide = function() {
        if (this.notice) {
            this.noticeOn = false;
            this.notice.hide();
        }
    };

    /**
     * Called to allow or drop a connection attempt. It also writes the log entry data to the proper array and calls
     * the functions to launch the concierge explanations and the reply effects animations
     * @param {Object} item - Object representing the keyboard button passed by the input onDown handler function
     * @param {string} action - Indicates whether the connection attempt has been allowed or dropped
     * @param {string} treatas - If provided, it means that the player replied through the "consider as" function. The
     * string content specifies which "consider as" choice has been made
     */
    NoticeManager.prototype.allowDrop = function(item, action, treatas) {
        var treatAsCons;
        var icon;
        this.signalOver.addOnce(this.startNotice, this);
        this.outcome = {};

        if (action === this.notice.connectionAttempt.shouldBe) { //Player is right
            //Reads the outcome object from the consequences file
            this.outcome = $.extend(true, {}, window.game.globals.cons[this.levelNumber][this.notice.connectionAttempt.idCons[0]].right);

            //Log icon id
            icon = 'correctIcon';

            if (action === 'allowed') { //Correctly allowed
                //Calls the function to update level score and statistics
                this.incFunc.call(this.extContext, 0);
            }
            else { //Correctly blocked
                this.incFunc.call(this.extContext, 1);
            }

        }
        else if (action === "autoDrop") { //Notice dropped because the related time has elapsed.
            icon = "warningIcon";
            this.outcome.type = "wrongDrop";
            this.outcome.message = "Hurry up! You let the firewall automatically drop a request!";
            this.outcome.explanation = "This notice has been automatically dropped by the firewall.";
            this.incFunc.call(this.extContext, 4);
        }
        else { //Player is wrong
            this.outcome = $.extend(true, {}, window.game.globals.cons[this.levelNumber][this.notice.connectionAttempt.idCons[0]].wrong);

            if (action === 'allowed') { //Wrongly allowed
                this.incFunc.call(this.extContext, 2);
                icon = 'errorIcon';
            }
            else { //Wrongly blocked
                this.incFunc.call(this.extContext, 3);
                icon = 'warningIcon';
            }
        }

        //In case the consider as option has been chosen, the outcome object is overwritten with the consT file values
        if (treatas) {
            treatAsCons = window.game.globals.consTContent[this.notice.connectionAttempt.idCons[1]][treatas];
            this.outcome.message = treatAsCons.message;
            this.outcome.explanation = treatAsCons.explanation;

            //Calls the animResponse function in the play state passing also the talk type read from the consT file
            this.responseSignal.dispatch(this.outcome, this.gateNumber, treatAsCons.talk);
        }
        else{
            //Calls the animResponse function in the play state passing the information read by the level cons file
            this.responseSignal.dispatch(this.outcome, this.gateNumber, null);
        }

        //Inserts a new object in the log entries array with the information of this notice
        this.logEntries.push({action: action, conn: this.notice.connectionAttempt, icon: icon, explanation: this.outcome.explanation});

        this.noticeOn = false;
        this.removeNotice();

        //If the notice has been dropped, the gate is available immediately to activate a new notice and the level
        //end conditions are checked
        if (action === "dropped" || action === "autoDrop") {
            this.signalOver.dispatch();
            this.signalLevelOver.dispatch();
        }
    };

    /**
     * This function manages the treatAs reply by toggling the "consider as" mode if the C key is pressed or by
     * applying the predefined applications policies if a number key has been pressed
     */
    NoticeManager.prototype.treatAs = function(key) {
        if (key.keyCode === Phaser.Keyboard.C) { //C key pressed
            if (this.notice.treatasOpen) {
                this.notice.treatasHide();
            }
            else {
                this.notice.treatasShow();
            }
        }
        else {
            switch(key.keyCode) {
                case Phaser.Keyboard.ONE:
                    //Trusted App
                    this.allowDrop(null, "allowed", "trust");
                    break;
                case Phaser.Keyboard.TWO:
                    //Blocked App
                    this.allowDrop(null, "dropped", "block");
                    break;
                case Phaser.Keyboard.THREE:
                    //Outgoing only
                    if(this.notice.connectionAttempt.ingoing) {
                        this.allowDrop(null, "dropped", "onlyout");
                    }
                    else {
                        this.allowDrop(null, "allowed", "onlyout");
                    }
                    break;
                case Phaser.Keyboard.FOUR:
                    //Web Browser
                    //Allow requests in the loopback zone
                    if (this.notice.connectionAttempt.ipAddress === "127.0.0.1") {
                        this.allowDrop(null, "allowed", "browser");
                    }
                    if (!this.notice.connectionAttempt.ingoing) {
                        //Outgoing HTTP, FTP, FTP-PASV, DNS
                        if (this.notice.connectionAttempt.appProtocol === "HTTP" ||
                            this.notice.connectionAttempt.appProtocol === "HTTPS" ||
                            this.notice.connectionAttempt.appProtocol === "FTP" ||
                            this.notice.connectionAttempt.appProtocol === "FTP-PASV" ||
                            this.notice.connectionAttempt.appProtocol === "DNS") {
                            this.allowDrop(null, "allowed", "browser");
                        }
                        //Drop any other
                        else {
                            this.allowDrop(null, "dropped", "browser");
                        }
                    }
                    //Drop if ingoing
                    else {
                        this.allowDrop(null, "dropped", "browser");
                    }
                    break;
                case Phaser.Keyboard.FIVE:
                    //Mail Client

                    if (!this.notice.connectionAttempt.ingoing) {
                        //Outgoing POP3, IMAP, SMTP, DNS
                        if (this.notice.connectionAttempt.appProtocol === "POP3" ||
                            this.notice.connectionAttempt.appProtocol === "IMAP" ||
                            this.notice.connectionAttempt.appProtocol === "SMTP" ||
                            this.notice.connectionAttempt.appProtocol === "POP3-SSL" ||
                            this.notice.connectionAttempt.appProtocol === "IMAP-SSL" ||
                            this.notice.connectionAttempt.appProtocol === "SMTP-SSL" ||
                            this.notice.connectionAttempt.appProtocol === "DNS") {
                            this.allowDrop(null, "allowed", "mail");
                        }
                        //Drop any other
                        else {
                            this.allowDrop(null, "dropped", "mail");
                        }
                    }
                    //Drop if ingoing
                    else {
                        this.allowDrop(null, "dropped", "mail");
                    }
                    //Ask rule only if remember my answer is implemented
                    break;
                case Phaser.Keyboard.SIX:
                    //Torrent Client
                    //Outgoing UDP from port 53200 to port [1025-65535]
                    if (!this.notice.connectionAttempt.ingoing && this.notice.connectionAttempt.protocol === "UDP" &&
                        this.notice.connectionAttempt.srcPort === 53200 && this.notice.connectionAttempt.destPort >= 1025 &&
                        this.notice.connectionAttempt.destPort <= 65535) {
                        this.allowDrop(null, "allowed", "torrent");
                    }
                    //Outgoing TCP from port [1025-65535] to port [1025-65535]
                    else if (!this.notice.connectionAttempt.ingoing && this.notice.connectionAttempt.protocol === "TCP" &&
                        this.notice.connectionAttempt.srcPort >= 1025 && this.notice.connectionAttempt.srcPort <= 65535 &&
                        this.notice.connectionAttempt.destPort >= 1025 && this.notice.connectionAttempt.destPort <= 65535) {
                        this.allowDrop(null, "allowed", "torrent");
                    }
                    //Outgoing TCP from port [1025-65535] to port 80
                    else if (!this.notice.connectionAttempt.ingoing && this.notice.connectionAttempt.protocol === "TCP" &&
                        this.notice.connectionAttempt.srcPort >= 1025 && this.notice.connectionAttempt.srcPort <= 65535 &&
                        this.notice.connectionAttempt.destPort === 80) {
                        this.allowDrop(null, "allowed", "torrent");
                    }
                    //Ingoing TCP or UDP from port [1025-65535] to port 53200
                    else if (this.notice.connectionAttempt.ingoing && this.notice.connectionAttempt.srcPort >= 1025 &&
                        this.notice.connectionAttempt.srcPort <= 65535 && this.notice.connectionAttempt.destPort === 53200) {
                        this.allowDrop(null, "allowed", "torrent");
                    }
                    //Drop any other
                    else {
                        this.allowDrop(null, "dropped", "torrent");
                    }
                    break;
            }
        }
    };

    /**
     * Handles the notices destruction operations.
     */
    NoticeManager.prototype.removeNotice = function() {
        this.noticeTimer.removeAll();
        this.noticeTimer.destroy();
        this.notice.disableKeys();
        this.activeNoticesQueue.dequeue();
        this.notice.noticeIndicator.destroy();
        this.notice.controlGroup.destroy();
        this.notice.treatasGroup.destroy();
        delete this.notice;
        $.each(this.activeNoticesQueue.queue, function(index, obj) {
            window.game.add.tween(obj.noticeIndicator).to({x: obj.indicatorX + 25 * index}, 500, Phaser.Easing.Quadratic.In, true);
        });
    };

    /**
     * Manages the notice indicators which show the time left to answer the active notice.
     */
    NoticeManager.prototype.manageNoticeIndicators = function() {
        //If the time elapses and the indicator is blue, turns it to yellow
        if (this.notice.noticeIndicator.key === "blueMark") {
            this.notice.noticeIndicator.loadTexture('yellowMark');
        }
        //If the time elapses and the indicator is yellow, turns it to red
        else if (this.notice.noticeIndicator.key === "yellowMark") {
            this.notice.noticeIndicator.loadTexture('redMark');
        }
        //If the time elapses and the indicator is red, automatically drops the notice
        else {
            this.allowDrop(null, "autoDrop");
        }
    };

    /**
     * Deactivates the notice manager, disabling and removing all its notices to prevent the player to further reply
     * to them. It is used in the last level to deletes all the notices when the level objectives have been met
     * also if there are still notices to reply to.
     */
    NoticeManager.prototype.deactivate = function () {
        if (this.noticeTimer) {
            this.noticeTimer.destroy();
        }
        if (this.nextNoticeTimer) {
            this.nextNoticeTimer.destroy();
        }

        this.signalOver.removeAll();

        $.each(this.activeNoticesQueue.queue, function(index, obj) {
            obj.hideIndicator();
        });
        if (this.noticeOn) {
            this.notice.hide();
        }

        if (this.notice) {
            this.notice.disableKeys();
        }

        this.noticeOn=true;
    };

    return NoticeManager;
};