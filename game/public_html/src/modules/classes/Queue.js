module.exports = function() {

    /**
     * @classdesc Small class to manage an array queue.
     * @constructor
     */
    function Queue() {
        this.queue  = [];
    }

    /**
     * Checks the length of the queue.
     * @returns {int} Queue length.
     */
    Queue.prototype.getLength = function(){
        return this.queue.length;
    };

    /**
     * Checks whether the queue is empty.
     * @returns {boolean} Returns true if the queue is empty, and false otherwise.
     */
    Queue.prototype.isEmpty = function(){
        return (this.queue.length === 0);
    };

    /**
     * Queues the specified item.
     * @param {Object} item - The item to enqueue
     * @returns {Number} elementIndex  The index of the element inserted into the queue
     */
    Queue.prototype.enqueue = function(item){
        return this.queue.push(item) - 1;
    };

    /**
     *  Dequeues an item and returns it.
     *  @returns {Object}  The removed item or 'undefined' if the queue is empty.
     */
    Queue.prototype.dequeue = function(){

        // if the queue is empty, return immediately
        if (this.queue.length === 0) {
            return undefined;
        }

        // store the item at the front of the queue
        return this.queue.shift();

    };

    /**
     * Returns the item at the front of the queue without dequeuing it.
     * @returns {Object} The first item in queue or 'undefined' if the queue is empty.
     */
    Queue.prototype.peek = function(){
        return (this.queue.length > 0 ? this.queue[0] : undefined);
    };

    return Queue;
};