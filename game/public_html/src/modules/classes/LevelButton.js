/* global Phaser */

module.exports = function() {

    /**
     * @classdesc Represents a button on the level selection screen.
     * @param {int} levelNumber - Number of the level the button takes to
     * @param {function} clickFunc - Function to call when the button is clicked
     * @constructor
     */
    function LevelButton(levelNumber, clickFunc) {
        //Computes the grid position index considering a grid of 15 elements
        this.levelNumberGrid = levelNumber%15;

        this.levelNumber = levelNumber;
        this.controlGroup = window.game.add.group();
        this.box = this.controlGroup.create(this.levelNumberGrid%5 * 140 + 90, parseInt(this.levelNumberGrid/5) * 140 + 90, 'levelButton');
        this.box.anchor.set(0.5, 0.5);
        this.controlGroup.pivot.x = this.box.x + this.box.width/2;
        this.controlGroup.pivot.y = this.box.y + this.box.height/2;
        this.controlGroup.x += this.controlGroup.pivot.x;
        this.controlGroup.y += this.controlGroup.pivot.y;

        //Creates the text element to write the level number on the button
        this.style = { font: "25px Courier New, monospace", fill: "#00AA11", align: "center"};
        this.text = window.game.add.text(this.box.x + 36, this.box.y + 23, Number(levelNumber+1), this.style);
        this.text.anchor.set(0.5, 0.5);
        this.controlGroup.add(this.text);

        this.box.inputEnabled = true;
        this.box.events.onInputOver.add(this.overFunc, this);
        this.box.events.onInputOut.add(this.outFunc, this);

        this.clickSound = window.game.add.audio('buttonClick');
        this.wrongClickSound = window.game.add.audio('wrong');

        var i;
        //Reads the save data to print the correct amount of shields earned
        if(window.game.globals.savedata && window.game.globals.savedata[levelNumber]) { //Level completed
            //Prints as many shields as the number written in the save data
            for (i=0; i<window.game.globals.savedata[levelNumber].shields; i++) {
                this.controlGroup.create(this.box.x + 7 + 20*i, this.box.y + 47, 'shield');
            }
            //Prints as many empty shields as necessary to arrive to three shields/empty shields
            for (; i<3; i++) {
                this.controlGroup.create(this.box.x + 7 + 20*i, this.box.y + 47, 'shieldNotEarned');
            }
        }
        else { //Level not completed
            //Prints three empty shields because the level has never been completed
            for (i=0; i<3; i++) {
                this.controlGroup.create(this.box.x + 7 + 20*i, this.box.y + 47, 'shieldNotEarned');
            }
        }

        //Check if level can be played
        //That is if it has already been completed, if it is the first level or if the previous level has been completed
        //If it cannot be played a big red X is placed on it and the parameter false is passed to clickFunc
        if ( (window.game.globals.savedata && window.game.globals.savedata[levelNumber]) || levelNumber === 0 || (levelNumber !== 0 && window.game.globals.savedata && window.game.globals.savedata[levelNumber-1]) ) {
            this.box.events.onInputDown.add(clickFunc, this, 0, true);
        }
        else {
            this.controlGroup.create(this.box.x, this.box.y, 'redX');
            this.box.events.onInputDown.add(clickFunc, this, 0, false);
        }

        this.box.position.x = this.controlGroup.x;
        this.box.position.y = this.controlGroup.y;
    }

    /**
     * Called when the mouse pointer goes over the button enlarging the image.
     */
    LevelButton.prototype.overFunc = function() {
        window.game.add.tween(this.controlGroup.scale).to({x: 1.5, y:1.5}, 350, Phaser.Easing.Linear.None, true);
    };

    /**
     * Called when the mouse pointed goes out of the button restoring the image size.
     */
    LevelButton.prototype.outFunc = function() {
        window.game.add.tween(this.controlGroup.scale).to({x: 1, y:1}, 350, Phaser.Easing.Linear.None, true);
    };

    /**
     * Removes the displayed button from the screen and from memory.
     */
    LevelButton.prototype.destroyButton = function() {
        delete this.levelNumber;
        delete this.style;
        this.controlGroup.destroy();
    };

    return LevelButton;
};