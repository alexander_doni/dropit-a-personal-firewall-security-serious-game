module.exports = function() {
    /**
     * @classdesc Class representing the PDA screen in game. It manages the buttons and the general appearance of the device.
     * @param {function} lArrowFunc - Function called when the left arrow is pressed
     * @param {function} homeFunc - Function called when the home button is pressed
     * @param {function} rArrowFunc - Function called when the right arrow is pressed
     * @param {string} tag1 - String that explains what the left arrow button does.
     * It appears in a small box when mouse is hover the button.
     * @param {string} tag2 - String that explains what the home button does.
     * It appears in a small box when mouse is hover the button.
     * @param {string} tag3 - String that explains what the right arrow button does.
     * It appears in a small box when mouse is hover the button.
     * @param {Object} context - Context in which the three buttons functions have to be executed
     * @constructor
     */
    function Pda (lArrowFunc, homeFunc, rArrowFunc, tag1, tag2, tag3, context) {
        this.controlGroup = window.game.add.group();
        this.lArrowFunc = lArrowFunc;
        this.homeFunc = homeFunc;
        this.rArrowFunc = rArrowFunc;
        this.context = context;

        this.buttonSound = window.game.add.audio('buttonClick');

        //Drawing main PDA image
        this.pda = this.controlGroup.create(0, 0, 'pda');

        //Drawing buttons and adding click event handlers
        this.button1 = window.game.add.button(window.game.world.centerX - 250, 535, 'larrowb', this.lArrowFuncWrapper, this, 2, 0, 1, 2);
        this.button1.anchor.setTo(0.5, 0.5);
        this.button1.onInputOver.add(this.showHintBox, this, 0, tag1);
        this.button1.onInputOut.add(this.hideHintBox, this, 0);
        this.controlGroup.add(this.button1);

        this.button2 = window.game.add.button(window.game.world.centerX, 538, 'homeb', this.homeFuncWrapper, this, 2, 0, 1, 2);
        this.button2.anchor.setTo(0.5, 0.5);
        this.button2.onInputOver.add(this.showHintBox, this, 0, tag2);
        this.button2.onInputOut.add(this.hideHintBox, this, 0);
        this.controlGroup.add(this.button2);

        this.button3 = window.game.add.button(window.game.world.centerX + 250, 535, 'rarrowb', this.rArrowFuncWrapper, this, 2, 0 ,1, 2);
        this.button3.anchor.setTo(0.5, 0.5);
        this.button3.onInputOver.add(this.showHintBox, this, 0, tag3);
        this.button3.onInputOut.add(this.hideHintBox, this, 0);
        this.controlGroup.add(this.button3);

        //Creating hint box sprites and text
        this.hintBoxGroup = window.game.add.group();
        this.hintBox = this.hintBoxGroup.create(0, 0, 'hintBox');
        this.hintBox.anchor.setTo(0.5, 0.5);
        this.hintBoxText = window.game.add.text(0, 0, "", {font: "13px Courier New, monospace", fill: "#00AA11", align: "center", wordWrap: true, wordWrapWidth: this.hintBox.width - 10});
        this.hintBoxText.anchor.setTo(0.5, 0);
        this.hintBoxGroup.add(this.hintBoxText);
        this.hintBoxGroup.visible = false;
    }

    /**
     * Hides the current Pda.
     */
    Pda.prototype.hide = function() {
        this.controlGroup.visible = false;
        this.hintBoxGroup.visible = false;
    };

    /**
     * Shows the current Pda.
     */
    Pda.prototype.show = function() {
        this.controlGroup.visible = true;
        this.hintBoxGroup.visible = true;
    };

    /**
     * Shows a hint box to better explain the Pda buttons function. It is showed when the mouse hovers a button.
     * @param {Phaser.Sprite} button - The sprite object of the clicked button
     * @param {Phaser.Pointer} item - A pointer object passed by the Phaser.Signal dispatch function
     * @param {string} tag - String to show on the hint box
     */
    Pda.prototype.showHintBox = function(button, item, tag) {
        this.hintBox.position.x = button.position.x;
        this.hintBox.position.y = button.position.y - 80;
        this.hintBoxText.position.x = this.hintBox.position.x;
        this.hintBoxText.position.y = this.hintBox.position.y - this.hintBox.height/2 + 10;
        this.hintBoxText.setText(tag);
        window.game.world.bringToTop(this.hintBoxGroup);
        this.hintBoxGroup.visible = true;
    };

    /**
     * Hides the hint box when the mouse cursor stops hovering a Pda button.
     */
    Pda.prototype.hideHintBox = function() {
        this.hintBoxGroup.visible = false;
    };

    /**
     * Wraps the left arrow button function to hide the hint box and play the click sound. The function is, however,
     * executed in the original context.
     */
    Pda.prototype.lArrowFuncWrapper = function() {
        this.hideHintBox();
        this.buttonSound.play("", 0, window.game.globals.soundEffectsOn);
        this.lArrowFunc.call(this.context);
    };

    /**
     * Wraps the home button function to hide the hint box and play the click sound. The function is, however,
     * executed in the original context.
     */
    Pda.prototype.homeFuncWrapper = function() {
        this.hideHintBox();
        this.buttonSound.play("", 0, window.game.globals.soundEffectsOn);
        this.homeFunc.call(this.context);
    };

    /**
     * Wraps the right arrow button function to hide the hint box and play the click sound. The function is, however,
     * executed in the original context.
     */
    Pda.prototype.rArrowFuncWrapper = function() {
        this.hideHintBox();
        this.buttonSound.play("", 0, window.game.globals.soundEffectsOn);
        this.rArrowFunc.call(this.context);
    };

    /**
     * It changes the Pda buttons hint boxes tags to suit the current Pda use.
     * @param {function} lArrowFunc - Function called when the left arrow is pressed
     * @param {function} homeFunc - Function called when the home button is pressed
     * @param {function} rArrowFunc - Function called when the right arrow is pressed
     * @param {string} tag1 - String that explains what the left arrow button does.
     * It appears in a small box when mouse is hover the button.
     * @param {string} tag2 - String that explains what the home button does.
     * It appears in a small box when mouse is hover the button.
     * @param {string} tag3 - String that explains what the right arrow button does.
     * It appears in a small box when mouse is hover the button.
     * @param {Object} context - Context in which the three buttons functions have to be executed
     */
    Pda.prototype.updateHandlers = function(lArrowFunc, homeFunc, rArrowFunc, tag1, tag2, tag3, context) {
        this.lArrowFunc = lArrowFunc;
        this.homeFunc = homeFunc;
        this.rArrowFunc = rArrowFunc;
        this.context = context;
        this.button1.onInputOver.removeAll();
        this.button1.onInputOver.add(this.showHintBox, this, 0, tag1);
        this.button2.onInputOver.removeAll();
        this.button2.onInputOver.add(this.showHintBox, this, 0, tag2);
        this.button3.onInputOver.removeAll();
        this.button3.onInputOver.add(this.showHintBox, this, 0, tag3);
    };

    return Pda;
};