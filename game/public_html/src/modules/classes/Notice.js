/*global Phaser*/

module.exports = function() {
    /**
     * @classdesc Represents a firewall notice regarding a new connection attempt.
     * @param {ConnectionAttempt} connectionAttempt - The connection attempt of which the notice is about
     * @param {function} allowFunc - Function called if the connection is allowed
     * @param {function} treatasFunc - Function called if the connection is chosen to be treated as a predefined application
     * @param {function} dropFunc - Function called if the connection is dropped
     * @param {Object} context - Context in which the three actions functions have to be executed
     * @param {int} brokenFunction - Number representing if and which between the allow/drop and the consider as functions
     * cannot be used
     * @param {Phaser.Group} lowerZGroup - The sprites belonging to this group are drawed under the others not belonging to it
     * @constructor
     */
    function Notice(connectionAttempt, allowFunc, treatasFunc, dropFunc, context, brokenFunction, lowerZGroup) {
        //Defining class Properties
        this.connectionAttempt = connectionAttempt;
        this.indicatorX = 50 + 300* ((connectionAttempt.gate > 2) ? connectionAttempt.gate-3: connectionAttempt.gate);
        this.indicatorY = 10 + ((connectionAttempt.gate > 2) ? 535 : 0);
        this.noticeX = 1500;
        this.noticeY = 200;
        this.noticeTimer = 0;
        this.allowFunc = allowFunc;
        this.treatasFunc = treatasFunc;
        this.dropFunc = dropFunc;
        this.context = context;
        this.treatasOpen = false;
        this.treatasText = [];
        this.treatasChoiceKeys = [];
        this.brokenFunction = brokenFunction;
        this.lowerZGroup = lowerZGroup;
    }

    /**
     * Draws and initializes a notice when the player steps for the first time in front of the gate on which this
     * notice was waiting.
     */
    Notice.prototype.draw = function() {
        this.controlGroup = window.game.add.group();
        this.box = this.controlGroup.create(this.noticeX, this.noticeY, 'notice');
        this.box.anchor.set(1, 0.5);
        this.startX = this.box.x - this.box.width;
        this.startY = this.box.y - this.box.height/2;

        //Header
        this.alertText = window.game.add.text(this.startX + 50, this.startY + 15, "FIREWALL ALERT!", {font: "20px Courier New, monospace", fill: "red"});
        this.controlGroup.add(this.alertText);
        this.alertIconWhite = this.controlGroup.create(this.startX + 250, this.startY, 'alertWhite');
        this.alertIconOrange = this.controlGroup.create(this.startX + 250, this.startY, 'alertOrange');

        //Information box with all the connection attempt related information
        this.style = { font: "13px Courier New, monospace", fill: "#00AA11", wordWrap: true, wordWrapWidth: 240};
        this.applicationText = window.game.add.text(this.startX + 25, this.startY + 70, "Application: " + this.connectionAttempt.application, this.style);
        this.controlGroup.add(this.applicationText);

        this.addressText = window.game.add.text(this.startX + 25, this.startY + 100, "IP Address: " + this.connectionAttempt.ipAddress, this.style);
        this.controlGroup.add(this.addressText);

        this.portText = window.game.add.text(this.startX + 25, this.startY + 130, "Port: " + this.connectionAttempt.destPort +
            " " + this.connectionAttempt.protocol + ((this.connectionAttempt.appProtocol) ? ("   (" + this.connectionAttempt.appProtocol + ")") : ""), this.style);
        this.controlGroup.add(this.portText);

        this.arrow = this.controlGroup.create(this.startX + 252, this.startY + 65, (this.connectionAttempt.ingoing) ? 'inArrow' : 'outArrow');

        //Tweaking pivot to ensure a correct animation trajectory
        this.controlGroup.pivot.x = this.box.x + this.box.width/2;
        this.controlGroup.pivot.y = this.box.x + this.box.height/2;
        this.controlGroup.x += this.controlGroup.pivot.x;
        this.controlGroup.y += this.controlGroup.pivot.y;

        //In case the broken function variable in the level definition file is set, the corresponding key(s) is/are not
        //bound
        if (this.brokenFunction !== 1) {
            this.allowKey = window.game.input.keyboard.addKey(Phaser.Keyboard.A);
            this.dropKey = window.game.input.keyboard.addKey(Phaser.Keyboard.D);
        }
        if (this.brokenFunction !== 2) {
            this.treatAsKey = window.game.input.keyboard.addKey(Phaser.Keyboard.C);
        }

        //Tween to make the firewall alert text blink
        window.game.add.tween(this.alertText).to({ alpha: 0}, 70, Phaser.Easing.Linear.None, false).delay(700, 0)
            .to({ alpha: 1}, 70, Phaser.Easing.Linear.None, false).delay(700, 1).repeatAll(-1).start();

        //Tween to make the alert icon change colour
        window.game.add.tween(this.alertIconOrange).to({ alpha: 0}, 70, Phaser.Easing.Linear.None, false).delay(700, 0)
            .to({ alpha: 1}, 70, Phaser.Easing.Linear.None, false).delay(700, 1).repeatAll(-1).start();
        this.controlGroup.alpha = 0;

        //Binding "consider as" keys
        this.treatasChoiceKeys[0] = window.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.treatasChoiceKeys[1] = window.game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.treatasChoiceKeys[2] = window.game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        this.treatasChoiceKeys[3] = window.game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
        this.treatasChoiceKeys[4] = window.game.input.keyboard.addKey(Phaser.Keyboard.FIVE);
        this.treatasChoiceKeys[5] = window.game.input.keyboard.addKey(Phaser.Keyboard.SIX);

        //Creating "consider as" array of text
        this.treatasGroup = window.game.add.group();
        this.treatasText[0] = window.game.add.text(470, this.startY + 70, "1) Trusted App", this.style);
        this.treatasText[1] = window.game.add.text(620, this.startY + 70, "2) Blocked App", this.style);
        this.treatasText[2] = window.game.add.text(470, this.startY + 100, "3) Outgoing Only", this.style);
        this.treatasText[3] = window.game.add.text(620, this.startY + 100, "4) Web Browser", this.style);
        this.treatasText[4] = window.game.add.text(470, this.startY + 130, "5) Mail Client", this.style);
        this.treatasText[5] = window.game.add.text(620, this.startY + 130, "6) Torrent Client", this.style);

        this.treatasText.forEach(function(obj) {
            this.treatasGroup.add(obj);
        }, this);

        this.treatasGroup.visible = false;
    };

    /**
     * Hides the notice. It is called when the players steps away from the corresponding gate.
     */
    Notice.prototype.hide = function() {
        window.game.add.tween(this.controlGroup.position).to({x: this.noticeX},1000,Phaser.Easing.Linear.None,true, 0, 0);
        window.game.add.tween(this.controlGroup).to({alpha: 0},1000,Phaser.Easing.Linear.None,true, 0, 0);

        if (this.treatasOpen) {
            this.treatasGroup.visible = false;
            this.treatasChoiceKeys.forEach(function(obj) {
                obj.onDown.removeAll();
            }, this);
        }

        this.disableKeys();
    };

    /**
     * Shows the notice. It is called when the players steps in front of the corresponding gate.
     */
    Notice.prototype.show = function() {
        var tween = window.game.add.tween(this.controlGroup.position).to({x: 950},1000,Phaser.Easing.Linear.None,true, 0, 0);
        window.game.add.tween(this.controlGroup).to({alpha: 1},1000,Phaser.Easing.Linear.None,true, 0, 0);

        if (this.treatasOpen) {
            tween.onComplete.add(function() {
                this.treatasGroup.visible = true;
                this.treatasChoiceKeys.forEach(function(obj) {
                    obj.onDown.add(this.treatasFunc, this.context);
                }, this);
            }, this);
        }

        //If the broken function variable is set, the corresponding key(s) event(s) is/are not bound
        if (this.brokenFunction !== 1) {
            this.allowKey.onDown.add(this.allowFunc, this.context, 0, "allowed", null);
            this.dropKey.onDown.add(this.dropFunc, this.context, 0, "dropped", null);
        }
        if (this.brokenFunction !== 2) {
            this.treatAsKey.onDown.add(this.treatasFunc, this.context);
        }
    };

    /**
     * Toggles the "consider as" mode of the firewall notice to show the "consider as" options. It is called
     * when the "consider as" button is pressed and the related mode was not active.
     */
    Notice.prototype.treatasShow = function() {
        this.treatasOpen = true;

        //Tweens that enlarge the notice box
        var tween = window.game.add.tween(this.box).to({width: 380}, 700, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.alertText).to({x: 1200}, 700, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.alertIconWhite).to({x: 1400}, 700, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.alertIconOrange).to({x: 1400}, 700, Phaser.Easing.Linear.None, true, 0, 0);

        this.treatAsKey.onDown.removeAll();

        tween.onComplete.add(function () {
            this.treatAsKey.onDown.add(this.treatasFunc, this.context);
            this.applicationText.visible = false;
            this.addressText.visible = false;
            this.portText.visible = false;
            this.arrow.visible = false;

            this.treatasGroup.visible = true;

            this.treatasChoiceKeys.forEach(function(obj) {
                obj.onDown.add(this.treatasFunc, this.context);
            }, this);
        }, this);
    };

    /**
     * Toggles the "consider as" mode of the firewall notice to show the original information. It is called
     * when the "consider as" button is pressed and the related mode was active.
     */
    Notice.prototype.treatasHide = function() {
        this.treatasOpen = false;
        window.game.add.tween(this.box).to({width: 300}, 700, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.alertText).to({x: 1250}, 700, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.alertIconWhite).to({x: 1450}, 700, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.alertIconOrange).to({x: 1450}, 700, Phaser.Easing.Linear.None, true, 0, 0);
        this.applicationText.visible = true;
        this.addressText.visible = true;
        this.portText.visible = true;
        this.arrow.visible = true;

        this.treatasGroup.visible = false;

        this.treatasChoiceKeys.forEach(function(obj) {
            obj.onDown.removeAll();
        }, this);
    };

    /**
     * Disables the keyboard keys to give a reply to the notice. It is executed after a notice has been hidden
     */
    Notice.prototype.disableKeys = function() {
        //Does not disable the event(s) of a broken function
        if (this.brokenFunction !== 1) {
            this.allowKey.onDown.removeAll();
            this.dropKey.onDown.removeAll();
        }
        if (this.brokenFunction !== 2) {
            this.treatAsKey.onDown.removeAll();
        }

        this.treatasChoiceKeys.forEach(function(obj) {
            obj.onDown.removeAll();
        }, this);
    };

    /**
     * Shows an indicator next to the corresponding gate when the notice is activated.
     */
    Notice.prototype.showIndicator = function(queueIndex) {
        this.noticeIndicator = this.lowerZGroup.create(this.indicatorX + 25*queueIndex, this.indicatorY, 'blueMark');
    };

    /**
     * Hides the notice related indicator.
     */
    Notice.prototype.hideIndicator = function () {
        this.noticeIndicator.visible = false;
    };

    return Notice;
};