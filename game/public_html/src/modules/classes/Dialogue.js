/*global Phaser*/
var dynamic_text = require('../dynamic_text');
var Queue = require('./Queue')();

module.exports = function() {

    /**
     * @classdesc Class representing a dialogue unit inside the game. Each level can only have one dialogue unit per talking character.
     * E.g. only one for the guide character and only one for the boss.
     * @constructor
     */
    function Dialogue(helpMessage, player, levelNumber, signalStory, face) {
        //Initialization
        this.messageQueue = new Queue();
        this.dialogueOn = false;
        this.writingInProgress = false;
        this.helpMessage = helpMessage;
        this.helpMessageOn = false;
        this.player = player;
        this.levelNumber = levelNumber;
        this.signalStory = signalStory;

        this.typingSound = window.game.add.audio('typing');
        this.incomingCallSound = window.game.add.audio('incomingCall');

        //Read the data about the story dialogues
        this.parseDialogueData();

        this.isFromDown = true;

        //Keyboard keys initialization
        if (helpMessage) {
            this.helpKey = window.game.input.keyboard.addKey(Phaser.Keyboard.H);
        }
        this.skipDialogueKey = window.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.controlGroup = window.game.add.group();
        this.controlGroup.visible = false;
        this.signal = new Phaser.Signal();
        this.signal.add(this.activateInput, this);
        this.storyMessageSignal = new Phaser.Signal();
        this.storyMessageSignal.add(this.storyMessageWritten, this);

        //Dialogue box creation
        this.box = this.controlGroup.create(400, window.game.height - 35, 'dialogue_box');
        this.box.anchor.set(0.5, 0.5);

        //Space bar and related tween creation
        this.spaceBar = this.controlGroup.create(this.box.x + this.box.width/2 -80,
            this.box.y + this.box.height/2 - 30, 'spaceBar');
        this.spaceBar.anchor.set(0.5, 0.5);
        this.spaceBar.width = 39;
        this.spaceBar.height = 13;
        this.spaceBar.visible = false;
        this.spaceBarTween = window.game.add.tween(this.spaceBar.scale).to({x: 0.35, y:0.35}, 1000,
            Phaser.Easing.Linear.None, false, 0, -1, true).start();
        this.spaceBarTween.pause();

        this.style = { font: "15px Courier New, monospace", fill: "#00AA11", align: "left", wordWrap: true, wordWrapWidth: this.box.width - 195};
        this.messageText = window.game.add.text(this.box.x - this.box.width/2 + 155, this.box.y - this.box.height/2 + 25, "", this.style);
        this.controlGroup.add(this.messageText);

        //Pivot setting pivot to ensure a correct animation trajectory
        this.controlGroup.pivot.x = this.box.x + this.box.width/2;
        this.controlGroup.pivot.y = this.box.x + this.box.height/2;
        this.controlGroup.x += this.box.x + this.box.width/2;
        this.controlGroup.y += this.box.x + this.box.height/2;
        //Modifying pivot changes box position so new position has to be set
        this.controlGroup.position.y = window.game.height + 150;
        this.controlGroup.position.x = 720;

        //Defining animations for facial expressions
        this.face = this.controlGroup.create(this.box.x - this.box.width/2 + 40, this.box.y - 48, face);
        if (face === 'conciergeFace') {
            //Severe
            this.face.animations.add('wrongDrop', [0, 8, 0, 8, 4, 1, 4, 1], 3, true);
            //Angry
            this.face.animations.add('warning', [10, 2], 3, true);
            //Displeased
            this.face.animations.add('error', [15, 7], 3, true);
            //Happy
            this.face.animations.add('correctAllow', [5, 13, 5, 13, 14, 6, 14, 6], 3, true);
            //Enthusiastic
            this.face.animations.add('correctDrop', [11, 3], 3, true);
        }
        else if (face === 'bossFace') {
            //Angry
            this.face.animations.add('wrongDrop', [0, 2, 5, 2], 3, true);
            //Badly surprised
            this.face.animations.add('warning', [11, 3], 3, true);
            //Angry
            this.face.animations.add('error', [0, 2, 5, 2], 3, true);
            //Happy
            this.face.animations.add('correctAllow', [9, 1], 3, true);
            //Happy
            this.face.animations.add('correctDrop', [9, 1], 3, true);
        }
    }

    /**
     * Reads the file l#_dialogue.txt to retrieve the level story dialogues information. This function executes the
     * parsing of the text file given the know delimiters.
     * _: To subdivide the start level dialogue from the end level one;
     * #: (OPTIONAL) To subdivide the concierge dialogue from the boss one;
     * &: To divide a dialogue line from its talking animation;
     * new line: To divide each dialogue entry. Note that the new line can be either \n, \r, or \r\n and all three cases
     * are considered.
     */
    Dialogue.prototype.parseDialogueData = function () {
        var lineSplit, finalSplit, conciergeBossSplit, j;
        this.startDialogue = [];
        this.endDialogue = [];
        var startEndSplit = window.game.globals.storyDialogue[this.levelNumber].split(/_\r\n|_\r|_\n/);

        //Concierge message at the beginning of a level
        conciergeBossSplit = startEndSplit[0].split(/#\r\n|#\r|#\n/);
        lineSplit = conciergeBossSplit[0].split(/\r\n|\r|\n/);
        for (j=0; j<lineSplit.length; j++) {
            this.startDialogue[j] = {};
            finalSplit = lineSplit[j].split("&");
            this.startDialogue[j].message = finalSplit[0];
            this.startDialogue[j].talkType = finalSplit[1];
        }

        //Optional boss messages at the beginning of a level
        if (conciergeBossSplit[1]) {
            lineSplit = conciergeBossSplit[1].split(/\r\n|\r|\n/);
            this.startBossDialogue = [];
            for (j=0; j<lineSplit.length; j++) {
                this.startBossDialogue[j] = {};
                finalSplit = lineSplit[j].split("&");
                this.startBossDialogue[j].message = finalSplit[0];
                this.startBossDialogue[j].talkType = finalSplit[1];
            }
        }

        //Concierge message at the end of a level
        conciergeBossSplit = startEndSplit[1].split(/#\r\n|#\r|#\n/);
        lineSplit = conciergeBossSplit[0].split(/\r\n|\r|\n/);
        for (j=0; j<lineSplit.length; j++) {
            finalSplit = lineSplit[j].split("&");
            if (finalSplit[1]) {
                this.endDialogue[j] = {};
                this.endDialogue[j].message = finalSplit[0];
                this.endDialogue[j].talkType = finalSplit[1];
            }
        }

        //Optional boss message at the end of a level
        if (conciergeBossSplit[1]) {
            lineSplit = conciergeBossSplit[1].split(/\r\n|\r|\n/);
            this.endBossDialogue = [];
            for (j = 0; j < lineSplit.length; j++) {
                finalSplit = lineSplit[j].split("&");
                if (finalSplit[1]) {
                    this.endBossDialogue[j] = {};
                    this.endBossDialogue[j].message = finalSplit[0];
                    this.endBossDialogue[j].talkType = finalSplit[1];
                }
            }
        }
    };

    /**
     * Launched by the play state when the level has been fully loaded. It generates the start dialogue of the level
     * where the player receives important objectives information.
     * @param {boolean} conciergeDialogue - Parameter indicating whether the dialogue belongs to the guide character or not
     */
    Dialogue.prototype.startLevelDialogue = function(conciergeDialogue) {
        this.conciergeDialogue = conciergeDialogue;

        //Level start call
        this.incomingCallSound.play("", 0, window.game.globals.soundEffectsOn).onStop.addOnce(function () {
            this.signalStory.addOnce(this.removeStoryDialogue, this, 1);
            this.controlGroup.visible = true;
            this.popupFromDown();
            this.storyIndex = 0;
            this.storyDialogue = true;
            this.storyDialogueIsStart = true;
            if (this.conciergeDialogue) {
                this.writeStoryDialogue(this.startDialogue);
            }
            else {
                this.writeStoryDialogue(this.startBossDialogue);
            }
        }, this);
    };

    /**
     * Launched when the mission is over. It launches the last dialogue sequence.
     * @param {boolean} conciergeDialogue - Parameter indicating whether the dialogue belongs to the guide character or not
     */
    Dialogue.prototype.endLevelDialogue = function(conciergeDialogue) {

        if (conciergeDialogue) {
            this.conciergeDialogue = conciergeDialogue;
        }

        if (this.dialogueOn) {
            //This is the case in which a dialogue is already on, for example the concierge is
            //cheering the player for a successful drop, and the module will wait for that dialogue to disappear before
            //to start the end level one
            this.endLevelDialogueWaiting = true;
        } else {
            this.signalStory.addOnce(this.removeStoryDialogue, this, 1);
            if (this.helpKey) {
                this.helpKey.onDown.removeAll();
            }

            //Level end call
            this.incomingCallSound.play("", 0, window.game.globals.soundEffectsOn).onStop.addOnce(function () {
                this.messageText.setText("");
                this.popupFromDown();
                this.storyIndex = 0;
                this.storyDialogue = true;
                this.storyDialogueIsStart = false;
                if (this.conciergeDialogue) {
                    this.writeStoryDialogue(this.endDialogue);
                }
                else {
                    this.writeStoryDialogue(this.endBossDialogue);
                }
            }, this);
        }
    };

    /**
     * It can be used to write any dialogue during the game which is not bound the the beginning or the end of the level.
     * It has been used to better support the story in the last level.
     * @param {Array} dialogueVett - Strings array containing the dialogue sentences
     * @param {Phaser.Signal} dialogueOver - Signal dispatched when the dialogue has been completely read by the player
     */
    Dialogue.prototype.normalDialogue = function (dialogueVett, dialogueOver) {
        if (this.helpKey) {
            this.helpKey.onDown.removeAll();
        }
        this.normalDialogueOver = dialogueOver;
        this.normalDialogueOver.addOnce(this.removeStoryDialogue, this, 1);
        this.popupFromDown();
        this.storyIndex = 0;
        this.storyDialogue = false;
        this.writeStoryDialogue(dialogueVett);
    };

    /**
     * Manages the writing of a single dialogue sentence.
     * @param {Array} dialogueVett - Strings array with all the dialogue sentences. The function takes care of choosing
     * the right entry element
     */
    Dialogue.prototype.writeStoryDialogue = function(dialogueVett) {
        if (!this.currentDialogue) { //Enters the first time to write the dialogue entries to be printed
            this.currentDialogue = dialogueVett;
        }
        if (this.storyIndex === this.currentDialogue.length) { //When also the last dialogue line has been printed
            if (this.normalDialogueOver) { //Dispatches the proper signal if this is a normal dialogue
                this.normalDialogueOver.dispatch();
            }
            else { //Dispatches the proper signal if this is a story dialogue
                this.signalStory.dispatch(this.storyDialogueIsStart, this.conciergeDialogue);
            }
            return;
        }

        //Dialogue line writing
        this.typingSound.play("", 0, window.game.globals.soundEffectsOn, true);
        this.writeTimer = dynamic_text.write_one(this.messageText, 40, this.currentDialogue[this.storyIndex].message, this.storyMessageSignal);
        this.talkAnimation = this.face.play(this.currentDialogue[this.storyIndex].talkType);
    };

    /**
     * Function launched when a story message has been written to activate the possibility for the player to skip to
     * the next sentence. Moreover it stops the talking character animation and the typing sound.
     */
    Dialogue.prototype.storyMessageWritten = function () {
        this.talkAnimation.stop(true);
        this.typingSound.stop();
        this.skipDialogueKey.onDown.addOnce(this.nextStoryDialogue, this);
        this.spaceBar.visible = true;
        this.spaceBarTween.resume();
    };

    /**
     * Launched when the player skips to the next dialogue sentence.
     */
    Dialogue.prototype.nextStoryDialogue = function() {
        this.spaceBar.visible = false;
        this.spaceBarTween.pause();
        this.storyIndex++;
        this.messageText.setText("");
        this.writeStoryDialogue();
    };

    /**
     * Launched when the story related dialogue has been finished to close the dialogue box and to enable the help key
     */
    Dialogue.prototype.removeStoryDialogue = function () {
        this.storyIndex = 0;
        delete this.currentDialogue;
        window.game.add.tween(this.controlGroup).to({alpha: 0}, 900, Phaser.Easing.Linear.None, true, 0, 0);
        var tween = window.game.add.tween(this.controlGroup.position).to({y: window.game.height + 150}, 900, Phaser.Easing.Linear.None, true, 0, 0);
        tween.onComplete.add(function() {
            //this.controlGroup.visible = false;
            if (this.storyDialogueIsStart) {
                if (this.helpKey) {
                    this.helpKey.onDown.addOnce(this.helpFunc, this);
                }
            }
        }, this);
    };

    /**
     * Brings up a dialogue box if none is present or queues the message otherwise.
     * @param {string} message - The message to display
     * @param {string} typeoftalk - Id of the concierge's facial expression depending on the correctness of the given reply
     */
    Dialogue.prototype.popup = function(message, typeoftalk) {
        if (!this.dialogueOn) { //Pops the dialogue box is it is not on already
            this.dialogueOn = true;
            this.writingInProgress = true;

            if (this.player.position.y < 300) {
                this.isFromDown = true;
                this.popupFromDown();
            }
            else {
                this.isFromDown = false;
                this.popupFromUp();
            }

            this.messageText.setText("");
            this.controlGroup.visible = true;
            this.typingSound.play("", 0, window.game.globals.soundEffectsOn, true);
            this.writeTimer = dynamic_text.write_one(this.messageText, 40, message, this.signal);
            this.talkAnimation = this.face.play(typeoftalk);
        }
        else if (!this.writingInProgress) { //If the box is on and the previous line has already been written,
            //stops the disappearance timer and write immediately the new message
            if (this.timer) {
                this.timer.destroy();
            }
            internal_popup.call(this, message, typeoftalk);
        }
        else { //If the dialogue box is on and another message is being written, queues the new message
            this.messageQueue.enqueue({mess: message, type: typeoftalk});
        }
    };

    /**
     * Popup function called internally to display a queued message when the previous one has finished displaying.
     * @param {string} message - The message to display
     * @param {string} typeoftalk - Id of the concierge's facial expression depending on the correctness of the given reply
     */
    var internal_popup = function(message, typeoftalk) {
        this.spaceBar.visible = false;
        this.spaceBarTween.pause();
        this.skipDialogueKey.onDown.removeAll();
        this.writingInProgress = true;
        this.messageText.setText("");
        this.controlGroup.visible = true;
        this.typingSound.play("", 0, window.game.globals.soundEffectsOn, true);
        this.writeTimer = dynamic_text.write_one(this.messageText, 40, message, this.signal);
        this.talkAnimation = this.face.play(typeoftalk);
    };

    /**
     * Launched when the player hits the help key. It opens the help dialogue printing the level help message written
     * in the level definition file.
     */
    Dialogue.prototype.helpFunc = function() {
        this.helpMessageOn = true;
        this.popup(this.helpMessage, 'wrongDrop');
    };

    /**
     * Called when the message has been completely written. It allows to click on the dialogue box to discard the message.
     */
    Dialogue.prototype.activateInput = function() {
        this.typingSound.stop();
        this.skipDialogueKey.onDown.addOnce(this.removeNotification, this);
        this.talkAnimation.stop(true);
        this.writingInProgress = false;

        if (!this.messageQueue.isEmpty()) { //If the previous message has been written and another is in queue,
            //deletes the previous message and immediately writes the new one
            var item = this.messageQueue.dequeue();
            internal_popup.call(this, item.mess, item.type);
        } else { //If there is no new message, the old message stays on screen until the timer triggers or the player
            //hits the space bar
            //Auto remove notification
            this.timer = window.game.time.create(window.game, true);
            this.timer.add(2200, this.removeNotification, this);
            this.timer.start();
            this.spaceBar.visible = true;
            this.spaceBarTween.resume();
        }
    };

    /**
     * Removes the message when the time expires or if the user clicks on the dialogue box. It displays the next message
     * in queue or removes the box if the queue is empty.
     */
    Dialogue.prototype.removeNotification = function() {
        if (this.controlGroup) {
            this.spaceBar.visible = false;
            this.spaceBarTween.pause();
            this.dialogueOn = false;
            this.skipDialogueKey.onDown.removeAll();
            this.timer.destroy();
            this.messageQueue.dequeue();

            window.game.add.tween(this.controlGroup).to({alpha: 0}, 900, Phaser.Easing.Linear.None, true, 0, 0);
            if(this.isFromDown) { //Set the correct tween depending from the position of the dialogue box
                window.game.add.tween(this.controlGroup.position).to({y: window.game.height + 150}, 900, Phaser.Easing.Linear.None, true, 0, 0);
            }
            else {
                window.game.add.tween(this.controlGroup.position).to({y: - 150}, 900, Phaser.Easing.Linear.None, true, 0, 0);
            }

            this.messageText.setText("");

            if (this.helpMessageOn) { //Enters the "if" if this was the help message dialogue to reset the event on the
                //help key
                this.helpMessageOn = false;
                if (this.helpKey) {
                    this.helpKey.onDown.addOnce(this.helpFunc, this);
                }
            }

            if (this.endLevelDialogueWaiting) { //If the end level dialogue flag is set, it calls endLevelDialogue()
                this.endLevelDialogue();
            }
        }
    };

    /**
     * Opens up the dialogue box in the lower part of the screen. It is used when the player is in the upper part of the
     * screen so to not cover him.
     */
    Dialogue.prototype.popupFromDown = function() {
        this.controlGroup.position.x = 725;
        this.controlGroup.position.y = window.game.height - 35;
        window.game.add.tween(this.controlGroup).to({alpha: 1}, 900, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.controlGroup.position).to({y: window.game.height - 180}, 900, Phaser.Easing.Linear.None, true, 0, 0);
    };

    /**
     * Opens up the dialogue box in the upper part of the screen. It is used when the player is in the lower part of the
     * screen so to not cover him.
     */
    Dialogue.prototype.popupFromUp = function() {
        this.controlGroup.position.x = 725;
        this.controlGroup.position.y = - 150;
        window.game.add.tween(this.controlGroup).to({alpha: 1}, 900, Phaser.Easing.Linear.None, true, 0, 0);
        window.game.add.tween(this.controlGroup.position).to({y: -30}, 900, Phaser.Easing.Linear.None, true, 0, 0);
    };

    /**
     * Destroys the allocated resources to free up memory. It is called at the end of a level.
     */
    Dialogue.prototype.destroyObject = function() {
        if (this.writeTimer) {
            this.writeTimer.stop();
        }
        this.typingSound.stop();
        if (this.talkAnimation) {
            this.talkAnimation.stop();
        }
        this.controlGroup.destroy();
    };

    return Dialogue;
};