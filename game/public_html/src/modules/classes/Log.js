/*global Phaser*/

var Pda = require('./Pda');

module.exports = function() {
    /**
     * @classdesc Represents a level log that contains all the player's replies to the connection attempts.
     * @param {Array} entries - Array containing all the level log entries
     * @param {Object} pda - Object representing the Pda instance on which the log is printed
     * @param {Object} extContext - The external context used to update the log buttons hint boxes
     * @constructor
     */
    function Log(entries, pda, extContext) {
        this.currentPage = 0;
        this.elemperpage = 10;
        this.entries = entries;
        this.extContext = extContext;

        //Imports the alert box module
        this.alertBox = require('../alert_box');

        //Sets the text styles
        this.style = {font: "22px Courier New, monospace", fill: "#00AA11", align: "center"};
        this.style1 = {font: "15px Courier New, monospace", fill: "#00AA11", align: "left"};

        this.pda = pda;
        this.controlGroup = window.game.add.group();

        //Creates the log title text
        this.titleText = window.game.add.text(window.game.world.centerX, 90, "Firewall Log", this.style);
        this.titleText.anchor.setTo(0.5, 0.5);
        this.controlGroup.add(this.titleText);

        //Log header with columns
        this.headerY = 140;
        this.headerX = 70;
        this.controlGroup.add(window.game.add.text(this.headerX - 20, this.headerY, "#", this.style1));
        this.controlGroup.add(window.game.add.text(this.headerX + 20, this.headerY, "Gate", this.style1));
        this.controlGroup.add(window.game.add.text(this.headerX + 70, this.headerY, "Action", this.style1));
        this.controlGroup.add(window.game.add.text(this.headerX + 160, this.headerY, "Address", this.style1));
        this.controlGroup.add(window.game.add.text(this.headerX + 375, this.headerY, "Protocol", this.style1));
        this.controlGroup.add(window.game.add.text(this.headerX + 510, this.headerY, "Application", this.style1));
        this.controlGroup.add(window.game.add.text(this.headerX + 630, this.headerY, "Button", this.style1));

        this.hidden = false;
        this.entriesText = [];

        //Prints text that shows the current page and the overall page numbers of the log
        this.numberOfPages = Math.ceil(this.entries.length / this.elemperpage);
        this.npageText = window.game.add.text(680, 470, (Number)(this.currentPage+1) + " / " + this.numberOfPages, this.style1);
        this.npageText.anchor.set(0, 0);
        this.controlGroup.add(this.npageText);

        //Changes the functions to execute when the Pda buttons are pressed and their associated hint box texts
        this.pda.updateHandlers(this.lArrow, this.home, this.rArrow, "Previous page", "Close log", "Next page", this);

        //Calls the changePage function to print the first log page
        this.changePage.call(this, this.currentPage);
    }

    /**
     * Executed when left arrow button is pressed. It is used to go to the previous log page.
     */
    Log.prototype.lArrow = function() {
        if (this.currentPage > 0) {
            this.currentPage--;
            this.npageText.setText( (Number)(this.currentPage+1) + " / " + this.numberOfPages);
            this.changePage.call(this, this.currentPage);
        }
    };

    /**
     * Executed when the home button is pressed. It closes the log and comes back to the level selection screen.
     */
    Log.prototype.home = function() {
        this.hide();
        this.extContext.textGroup.visible = true;
        this.pda.updateHandlers(this.extContext.lArrow, this.extContext.home, this.extContext.rArrow, "Retry level",
            "Back to level selection", (this.extContext.playerWon) ? "Next level" : "Retry Level", this.extContext);
    };

    /**
     * Executed when right arrow button is pressed. It is used to go to the next log page.
     */
    Log.prototype.rArrow = function() {
        if (this.currentPage < this.numberOfPages - 1) {
            this.currentPage++;
            this.npageText.setText((Number)(this.currentPage+1) + " / " + this.numberOfPages);
            this.changePage.call(this, this.currentPage);
        }
    };

    /**
     * Hides the log.
     */
    Log.prototype.hide = function() {
        this.controlGroup.visible = false;
        this.hidden = true;
        if (this.alertBox.controlGroup) {
            this.alertBox.boxClicked();
        }
    };

    /**
     * Shows the log.
     */
    Log.prototype.show = function() {
        this.pda.button2.frame = 0;
        this.controlGroup.visible = true;
        this.hidden = false;
        this.pda.updateHandlers(this.lArrow, this.home, this.rArrow, "Previous page", "Close log", "Next page", this);
        this.pda.hideHintBox();
    };

    /**
     * Shows a player reply explanation when the corresponding status icon is clicked.
     * @param {Phaser.Sprite} sprite - Sprite clicked
     * @param {Object} item - A pointer object passed by the Phaser.Signal dispatch function
     * @param {string} explanation - The text message to print on the alert box in order to explain the player why
     * he/she has done right/wrong
     */
    Log.prototype.showExplanation = function(sprite, item, explanation) {
        if (!this.alertBox.controlGroup) {
            this.alertBox.popup(explanation);
        }
        else {
            this.alertBox.updateMessage(explanation);
        }

    };

    /**
     * Called when the left or right arrow button is pressed. It destroys previous displayed buttons and draws a new page.
     */
    Log.prototype.changePage = function() {
        var i;
        var index;

        //Destroys all the previous entries
        for (i=0; i < this.entriesText.length; i++) {
            this.entriesText[i].number.destroy();
            this.entriesText[i].gate.destroy();
            this.entriesText[i].action.destroy();
            this.entriesText[i].address.destroy();
            this.entriesText[i].proto.destroy();
            this.entriesText[i].app.destroy();
            this.entriesText[i].arrow.destroy();
            this.iconButtons[i].destroy();
        }
        delete this.entriesText;
        delete this.iconButtons;

        this.entriesText = [];
        this.iconButtons = [];

        //Computes how many elements to print on the current page, useful when this is the last page because
        //the amount most likely will be different from the this.elemperpage value
        var remaining_elem = this.entries.length - this.currentPage*this.elemperpage;
        for (i=0; i<remaining_elem && i < this.elemperpage; i++) {
            index = i + this.currentPage*this.elemperpage;
            this.entriesText[i] = {};

            //Prints the id of the log entry
            this.entriesText[i].number = window.game.add.text(this.headerX - 20, 180 + i*27, (Number)(index + 1), this.style1);
            this.controlGroup.add(this.entriesText[i].number);

            //Prints the gate number
            this.entriesText[i].gate = window.game.add.text(this.headerX + 20, 180 + i*27, (Number)(this.entries[index].conn.gate+1), this.style1);
            this.controlGroup.add(this.entriesText[i].gate);

            //Prints the action performed by the player
            this.entriesText[i].action = window.game.add.text(this.headerX + 70, 180 + i*27, this.entries[index].action, this.style1);
            this.controlGroup.add(this.entriesText[i].action);

            //Prints the IP address
            this.entriesText[i].address = window.game.add.text(this.headerX + 160, 180 + i*27, this.entries[index].conn.ipAddress + ":" + this.entries[index].conn.destPort, this.style1);
            this.controlGroup.add(this.entriesText[i].address);

            //Prints a green or a red arrow depending if the connection is incoming or outgoing
            this.entriesText[i].arrow = this.controlGroup.create(this.headerX + 345, 177 + i*27, (this.entries[index].conn.ingoing) ? 'inArrow' : 'outArrow');
            this.entriesText[i].arrow.width = 15;
            this.entriesText[i].arrow.height = 21;

            //Prints the transport protocol along with the optional application protocol
            this.entriesText[i].proto = window.game.add.text(this.headerX + 375, 180 + i*27, this.entries[index].conn.protocol +
                (this.entries[index].conn.appProtocol ? " (" + this.entries[index].conn.appProtocol + ")" : ""), this.style1);
            this.controlGroup.add(this.entriesText[i].proto);

            //Prints the application name
            this.entriesText[i].app = window.game.add.text(this.headerX + 510, 180 + i*27, this.entries[index].conn.application, this.style1);
            this.controlGroup.add(this.entriesText[i].app);

            //Creates the status icon button
            this.iconButtons[i] = this.controlGroup.create(this.headerX + 650, 180 + i*27, this.entries[index].icon);
            this.iconButtons[i].inputEnabled = true;
            this.iconButtons[i].events.onInputDown.add(this.showExplanation, this, 0, this.entries[index].explanation);
            this.iconButtons[i].events.onInputOver.add(this.iconButtonsEffectFuncOver, this, this.iconButtons[i]);
            this.iconButtons[i].events.onInputOut.add(this.iconButtonsEffectFuncOut, this, this.iconButtons[i]);
        }
    };

    /**
     * Called when the mouse hovers on a status icon to enlarge it. This has been implemented to remind the player about
     * the possibility to click the icons to receive an explanation.
     * @param {Phaser.Sprite} buttonObj - Sprite object hovered on
     */
    Log.prototype.iconButtonsEffectFuncOver = function (buttonObj) {
        window.game.add.tween(buttonObj.scale).to({x: 1.3, y:1.3}, 350, Phaser.Easing.Linear.None, true, 0, 0);
    };

    /**
     * Called when the mouse stops hovering on a status icon to bring it back to its normal size.
     * @param {Phaser.Sprite} buttonObj - Sprite object
     */
    Log.prototype.iconButtonsEffectFuncOut = function (buttonObj) {
        window.game.add.tween(buttonObj.scale).to({x: 1, y:1}, 350, Phaser.Easing.Linear.None, true, 0, 0);
    };

    return Log;
};