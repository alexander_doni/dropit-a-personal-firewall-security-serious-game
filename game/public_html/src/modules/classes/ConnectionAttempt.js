module.exports = function() {
    /**
     * @classdesc The class that models a connection attempt in the game.
     * @param {string} ipAddress - The IP address making the request, if ingoing, or receiving it, if outgoing
     * @param {boolean} ingoing - Flag indicating whether the connection is ingoing (true) or outgoing (false)
     * @param {int} destPort - Destination port of the connection. It represents a remote port if the request is outgoing,
     * or a local port if it is ingoing
     * @param {int} srcPort - Source port of the connection. It represents a remote port if the request is ingoing,
     * or a local port if it is outgoing
     * @param {string} protocol - Whether the request is carried by a TCP or a UDP protocol
     * @param {string} application - Name of the application originating or receiving the connection
     * @param {string} appProtocol - The protocol used by the application, if any (e.g. HTTP, SMTP, ecc.)
     * @param {string} shouldBe - The right action for this connection attempt (allowed or dropped)
     * @param {int} idCons - Id of the consequence object to take into account for this connection attempt
     * @param {int} gate - Gate number on which the connection has to arrive
     * @param {int} tts - Time to the appearance of this connection attempt. It starts when the tts of the previous
     * connection on the same gate elapses
     * @constructor
     */
    function ConnectionAttempt(ipAddress, srcPort, destPort, ingoing, protocol, application, appProtocol, shouldBe, idCons, gate, tts) {
        //Defining class Properties
        this.ipAddress = ipAddress;
        this.srcPort = srcPort;
        this.destPort = destPort;
        this.ingoing = ingoing;
        this.protocol = protocol;
        this.application = application;
        this.appProtocol = appProtocol;
        this.shouldBe = shouldBe;
        this.idCons = idCons;
        this.gate = gate;
        this.tts = tts;
    }

    return ConnectionAttempt;
};