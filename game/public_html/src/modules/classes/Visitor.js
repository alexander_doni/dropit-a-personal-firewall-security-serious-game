/* global Phaser*/

var mainframes_effects = require('../mainframes_effects');
var movement = require('../visitor_movement');

module.exports = function() {
    /**
     * @classdesc The instances of this class are the visitors that enters the game stage when the player allows a
     * request
     * @param {int} currentGate - The id of the gate where the player replied to the notice
     * @param {string} outcomeType - The string key that explains which effects has to be launched (e.g. "error")
     * @param {Phaser.Group} mainframes - The mainframesdown objects group from the play state
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     * @param {Phaser.Signal} signalOver - Signal launched to indicate that the gate animation is over
     * @param {Phaser.Signal} signalLevelOver - Signal launched when level end conditions have to be checked
     * @param {Phaser.Signal} signalMainframeDestroyed - Signal launched when the destroy effects have finished
     * @param {Object} player - Player object. It is needed to perform effects on the player itself, e.g. stunning it.
     * @constructor
     */
    function Visitor(currentGate, outcomeType, mainframes, lowerZGroup, signalOver, signalLevelOver, signalMainframeDestroyed, player) {
        this.currentGate = currentGate;
        this.lowerZGroup = lowerZGroup;
        this.signalLevelOver = signalLevelOver;
        this.mainframes = mainframes;
        this.signalMainframeDestroyed = signalMainframeDestroyed;
        this.signalOver = signalOver;
        this.player = player;
        this.startX = currentGate.position.x + currentGate.width/2;
        this.startY = currentGate.position.y + currentGate.height;
        this.disappeareanceSound = window.game.add.audio("poof");

        this.signalEffectOver = new Phaser.Signal();
        this.signalEffectOver.add(this.disappear, this);

        //Checks the outcome type to decide which effect function to launch
        if (outcomeType === "error") {
            this.errorOutcome();
        }
        else if (outcomeType === "correctAllow") {
            this.correctOutcome();
        }
        else if (outcomeType === "warning") {
            this.warningOutcome();
        }
    }

    /**
     * Creates and draws the visitor related to the passed sprite key. It enables the physics on it and sets its
     * animations up.
     * @param {string} spriteKey - The string used as sprite key of the loaded asset, e.g. "visitor1"
     */
    Visitor.prototype.enter = function(spriteKey) {
        this.visitor = this.lowerZGroup.create(this.startX, this.startY - ((this.currentGate < 3) ? 0 : 35), spriteKey);
        this.visitor.anchor.setTo(0.5, 0.5);
        window.game.physics.enable(this.visitor, Phaser.Physics.ARCADE);
        this.visitor.body.collideWorldBounds = true;
        this.visitor.body.moves = false;
        this.visitor.animations.add('down', [0, 1, 2, 3], 10, true);
        this.visitor.animations.add('left', [4, 5, 6, 7], 10, true);
        this.visitor.animations.add('right', [8, 9, 10, 11], 10, true);
        this.visitor.animations.add('up', [12, 13, 14, 15], 10, true);
    };

    /**
     * It launches the disappear effect for a visitor. It destroys its sprite and dispatches the signalLevelOver when
     * the disappear animation has finished.
     */
    Visitor.prototype.disappear = function() {
        this.visitor.animations.stop();
        var disappearEffect = this.lowerZGroup.create(this.visitor.position.x, this.visitor.position.y - 10, 'disappearEffect');
        disappearEffect.anchor.setTo(0.5, 0.5);
        var anim = disappearEffect.animations.add('disappearEffect', null, 10, false);
        window.game.add.tween(this.visitor).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 0, 0).onComplete.add(function() {
            this.visitor.destroy();
        }, this);
        anim.onStart.addOnce(function () {
            this.disappeareanceSound.play("", 0, window.game.globals.soundEffectsOn);
        }, this);
        anim.play(null, false, true);
        anim.onComplete.add(function() {
            this.currentGate.animInProgress = false;
            this.signalLevelOver.dispatch();
        }, this);
    };

    /**
     * Function executed when the player correctly allows a request. A visitor enters and just goes to a mainframe.
     */
    Visitor.prototype.correctOutcome = function () {
        //The destination mainframe is randomly picked from one of the still not destroyed ones
        this.targetMainframe = getRandomAliveMainframe(this.mainframes);

        //The enter function is called by passing it the sprite key. The string is generated by adding
        //to "visitor", which is the name for any innocuous visitor, a number from 1 to the visitors sprites available
        this.enter("visitor" + getRandomInt(1, 6));

        //The moveTo function is called to move the visitor from the gate to the target mainframe.
        //The mainframe object also has a node variable representing the map node in front of it.
        this.finalTween = movement.moveTo(this.visitor, this.currentGate.number, this.targetMainframe.node, true);

        this.finalTween.onComplete.add(function()
        {
            //Executed when the movement is finished

            //Launching the disappear function
            this.signalEffectOver.dispatch();

            this.signalOver.dispatch();
        }, this);
    };

    /**
     * Function executed when the player wrongly allows a not so dangerous request. A visitor enters and stuns the
     * player. Then it goes to a mainframe and disappears.
     */
    Visitor.prototype.warningOutcome = function () {
        //The destination mainframe is randomly picked from one of the still not destroyed ones
        this.targetMainframe = getRandomAliveMainframe(this.mainframes);
        this.punchSound = window.game.add.audio("punch");

        //The enter function is called by passing it the sprite key. The string is generated by adding
        //to "pest", which is the name for any silly visitor, a number from 1 to the visitors sprites available
        this.enter("pest" + getRandomInt(1, 4));

        //Hit effect generation
        var hitEffect = window.game.add.sprite(this.player.position.x + 15, this.player.position.y + 15, 'hit1');
        hitEffect.anchor.setTo(0.5, 0.5);
        var anim = hitEffect.animations.add('hit1', [0, 1, 2, 3, 4, 5], 10, false);
        anim.onStart.addOnce(function () {
            this.punchSound.play("", 0, window.game.globals.soundEffectsOn);
        }, this);
        anim.play(null, false, true);

        //Keyboard input is disabled to prevent the player from moving
        window.game.input.keyboard.enabled = false;
        window.game.input.keyboard.reset(false);
        this.stunEffect = this.lowerZGroup.create(this.player.position.x + 15, this.player.position.y, 'stunEffect');
        this.stunEffect.anchor.setTo(0.5, 0.5);

        //Tween with the stars to show that the player is stunned
        window.game.add.tween(this.stunEffect).to({angle: 360}, 1000, Phaser.Easing.Linear.None, true, 0, 6).onComplete.add(function() {
            //When the tweens end the player can move again
            window.game.input.keyboard.enabled = true;
            window.game.input.keyboard.reset(false);
            this.stunEffect.destroy();
        }, this);

        //Visitor movements to target mainframe
        this.finalTween = movement.moveTo(this.visitor, this.currentGate.number, this.targetMainframe.node, true);

        this.finalTween.onComplete.add(function()
        {
            //Executed when the movement is finished

            //Launching the disappear function
            this.signalEffectOver.dispatch();

            this.signalOver.dispatch();
        }, this);
    };

    /**
     * Function executed when the player wrongly allows a dangerous request. A visitor enters, goes to a mainframe
     * and destroys it. After the destroy effects have finished the visitor disappears.
     */
    Visitor.prototype.errorOutcome = function () {
        //The destination mainframe is randomly picked from one of the still not destroyed ones
        this.targetMainframe = this.mainframes.getAt(3 - this.mainframes.mainframesLeft);

        //The enter function is called by passing it the sprite key. The string is generated by adding
        //to "enemy", which is the name for any dangerous visitor, a number from 1 to the visitors sprites available
        this.enter("enemy" + getRandomInt(1, 4));

        //Visitor movement
        this.finalTween = movement.moveTo(this.visitor, this.currentGate.number, this.targetMainframe.node, true);

        this.finalTween.onComplete.add(function()
        {
            //Executed when the movement is finished

            //Destruction effects are started while visitor keeps walking up to simulate its movement to destroy
            //the mainframe
            this.visitor.animations.play("up");
            mainframes_effects.hit(this.targetMainframe, this.signalEffectOver, this.signalMainframeDestroyed, this.lowerZGroup);

            this.signalOver.dispatch();
        }, this);
    };

    /**
     * Destroys the visitor object
     */
    Visitor.prototype.destroyObject = function() {
        this.controlGroup.destroy();
    };

    /**
     * Picks a random integer between min (included) and max (excluded)
     * @param {int} min - Min value that the generated number can have
     * @param {int} max - Max value. The generated number can be at most max-1
     * @returns {int} The generated number.
     */
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * It returns randomly one of the mainframe objects that has not been destroyed yet.
     * @param {Phaser.Group} mainframes - Groups containing the lower mainframes objects
     * @returns {Object}The mainframe object representing an alive mainframe picked randomly
     */
    function getRandomAliveMainframe(mainframes) {
        var index;
        if (mainframes.mainframesLeft === 0) {
            index = 0;
        }
        else {
            index = getRandomInt(4 - mainframes.mainframesLeft, 3);
        }
        return mainframes.getAt(index);
    }

    return Visitor;
};