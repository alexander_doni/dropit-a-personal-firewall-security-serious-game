var alertBox = require('./alert_box');
var loadSave = require('./load_save_functions');

/**
 * Module that handles the three main menu buttons.
 * @type {{pos: number[], draw: module.exports.draw, addButton: module.exports.addButton, playState: module.exports.playState, howtoState: module.exports.howtoState, creditsState: module.exports.creditsState}}
 * @module
 * @name Menu Buttons
 */
module.exports = {
    /**
     * Offset of each button.
     * @member {Array} pos
     */
    pos: [-50, 50, 150],

    /**
     * Creates and draws menu buttons.
     */
    draw: function () {
        this.menuButtonSound = window.game.add.audio("buttonClick2");
        //Drawing buttons and adding click event handlers
        this.button1 = this.addButton(1, this.playState);
        this.button1.anchor.setTo(0.5, 0.5);

        this.button2 = this.addButton(2, this.howtoState);
        this.button2.anchor.setTo(0.5, 0.5);

        this.button3 = this.addButton(3, this.creditsState);
        this.button3.anchor.setTo(0.5, 0.5);
    },

    /**
     * Creates a menu button from the passed parameters.
     * @param {int} id - Button id
     * @param {Function} func - button callback
     * @returns {Phaser.Button} the created button.
     */
    addButton: function (id, func) {
        return window.game.add.button(window.game.world.centerX,
            window.game.world.centerY + this.pos[id - 1],
            'menu_button' + id, func, this, 1, 0);
    },

    /**
     * Button callback that starts the game.
     */
    playState: function () {
        if (window.game.globals.soundEffectsOn) {
            this.menuButtonSound.play("", 0, window.game.globals.soundEffectsOn);
        }
        window.game.state.start('selection');

        //Checks if the tutorial has been opened before to start the game.
        /*if (loadSave.checkCompatibility) {
            var tutorialRead = localStorage.getItem('howtoread');
        }

        if (window.game.globals.tutorialRead || tutorialRead) {
            window.game.state.start('selection');
        }
        else {
            alertBox.popup("You have to read the \"How to\" before to play!");
        }*/
    },

    /**
     * Button callback that starts the tutorial.
     */
    howtoState: function() {
        if (window.game.globals.soundEffectsOn) {
            this.menuButtonSound.play("", 0, window.game.globals.soundEffectsOn);
        }
        window.game.globals.tutorialRead = true;
        if (loadSave.checkCompatibility) {
            localStorage.setItem('howtoread', true);
        }

        window.game.state.start('howto', true, false, -1);
    },

    /**
     * Button callback that opens the credit screen.
     */
    creditsState: function() {
        if (window.game.globals.soundEffectsOn) {
            this.menuButtonSound.play("", 0, window.game.globals.soundEffectsOn);
        }
        //window.game.scale.startFullScreen(false);
        window.game.state.start('howto', true, false, -2);
    }
};