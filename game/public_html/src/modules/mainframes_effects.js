/**
 * This module takes care of of all the damage effects applied to the mainframes (smoke, explosions, electricity)
 * @type {{hit: module.exports.hit, explode: module.exports.explode, effectFunc: module.exports.effectFunc, smokeFunc: module.exports.smokeFunc, sparkFunc: module.exports.sparkFunc, electricityFunc: module.exports.electricityFunc, getRandomIntInclusive: module.exports.getRandomIntInclusive}}
 * @module
 * @name Mainframes Effects
 */
module.exports = {
    /**
     * Called from a visitor object when a mainframe has been hit. It calls the hit effect function and, on
     * its completion, the explosion function.
     * @param {Object} mainframe - The mainframe object that has been hit.
     * @param {Phaser.Signal} signalEffectOver - Signal dispatched when all the effects are over
     * @param {Phaser.Signal} signalMainframeDestroyed - Signal dispatched when the mainframe has been destroyed
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     * to make the visitor disappear.
     */
    hit: function(mainframe, signalEffectOver, signalMainframeDestroyed, lowerZGroup) {
        var mainframeHitSound = window.game.add.audio("mainframeHit");
        this.effectFunc(mainframe.position.x + 20, mainframe.position.y + 3, 'smallExplosion', mainframeHitSound, lowerZGroup).onComplete.add(function() {
            this.effectFunc(mainframe.position.x + 50, mainframe.position.y + 10, 'smallExplosion', mainframeHitSound, lowerZGroup).onComplete.add(function() {
                this.explode(mainframe, signalEffectOver, signalMainframeDestroyed, lowerZGroup);
            }, this);
        }, this);
    },

    /**
     * Generates the explosion, smoke, and electricity effects for the hit mainframe.
     * @param {Object} mainframe - The mainframe object that is about to explode.
     * @param {Phaser.Signal} signalEffectOver - Signal dispatched when all the effects are over
     * @param {Phaser.Signal} signalMainframeDestroyed - Signal dispatched when the mainframe has been destroyed
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     * to make the visitor disappear.
     */
    explode: function(mainframe, signalEffectOver, signalMainframeDestroyed, lowerZGroup) {
        var positions = [];
        var smallExplosionSound = window.game.add.audio("smallExplosion");
        var bigExplosionSound = window.game.add.audio("bigExplosion");

        //Computes the random position of the three small explosions
        for (var i=0; i<3; i++) {
            positions[i] = {};
            positions[i].x = this.getRandomIntInclusive(mainframe.position.x + i*30, mainframe.position.x + i*30 + 30);
            positions[i].y = this.getRandomIntInclusive(mainframe.position.y - 20, mainframe.position.y + mainframe.height - 15);
        }

        //Starts first explosion
        this.effectFunc(positions[0].x, positions[0].y, 'explosion2', smallExplosionSound, lowerZGroup).onComplete.add(function() {
            //Starts smoke emitter in the same position as the first explosion
            this.smokeFunc(positions[0].x, positions[0].y, lowerZGroup);
            //Starts second explosion
            this.effectFunc(positions[1].x, positions[1].y, 'explosion2', smallExplosionSound, lowerZGroup).onComplete.add(function() {
                //Starts smoke emitter in the same position as the second explosion
                this.smokeFunc(positions[1].x, positions[1].y, lowerZGroup);
                //Starts the third explosion
                this.effectFunc(positions[2].x, positions[2].y, 'explosion2', smallExplosionSound, lowerZGroup).onComplete.add(function() {
                    //Starts smoke emitter in the same position as the third explosion
                    this.smokeFunc(positions[2].x, positions[2].y, lowerZGroup);
                    //Starts big explosion
                    var finalEffect = this.effectFunc(mainframe.position.x + mainframe.width/2,
                        mainframe.position.y + mainframe.height/2, 'explosion1', bigExplosionSound, lowerZGroup);
                    signalMainframeDestroyed.dispatch(mainframe);
                    finalEffect.onComplete.add(function() {
                        //Starts sparks emitter and electricity effects
                        this.sparkFunc(mainframe.position.x + mainframe.width/2, mainframe.position.y + mainframe.height/2 - 15, lowerZGroup);
                        this.electricityFunc(mainframe.position.x + mainframe.width/2 - 30, mainframe.position.y + mainframe.height/2 - 15, lowerZGroup);
                        this.electricityFunc(mainframe.position.x + mainframe.width/2 + 30, mainframe.position.y + mainframe.height/2 - 15, lowerZGroup);
                        signalEffectOver.dispatch();
                    }, this);
                }, this);
            }, this);
        }, this);
    },

    /**
     * Generates visual effects to simulate the hit given to the mainframe by the visitor.
     * @param {int} effectX - The x coordinate at which to generate the effect.
     * @param {int} effectY - The y coordinate at which to generate the effect.
     * @param {string} sprite - The key of the effect sprite.
     * @param {Phaser.Sound} sound - The sound effect object to play
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     * @param {int} frames - Number of frames per second of the animation effect
     * @returns {Phaser.Animation} The hit effect animation object. It will be used to synchronize next effects
     * with the completion of this animation.
     */
    effectFunc: function(effectX, effectY, sprite, sound, lowerZGroup, frames) {
        var hitEffect = lowerZGroup.create(effectX, effectY, sprite);
        hitEffect.anchor.setTo(0.5, 0.5);
        var anim = hitEffect.animations.add(sprite, null, (frames) ? frames : 20, false);

        anim.onStart.addOnce(function () {
            sound.play("", 0, window.game.globals.soundEffectsOn);
        }, this);

        anim.play(null, false, true);

        return anim;
    },

    /**
     * Starts a smoke emitter to generate a smoking effect.
     * @param {int} effectX - X coordinate of the emitter position
     * @param {int} effectY - Y coordinate of the emitter position
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     * @returns {Phaser.Emitter} The created emitter.
     */
    smokeFunc: function(effectX, effectY, lowerZGroup) {
        //Big smoke column
        var smokeEmitter = window.game.add.emitter(effectX, effectY, 50);
        smokeEmitter.makeParticles('smoke2');
        smokeEmitter.gravity = -100;
        smokeEmitter.setAlpha(1, 0.1, 2000);
        smokeEmitter.setScale(0, 0.8, 0, 0.8, 2000);
        smokeEmitter.setXSpeed(0, 0);
        smokeEmitter.setYSpeed(-1, -1);
        smokeEmitter.setRotation(0, 0);
        smokeEmitter.start(false, 1500, 150);

        lowerZGroup.add(smokeEmitter);

        return smokeEmitter;
    },

    /**
     * Starts a spark emitter.
     * @param {int} effectX - X coordinate of the emitter position
     * @param {int} effectY - Y coordinate of the emitter position
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     * @returns {Phaser.Emitter} The created emitter.
     */
    sparkFunc: function(effectX, effectY, lowerZGroup) {
        var sparkEmitter = window.game.add.emitter(effectX, effectY, 50);
        sparkEmitter.makeParticles('spark');
        sparkEmitter.gravity = 50;
        sparkEmitter.setAlpha(1, 0.1, 2000);
        sparkEmitter.setXSpeed(-35, 35);
        sparkEmitter.setYSpeed(-1, -1);
        sparkEmitter.setRotation(0, 0);
        sparkEmitter.start(false, 1500, 350);

        lowerZGroup.add(sparkEmitter);

        return sparkEmitter;
    },

    /**
     * Starts an animation to simulate electric lighting effects.
     * @param {int} effectX - X coordinate of the emitter position
     * @param {int} effectY - Y coordinate of the emitter position
     * @param {Phaser.Group} lowerZGroup - The sprites added to this group will be drawn under the other sprites
     * @returns {Phaser.Animation} The created animation object.
     */
    electricityFunc: function(effectX, effectY, lowerZGroup) {
        var electricityEffect = lowerZGroup.create(effectX, effectY, 'electricity');
        electricityEffect.anchor.setTo(0.5, 0.5);
        var anim = electricityEffect.animations.add('electricity', null, 20, false);
        anim.play(null, true, true);

        return anim;
    },

    /**
     * Picks a random integer between min (included) and max (excluded)
     * @param {int} min - Min value that the generated number can have
     * @param {int} max - Max value. The generated number can be at most max-1
     * @returns {int} The generated number.
     */
    getRandomIntInclusive: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
};