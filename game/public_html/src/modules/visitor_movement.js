/* global Phaser */

/**
 * Visitors movement module to handle the automatic movement of a visitor by providing the source and
 * destination map node.
 * @type {{mapNodes: *, moveTo: module.exports.moveTo, moveToX: module.exports.moveToX, moveToY: module.exports.moveToY}}
 * @module
 * @name Visitor Movement
 */
module.exports = {
    /**
     * Array containing all the map nodes objects. Each object is a point with an X and an Y coordinate.
     * @member {Array} mapNodes
     */
    mapNodes: [{x: 78, y: 135}, {x: 400, y: 135}, {x: 722, y: 135}, {x: 78, y: 410}, {x: 400, y: 410}, {x: 722, y: 410},
        {x: 78, y: 245}, {x: 400, y: 245}, {x: 722, y: 245},{x: 78, y: 515}, {x: 400, y: 515}, {x: 722, y: 515} ],

    /**
     * Main function that moves the sprite of a visitor from the node passed as source node to the one
     * passed as destination node.
     * @param {Phaser.Sprite} sprite - Sprite object of the visitor that has to be moved
     * @param {int} srcNodeIndex - The index fo the map node from which the visitor starts
     * @param {int} destNodeIndex - The index fo the map node to which the visitor has to arrive
     * @param {boolean} fromGate - Flag indicating whether the visitor movement is started from a gate or not
     * @returns {*}
     */
    moveTo: function(sprite, srcNodeIndex, destNodeIndex, fromGate) {
        var enterTween, centerTween, changeLineTween, finalTween;
        var currentNodeIndex = srcNodeIndex;
        var timeToMoveFactor = 10;

        srcNodeIndex = Number(srcNodeIndex);
        destNodeIndex = Number(destNodeIndex);

        //If sprite is entering from a gate, execute initial movement
        if (fromGate) {
            enterTween = this.moveToY(sprite, this.mapNodes[srcNodeIndex].y, timeToMoveFactor*(Math.abs(sprite.position.y - this.mapNodes[srcNodeIndex].y)), null);
            currentNodeIndex = srcNodeIndex;
        }

        //If destination node is the same as the enter point
        if (srcNodeIndex === destNodeIndex) {
            enterTween.start();
            return enterTween;
        }

        //If destination node is already in the same line as the source node, reach it directly
        if (this.mapNodes[srcNodeIndex].y === this.mapNodes[destNodeIndex].y) {
            finalTween = this.moveToX(sprite, this.mapNodes[destNodeIndex].x, timeToMoveFactor*(Math.abs(this.mapNodes[destNodeIndex].x - this.mapNodes[currentNodeIndex].x)), (enterTween) ? enterTween : null);

            if (enterTween) {
                enterTween.start();
            }
            else {
                finalTween.start();
            }

            return finalTween;
        }

        //If sprite is not in the center node of its line, move it to center first
        if (srcNodeIndex !== 1 && srcNodeIndex !== 4 && srcNodeIndex !== 7 && srcNodeIndex !== 10) {
            centerTween = this.moveToX(sprite, this.mapNodes[(Math.floor(currentNodeIndex/3) * 3) + 1].x,
                timeToMoveFactor*(Math.abs(this.mapNodes[(Math.floor(currentNodeIndex/3) * 3) + 1].x - this.mapNodes[currentNodeIndex].x)),
                (enterTween) ? enterTween : null);
            currentNodeIndex = (Math.floor(currentNodeIndex/3) * 3) + 1;
        }

        //If destination is the lower center gate, move it there
        if (destNodeIndex === 10) {
            finalTween = this.moveToY(sprite, this.mapNodes[destNodeIndex].y,
                timeToMoveFactor*(Math.abs(this.mapNodes[destNodeIndex].x - this.mapNodes[currentNodeIndex].x)), centerTween);

            if (enterTween) {
                enterTween.start();
            }
            else {
                centerTween.start();
            }

            return finalTween;
        }

        changeLineTween = this.moveToY(sprite, this.mapNodes[(Math.floor(destNodeIndex/3) * 3) + 1].y,
            timeToMoveFactor*(Math.abs(this.mapNodes[(Math.floor(destNodeIndex/3) * 3) + 1].y - this.mapNodes[currentNodeIndex].y)),
            (centerTween) ? centerTween : enterTween);
        currentNodeIndex = (Math.floor(destNodeIndex/3) * 3) + 1;

        //If destination has not yet been reached, execute final movement
        if (currentNodeIndex !== destNodeIndex) {
            finalTween = this.moveToX(sprite, this.mapNodes[destNodeIndex].x,
                timeToMoveFactor * (Math.abs(this.mapNodes[destNodeIndex].x - this.mapNodes[currentNodeIndex].x)), changeLineTween);
        }

        if (enterTween) {
            enterTween.start();
        }
        else if (centerTween) {
            centerTween.start();
        }
        else {
            changeLineTween.start();
        }

        return (finalTween) ? finalTween : changeLineTween;
    },

    /**
     * Moves a visitor sprite along the X coordinate until it reaches the X destination coordinate.
     * @param {Phaser.Sprite} sprite - Sprite object of the visitor that has to be moved
     * @param {int} destX - X destination coordinate that the visitor has to reach
     * @param {int} time - Number of ms in which the visitor has to reach the destination
     * @param {Phaser.Tween} previousTween - Optional previous tween object used for tweens chaining
     * @returns {Phaser.Tween} The created tween object that can be used for tweens chaining
     */
    moveToX: function(sprite, destX, time, previousTween) {
        var tween = window.game.add.tween(sprite.position).to({x: destX}, time, Phaser.Easing.Linear.None, false, 0, 0);
        tween.onStart.add(function () {
            //Decides the animation to play based on the sprite current position in respect to the destination
            //coordinate
            if (sprite.position.x < destX) {
                sprite.animations.play('right');
            }
            else {
                sprite.animations.play('left');
            }
        }, this);

        //If provided, the current tween is chained to the tween passed as parameter
        if (previousTween) {
            previousTween.chain(tween);
        }

        return tween;
    },

    /**
     * Moves a visitor sprite along the Y coordinate until it reaches the Y destination coordinate.
     * @param {Phaser.Sprite} sprite - Sprite object of the visitor that has to be moved
     * @param {int} destY - Y destination coordinate that the visitor has to reach
     * @param {int} time - Number of ms in which the visitor has to reach the destination
     * @param {Phaser.Tween} previousTween - Optional previous tween object used for tweens chaining
     * @returns {Phaser.Tween} The created tween object that can be used for tweens chaining
     */
    moveToY: function(sprite, destY, time, previousTween) {
        var tween = window.game.add.tween(sprite.position).to({y: destY}, time, Phaser.Easing.Linear.None, false, 0, 0);
        tween.onStart.add(function () {
            //Decides the animation to play based on the sprite current position in respect to the destination
            //coordinate
            if (sprite.position.y < destY) {
                sprite.animations.play('down');
            }
            else {
                sprite.animations.play('up');
            }
        }, this);

        //If provided, the current tween is chained to the tween passed as parameter
        if (previousTween) {
            previousTween.chain(tween);
        }

        return tween;
    }
};