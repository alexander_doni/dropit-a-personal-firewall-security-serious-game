This project has been developed for a Master's Degree thesis at Politecnico di Torino. The thesis includes the programmer's manual which contains all the information about the game design and development. The thesis PDF is downloadable from https://www.dropbox.com/s/5a6g5vxckcq7qnh/DropIt%21%20-%20A%20personal%20firewall%20security%20serious%20game.pdf?dl=0

DropIt! is a small browser game realized with the Phaser framework, HTML5, and JavaScript. It is aimed to teach computer users about how and why to use a personal firewall and it is currently playable at http://130.192.1.19/dropit

The game has been designed to be easily modifiable and expandable also by non programmers, in fact it reads the totality of the learning contents and levels data from text and JSON files. More levels can be expanded simply by creating the levels files, because the game will automatically recognize the levels available.

Browserify.js has been used to ensure a cross-browser game modularization and an easier development for possible future programmers. Using browserify, it has been necessary to adapt the JavaScript class pattern to work with it. The public_html/modules/classes folder contains all the modules that are created to be used as classes and can be thus instantiated through the "new" keyword.

Docs is contained in the docs folder and has been generated through the JSDoc3 tool. It is sufficient to open the index.html file within the docs folder to browse the documentation.

Being a serious game, it also includes a Python web service (written through the Tornado framework) to gather data about the players' performances and to store them in a database thanks to the SQLAlchemy framework.

The developer of the project is Antonino Aloi.

Contact: antonino.aloi@outlook.com